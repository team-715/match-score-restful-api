# pylint: disable=missing-docstring
# pylint: disable=W0707

import jwt
from fastapi import HTTPException, status
from common.config import settings
from data.models import Token, User
from data.database import read_query
from services.user_service import find_by_email

ACCESS_TOKEN_EXPIRE_MINUTES = settings.access_token_expire_minutes
SECRET_KEY = settings.secret_key
# HS256 Signing Algorithm
ALGORITHM = settings.algorithm


def create_token(user: User):
    to_encode = {"sub": user.email}
    # issued_at = datetime.utcnow()
    # to_encode.update({"iat": issued_at})
    # expire = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    # to_encode.update({"exp": expire})
    encoded = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

    return Token(token=encoded)


def from_token(token: str) -> User | None:

    return find_by_email(decode_email_from_token(token))


def is_authenticated(token: str) -> bool:
    email = decode_email_from_token(token)
    data = read_query(
        """SELECT id, email, role FROM users WHERE email =  ?""", (email,)
    )

    return bool(data)


def decode_email_from_token(token):
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=ALGORITHM)
        email: str = payload.get("sub")
    except jwt.exceptions.ExpiredSignatureError:
        # Raised when a token’s exp claim indicates that it has expired
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Your session has expired"
        )
    except jwt.exceptions.InvalidTokenError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
        )

    return email


def get_user_or_raise_401(token: str) -> User:
    if not is_authenticated(token):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
        )

    return from_token(token)
