# pylint: disable=missing-docstring
from math import log2
from random import randrange
from typing import Any


def is_even(number: int) -> bool:
    if number % 2 == 0:
        return True
    return False


def is_a_power_of_two(number: int) -> bool:
    if log2(number) % 1 == 0:
        return True
    return False


def pop_random(lst: list):
    """Select and delete a random element from a list using random.randrange"""
    idx = randrange(0, len(lst))

    return lst.pop(idx)


def construct_sql_for_in_operator(values: list[Any]) -> str:
    sql_string = ""
    for item in values:
        sql_string += f"{item}, "
    sql_ = sql_string.removesuffix(", ")

    return sql_


# def difference_two_sets(set1: set, set2: set) -> set:
#     return set1 - set2
