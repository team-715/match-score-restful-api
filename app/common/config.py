# pylint: disable=missing-docstring
# pylint: disable=R0903

from pydantic import BaseSettings


class Settings(BaseSettings):
    database_hostname: str
    database_port: int
    database_password: str
    database_name: str
    database_user: str
    secret_key: str
    algorithm: str
    access_token_expire_minutes: int
    mailjet_api_key: str
    mailjet_secret_key: str

    class Config:
        env_file = ".env"


settings = Settings()
