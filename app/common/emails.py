# pylint: disable=missing-docstring
from typing import Any
from mailjet_rest import Client
from requests.models import Response
from common.config import settings

from data.models import GameUpdate

EMOJI = "🐞🐍👍🎉🤩😂🐶🍿😎✨💬😘"

PROMOTE_SUBJECT = "✨ Promote-to-director ✨"
LINK_SUBJECT = "🐍 Link-to-player-profile 🐍"
ADDED_TO_TOURNAMENT_SUBJECT = "👍🎉🤩 Tournament participation 👍🎉🤩"
ADDED_TO_GAME_SUBJECT = "👍😎✨ Match participation 👍😎✨"
PROMOTE_APPROVED_TEMPLATE_ID = 4333166
PROMOTE_DECLINED_TEMPLATE_ID = 4333070
LINK_APPROVED_TEMPLATE_ID = 4330443
LINK_DECLINED_TEMPLATE_ID = 4333070
ADDED_TO_TOURNAMENT_TEMPLATE_ID = 4334599
ADDED_TO_GAME_TEMPLATE_ID = 4335043
SENDER_EMAIL = "match.score.fp@gmail.com"
RECIPIENT = "User"

api_key = settings.mailjet_api_key
api_secret = settings.mailjet_secret_key

_mailjet = Client(auth=(api_key, api_secret), version="v3.1")


def create_msg_added_in_tournament(
    receiver_email: str, receiver_name: str = RECIPIENT, **kwargs
):
    player_name = kwargs.get("player_name") or receiver_name
    data = {
        "Messages": [
            {
                "From": {"Email": f"{SENDER_EMAIL}", "Name": "Match Score"},
                "To": [{"Email": f"{receiver_email}", "Name": f"{player_name}"}],
                "TemplateID": ADDED_TO_TOURNAMENT_TEMPLATE_ID,
                "TemplateLanguage": True,
                "Subject": ADDED_TO_TOURNAMENT_SUBJECT,
            }
        ]
    }

    return data


def create_msg_game_update(
    receiver_email: str, receiver_name: str = RECIPIENT, **kwargs
):
    player_name = kwargs.get("player_name") or receiver_name
    game = kwargs.get("game") or GameUpdate()
    # location = kwargs.get('location')
    # datetime = kwargs.get('datetime')
    html_part = f"""
    <h3> Dear, {player_name} 😎, there has been an update to one of your matches! 
    For further info log in here <a href=\"https://match-score-api-team7.herokuapp.com/docs\">Match Score</a>!</h3>"""

    html_part += "<ol>"
    for k, value in game.dict().items():
        html_part += f"<li>{k} is {value};</li>"
    html_part += "</ol>"

    data = {
        "Messages": [
            {
                "From": {"Email": f"{SENDER_EMAIL}", "Name": "Match Score"},
                "To": [{"Email": f"{receiver_email}", "Name": f"{player_name}"}],
                "Subject": "👍👍Match Update👍👍",
                "TextPart": f"""
                    Dear {player_name}, There has been an update to one of your matches! 
                    Please log in to see the changes""",
                "HTMLPart": f"{html_part}",
            }
        ]
    }

    return data


def create_msg_added_in_game(
    receiver_email: str, receiver_name: str = RECIPIENT
) -> dict[str, list[dict[str, Any]]]:
    data = {
        "Messages": [
            {
                "From": {"Email": f"{SENDER_EMAIL}", "Name": "Match Score"},
                "To": [{"Email": f"{receiver_email}", "Name": f"{receiver_name}"}],
                "TemplateID": ADDED_TO_GAME_TEMPLATE_ID,
                "TemplateLanguage": True,
                "Subject": ADDED_TO_GAME_SUBJECT,
            }
        ]
    }

    return data


def create_msg_request_update(
    receiver_email, receiver_name=RECIPIENT, **kwargs
) -> dict[str, list[dict[str, Any]]]:
    request_type = kwargs.get("request_type")
    status = kwargs.get("status")

    # promote requests
    if request_type == "promote":
        email_subject = PROMOTE_SUBJECT
        if status == "approved":
            template_id = PROMOTE_APPROVED_TEMPLATE_ID
        else:  # status declined
            template_id = PROMOTE_DECLINED_TEMPLATE_ID
    # link requests
    else:
        email_subject = LINK_SUBJECT
        if status == "approved":
            template_id = LINK_APPROVED_TEMPLATE_ID
        else:  # status declined
            template_id = LINK_DECLINED_TEMPLATE_ID

    data = {
        "Messages": [
            {
                "From": {"Email": f"{SENDER_EMAIL}", "Name": "Match Score"},
                "To": [{"Email": f"{receiver_email}", "Name": f"{receiver_name}"}],
                "TemplateID": template_id,
                "TemplateLanguage": True,
                "Subject": f"{email_subject}",
            }
        ]
    }

    return data


def send_email(email: str, func, **kwargs) -> Response:
    data = func(email, **kwargs)
    result = _mailjet.send.create(data)

    return result


# def send_in_bulk(emails: list[str], func, **kwargs) -> list[Response]:
#     result = []
#     for e in emails:
#         # game=kwargs.get('game') is keyword argument for create_msg_game_update()
#         data = func(e, game=kwargs.get("game"))
#         r = _mailjet.send.create(data)
#         result.append(r)

#     return result
