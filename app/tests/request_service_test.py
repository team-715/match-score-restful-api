# pylint: disable=missing-docstring C0103 C0301

import unittest

from data.models import Request, RequestSingle, PromoteRequest, RequestLink
from services import request_service
from tests.testdata import (
    FAKE_USER_ID,
    FAKE_PLAYER_ID
)
from tests.testdata import fake_db


mock_db = fake_db()


class RequestService_Should(unittest.TestCase):
    def setUp(self) -> None:
        request_service.database = mock_db

    def tearDown(self) -> None:
        mock_db.reset_mock(return_value=True, side_effect=True)

    def test_all_returnsListOfRequest_when_dataIsPresent(self):
        mock_db.read_query.return_value = [
            (98, 'promote', 'approved'),
            (99, 'link', 'declined')
        ]
        result = request_service.get_all(0,0)

        expected = [
            Request(id=98, type='promote', status='approved'),
            Request(id=99, type='link', status='declined')
        ]

        self.assertEqual(2, len(result))
        self.assertEqual(expected, result)

    def test_all_returnsNone_when_noDataIsPresent(self):
        mock_db.read_query.return_value = []
        result = request_service.get_all(0,0)

        self.assertEqual(None, result)

    def test_get_by_id_returnsRequestSingle_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = (99, 11, 'promote', None, 'approved')

        result = request_service.get_by_id(0)

        expected = RequestSingle(
            id=99, user_id=11, type='promote',
            player_id=None, status='approved'
            )

        self.assertEqual(expected, result)

    def test_get_by_id_returnsNone_when_noDataIsPresent(self):
        mock_db.read_query_one.return_value = ()

        result = request_service.get_by_id(0)

        self.assertEqual(None, result)

    def test_add_promote_request_returnsPromoteRequest_with_correctId(self):
        mock_db.insert_query.return_value = 99

        result = request_service.add_promote_request(FAKE_USER_ID)

        expected = PromoteRequest(id=99, from_user=FAKE_USER_ID)

        self.assertEqual(expected, result)
        self.assertEqual(99, result.id)

    def test_add_link_request_returnsLinkRequest_with_correctId(self):
        mock_db.insert_query.return_value = 99

        result = request_service.add_link_request(FAKE_USER_ID, FAKE_PLAYER_ID)

        expected = RequestLink(id=99, from_user=FAKE_USER_ID, associate_with_player_id=FAKE_PLAYER_ID)

        self.assertEqual(expected, result)
        self.assertEqual(99, result.id)

    def test_update_returnsRequestSingle_with_updatedStatus(self):
        # mock_db.update_query.return_value = 1

        new_status = 'approved'
        request = RequestSingle(id=99, user_id=FAKE_USER_ID, type='promote', player_id=9, status='pending')
        expected = RequestSingle(id=99, user_id=FAKE_USER_ID, type='promote', player_id=9, status='approved')

        result = request_service.update(new_status, request)

        self.assertEqual(expected, result)
        self.assertEqual(new_status, result.status)

    def test_update_callsUpdateQueryOnce_when_newStatusIsNotApproved(self):
        new_status = 'declined'
        request = RequestSingle(id=99, user_id=FAKE_USER_ID, type='promote', player_id=9, status='pending')

        request_service.update(new_status, request)

        mock_db.update_query.assert_called_once()
