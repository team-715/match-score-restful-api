# pylint: disable=missing-docstring C0103 C0301 R0904

import unittest
from mariadb import IntegrityError

from tests.testdata import (
    FAKE_PLAYER_ID,
    FAKE_FULLNAME,
    FAKE_COUNTRY,
    FAKE_CLUB,
    FAKE_SEARCH_COUNTRY_AND_CLUB,
    FAKE_PAGINATION
)
from tests.testdata import fake_db, fake_params_player
from data.models import Player, PlayerShort
from services import player_service

mock_db = fake_db()


class PlayerService_Should(unittest.TestCase):
    def setUp(self) -> None:
        player_service.database = mock_db

    def tearDown(self) -> None:
        mock_db.reset_mock(return_value=True, side_effect=True)

    def test_existById_returns_True_if_dataIsPresent(self):
        mock_db.read_query_one.return_value = (FAKE_PLAYER_ID, )
        result = player_service.exist_by_id(FAKE_PLAYER_ID)

        self.assertTrue(result)

    def test_existById_returns_False_if_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = player_service.exist_by_id(FAKE_PLAYER_ID)

        self.assertFalse(result)

    def test_all_returns_listOfPlayer_when_dataIsPresent(self):
        row_1 = (FAKE_PLAYER_ID, FAKE_FULLNAME, FAKE_COUNTRY, FAKE_CLUB)
        row_2 = (FAKE_PLAYER_ID, FAKE_FULLNAME, FAKE_COUNTRY, FAKE_CLUB)
        mock_db.read_query.return_value = [row_1, row_2]
        result = list(player_service.get_all(
            **FAKE_PAGINATION, **FAKE_SEARCH_COUNTRY_AND_CLUB))

        mock_db.read_query.assert_called_once()
        self.assertEqual(len(result), 2)
        self.assertIsInstance(result[0], Player)

    def test_all_returns_None_when_noData(self):
        mock_db.read_query.return_value = []
        result = player_service.get_all(
            **FAKE_PAGINATION, **FAKE_SEARCH_COUNTRY_AND_CLUB)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)

    def test_getById_returns_Player_when_dataIsPresent(self):
        mock_db.read_query.return_value = [
            (FAKE_PLAYER_ID, FAKE_FULLNAME, FAKE_COUNTRY, FAKE_CLUB)
        ]
        result = player_service.get_by_id(FAKE_PLAYER_ID)
        expected = Player(id=FAKE_PLAYER_ID, fullname=FAKE_FULLNAME,
                          country=FAKE_COUNTRY, club=FAKE_CLUB)

        mock_db.read_query.assert_called_once()
        self.assertEqual(expected, result)
        self.assertEqual(expected.id, result.id)
        self.assertEqual(expected.fullname, result.fullname)
        self.assertEqual(expected.country, result.country)
        self.assertEqual(expected.club, result.club)

    def test_getById_returns_None_when_noData(self):
        mock_db.read_query.return_value = []
        result = player_service.get_by_id(FAKE_PLAYER_ID)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)


    def test_getByFullname_returns_Player_when_dataIsPresent(self):
        mock_db.read_query.return_value = [
            (FAKE_PLAYER_ID, FAKE_FULLNAME, FAKE_COUNTRY, FAKE_CLUB)
        ]
        result = player_service.get_by_fullname(FAKE_FULLNAME)
        expected = Player(id=FAKE_PLAYER_ID, fullname=FAKE_FULLNAME,
                          country=FAKE_COUNTRY, club=FAKE_CLUB)

        mock_db.read_query.assert_called_once()
        self.assertEqual(expected, result)
        self.assertEqual(expected.id, result.id)
        self.assertEqual(expected.fullname, result.fullname)
        self.assertEqual(expected.country, result.country)
        self.assertEqual(expected.club, result.club)

    def test_getByFullname_returns_None_when_noData(self):
        mock_db.read_query.return_value = []
        result = player_service.get_by_fullname(FAKE_FULLNAME)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)

    def test_add_returns_Player_when_insertSuccessful(self):
        mock_db.insert_query.return_value = FAKE_PLAYER_ID
        result = player_service.add(Player(**fake_params_player()))
        expected = Player(id=FAKE_PLAYER_ID, **fake_params_player())

        mock_db.insert_query.assert_called_once()
        self.assertEqual(expected, result)
        self.assertEqual(expected.id, result.id)
        self.assertEqual(expected.fullname, result.fullname)
        self.assertEqual(expected.country, result.country)
        self.assertEqual(expected.club, result.club)

    def test_add_returns_None_when_insertFailed(self):
        mock_db.insert_query.side_effect = IntegrityError
        result = player_service.add(Player(**fake_params_player()))

        mock_db.insert_query.assert_called_once()
        self.assertIsNone(result)

    def test_update_returns_Player_with_newClub_if_newClubIsNotNone(self):
        new = PlayerShort(club='New Club')
        old = Player(id=FAKE_PLAYER_ID, **fake_params_player())
        result = player_service.update(new, old)

        mock_db.update_query.assert_called_once()
        self.assertIsInstance(result, Player)
        self.assertEqual(new.club, result.club)

    def test_update_returns_Player_with_newCountry_if_newCountryIsNotNone(self):
        new = PlayerShort(country='New Country')
        old = Player(id=FAKE_PLAYER_ID, **fake_params_player())
        result = player_service.update(new, old)

        mock_db.update_query.assert_called_once()
        self.assertIsInstance(result, Player)
        self.assertEqual(new.country, result.country)

    def test_update_returns_Player_unchanged_if_newCountryIsNoneAndNewClubIsNone(self):
        new = PlayerShort()
        old = Player(id=FAKE_PLAYER_ID, **fake_params_player())
        result = player_service.update(new, old)

        mock_db.update_query.assert_called_once()
        self.assertIsInstance(result, Player)
        self.assertEqual(FAKE_COUNTRY, result.country)
        self.assertEqual(FAKE_CLUB, result.club)

    def test_findExistingPlayers_returns_emptyList_when_noData(self):
        mock_db.read_query.return_value = []
        result = player_service.find_existing_players([FAKE_FULLNAME, FAKE_FULLNAME])

        mock_db.read_query.assert_called_once()
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 0)

    def test_findExistingPlayers_returns_listOfStr_when_dataIsPresent(self):
        players = [(FAKE_FULLNAME,), (FAKE_FULLNAME,)]
        mock_db.read_query.return_value = players
        result = player_service.find_existing_players([FAKE_FULLNAME, FAKE_FULLNAME])

        mock_db.read_query.assert_called_once()
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)

    def test_findNonexistingPlayers_returns_emptyList_when_dataIsPresent(self):
        mock_db.read_query.return_value = [(FAKE_FULLNAME,), (FAKE_FULLNAME,)]
        result = player_service.find_non_existing_players([FAKE_FULLNAME, FAKE_FULLNAME])

        mock_db.read_query.assert_called_once()
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 0)

    def test_findNonexistingPlayers_returns_listOfStr_when_noData(self):
        mock_db.read_query.return_value = []
        result = player_service.find_non_existing_players([FAKE_FULLNAME, FAKE_FULLNAME])

        mock_db.read_query.assert_called_once()
        self.assertIsInstance(result, list)
        self.assertEqual(len(result), 2)

    def test_createNewPlayers_insertsInDb(self):
        player_service.create_new_players([FAKE_FULLNAME])

        mock_db.insert_query.assert_called_once()

    def test_delete_updates_db(self):
        player_service.delete(FAKE_PLAYER_ID)

        mock_db.insert_query.assert_called_once()


    def test_getTournamentsPlayed_returns_0_when_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = player_service.get_tournaments_played(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(0, result)

    def test_getTournamentsPlayed_returns_intNot0_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = (1,)
        result = player_service.get_tournaments_played(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(result, 1)

    def test_getTournamentsWon_returns_0_when_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = player_service.get_tournaments_won(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(0, result)

    def test_getTournamentsWon_returns_intNot0_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = (1,)
        result = player_service.get_tournaments_won(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(result, 1)

    def test_getMatchesPlayed_returns_0_when_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = player_service.get_matches_played(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(0, result)

    def test_getMatchesPlayed_returns_intNot0_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = (2,)
        result = player_service.get_matches_played(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(result, 2)


    def test_getMatchesWon_returns_0_when_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = player_service.get_matches_won(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(0, result)

    def test_getMatchesWon_returns_intNot0_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = (2,)
        result = player_service.get_matches_won(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(result, 2)

    def test_getMostOftenPlayedOpponentData_returnsEmptyList_when_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = player_service.get_most_often_played_opponent_data(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual([], result)

    def test_getMostOftenPlayedOpponentData_returnsListWithOpponentAndGamesCount_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = ('Most Often', 10)
        result = player_service.get_most_often_played_opponent_data(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(list(('Most Often', 10)), result)

    def test_getMostRarePlayedOpponentData_returnsEmptyList_when_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = player_service.get_most_rare_played_opponent_data(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual([], result)

    def test_getMostRarePlayedOpponentData_returnsListWithOpponentAndGamesCount_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = ('Most Rare', 0)
        result = player_service.get_most_often_played_opponent_data(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(list(('Most Rare', 0)), result)


    def test_getBestOpponentData_returns_emptyTuple_when_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = player_service.get_best_opponent_data(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(tuple(), result)

    def test_getBestOpponentData_returns_tupleWithOpponentAndLostGames_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = ('Best Opponent', 0)
        result = player_service.get_best_opponent_data(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(('Best Opponent', 0), result)

    def test_getBestOpponentLostGames_returns_int(self):
        mock_db.read_query_one.return_value = (5,)
        result = player_service.get_best_opponent_lost_games(FAKE_FULLNAME, 'Best Opponent')

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(5, result)

    def test_getWorstOpponentData_returns_emptyTuple_when_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = player_service.get_worst_opponent_data(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(tuple(), result)

    def test_getWorstOpponentData_returns_tupleWithOpponentAndLostGames_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = ('Worst Opponent', 13)
        result = player_service.get_worst_opponent_data(FAKE_FULLNAME)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(('Worst Opponent', 13), result)

    def test_getWorstOpponentLostGames_returns_int(self):
        mock_db.read_query_one.return_value = (1,)
        result = player_service.get_worst_opponent_lost_games(FAKE_FULLNAME, 'Best Opponent')

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(1, result)


    def test_hasScoredGames_returns_True_if_dataIsPresent(self):
        mock_db.read_query.return_value = [(5, 'Gogo', 'Pesho', 2, 1)]
        result = player_service.has_scored_games(FAKE_FULLNAME)

        mock_db.read_query.assert_called_once()
        self.assertTrue(result)

    def test_hasScoredGames_returns_False_if_noData(self):
        mock_db.read_query.return_value = []
        result = player_service.has_scored_games(FAKE_FULLNAME)

        mock_db.read_query.assert_called_once()
        self.assertFalse(result)

    def test_getExistingPlayerIdsFromFullname_returns_listOfInt_when_dataIsPresent(self):
        participants = [FAKE_FULLNAME, FAKE_FULLNAME, FAKE_FULLNAME]
        player_service.find_existing_players = lambda participants: [
            FAKE_FULLNAME, FAKE_FULLNAME, FAKE_FULLNAME
            ]

        mock_db.read_query.return_value = [(FAKE_PLAYER_ID,), (FAKE_PLAYER_ID,), (FAKE_PLAYER_ID,)]
        result = player_service.get_existing_player_ids_from_fullname(participants)

        mock_db.read_query.assert_called_once()
        mock_db.read_query_one.assert_not_called()
        self.assertEqual([FAKE_PLAYER_ID, FAKE_PLAYER_ID, FAKE_PLAYER_ID], result)
        self.assertEqual(len(result), 3)
        self.assertIsInstance(result[0], int)


    def test_getExistingPlayerIdsFromFullname_returns_listWithOneId_when_dataForOnePlayer(self):
        participants = [FAKE_FULLNAME, FAKE_FULLNAME]
        player_service.find_existing_players = lambda participants: [FAKE_FULLNAME]

        mock_db.read_query_one.return_value = (FAKE_PLAYER_ID,)
        result = player_service.get_existing_player_ids_from_fullname(participants)

        mock_db.read_query.assert_not_called()
        mock_db.read_query_one.assert_called_once()
        self.assertEqual(len(result), 1)
        self.assertIsInstance(result, list)


    def test_getExistingPlayerIdsFromFullname_returns_emptyList_when_noData(self):
        participants = [FAKE_FULLNAME, FAKE_FULLNAME]
        player_service.find_existing_players = lambda participants: []

        mock_db.read_query.return_value = []
        result = player_service.get_existing_player_ids_from_fullname(participants)

        mock_db.read_query.assert_not_called()
        mock_db.read_query_one.assert_not_called()
        self.assertEqual(len(result), 0)
        self.assertIsInstance(result, list)
