# pylint: disable=missing-docstring R0904 C0103 W0212

import unittest
from mariadb import IntegrityError

from tests.testdata import (
    FAKE_PASSWORD,
    FAKE_ROLE,
    FAKE_USER_ID,
    FAKE_EMAIL,
    FAKE_PLAYER_ID,
    FAKE_PAGINATION,
    INVALID_EMAIL,
    INVALID_PASSWORD,
)
from tests.testdata import fake_db
from data.models import User, UserBody, UserResponse
from services import user_service


mock_db = fake_db()


class UserService_Should(unittest.TestCase):
    def setUp(self) -> None:
        user_service.database = mock_db

    def tearDown(self) -> None:
        mock_db.reset_mock(return_value=True, side_effect=True)

    def test_all_returns_listOfUserResponse_when_dataIsPresent(self):
        row_1 = (FAKE_USER_ID, FAKE_EMAIL, FAKE_ROLE, FAKE_PLAYER_ID)
        row_2 = (FAKE_USER_ID, FAKE_EMAIL, FAKE_ROLE, FAKE_PLAYER_ID)
        mock_db.read_query.return_value = [row_1, row_2]
        result = list(user_service.get_all(**FAKE_PAGINATION))

        mock_db.read_query.assert_called_once()
        self.assertEqual(len(result), 2)
        self.assertIsInstance(result[0], UserResponse)

    def test_all_returns_None_when_noData(self):
        mock_db.read_query.return_value = []
        result = user_service.get_all(**FAKE_PAGINATION)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)

    def test_getById_returns_UserResponse_when_dataIsPresent(self):
        mock_db.read_query.return_value = [
            (FAKE_USER_ID, FAKE_EMAIL, FAKE_ROLE, FAKE_PLAYER_ID)
        ]
        result = user_service.get_by_id(FAKE_USER_ID)
        expected = UserResponse(
            id=FAKE_USER_ID,
            email=FAKE_EMAIL,
            role=FAKE_ROLE,
            player_id=FAKE_PLAYER_ID
        )

        mock_db.read_query.assert_called_once()
        self.assertEqual(expected, result)
        self.assertEqual(expected.id, result.id)
        self.assertEqual(expected.email, result.email)
        self.assertEqual(expected.role, result.role)
        self.assertEqual(expected.player_id, result.player_id)

    def test_getById_returns_None_when_noData(self):
        mock_db.read_query.return_value = []
        result = user_service.get_by_id(FAKE_USER_ID)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)


    def test_findByEmail_returns_User_when_dataIsPresent(self):
        mock_db.read_query.return_value = [
            (FAKE_USER_ID, FAKE_EMAIL, FAKE_PASSWORD, FAKE_ROLE)
        ]
        result = user_service.find_by_email(FAKE_EMAIL)
        expected = User(
            id=FAKE_USER_ID,
            email=FAKE_EMAIL,
            password=FAKE_PASSWORD,
            role=FAKE_ROLE,
        )

        mock_db.read_query.assert_called_once()
        self.assertEqual(expected, result)
        self.assertEqual(expected.id, result.id)
        self.assertEqual(expected.email, result.email)
        self.assertEqual(expected.password, result.password)
        self.assertEqual(expected.role, result.role)

    def test_findByEmail_returns_None_when_noData(self):
        mock_db.read_query.return_value = []
        result = user_service.find_by_email(FAKE_EMAIL)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)

    def test_findById_returns_User_when_dataIsPresent(self):
        mock_db.read_query.return_value = [
            (FAKE_USER_ID, FAKE_EMAIL, FAKE_PASSWORD, FAKE_ROLE)
        ]
        result = user_service.find_by_id(FAKE_USER_ID)
        expected = User(
            id=FAKE_USER_ID,
            email=FAKE_EMAIL,
            password=FAKE_PASSWORD,
            role=FAKE_ROLE,
        )

        mock_db.read_query.assert_called_once()
        self.assertEqual(expected, result)
        self.assertEqual(expected.id, result.id)
        self.assertEqual(expected.email, result.email)
        self.assertEqual(expected.password, result.password)
        self.assertEqual(expected.role, result.role)

    def test_findById_returns_None_when_noData(self):
        mock_db.read_query.return_value = []
        result = user_service.find_by_id(FAKE_USER_ID)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)

    def test_tryLogin_returns_None_when_validUsername_and_invalidPassword(self):
        mock_db.read_query.return_value = [
            (FAKE_USER_ID, FAKE_EMAIL, FAKE_PASSWORD, FAKE_ROLE)
        ]
        result = user_service.try_login(FAKE_EMAIL, INVALID_PASSWORD)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)

    def test_tryLogin_returns_None_when_invalidUsername_and_invalidPassword(self):
        mock_db.read_query.return_value = []
        result = user_service.try_login(INVALID_EMAIL, INVALID_PASSWORD)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)

    def test_tryLogin_returns_None_when_invalidUsername_and_validPassword(self):
        mock_db.read_query.return_value = []
        result = user_service.try_login(INVALID_EMAIL, FAKE_PASSWORD)

        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)

    def test_tryLogin_returns_User_when_validUsername_and_validPassword(self):
        user_service._hash_password = lambda password: FAKE_PASSWORD
        mock_db.read_query.return_value = [
            (FAKE_USER_ID, FAKE_EMAIL, FAKE_PASSWORD, FAKE_ROLE)
        ]

        result = user_service.try_login(FAKE_EMAIL, FAKE_PASSWORD)
        expected = User(
            id=FAKE_USER_ID,
            email=FAKE_EMAIL,
            password=FAKE_PASSWORD,
            role=FAKE_ROLE,
        )

        mock_db.read_query.assert_called_once()
        self.assertIsInstance(result, User)
        self.assertEqual(expected, result)

    def test_create_returns_UserResponse_when_emailIsUnique(self):
        mock_db.insert_query.return_value = FAKE_USER_ID
        result = user_service.create(FAKE_EMAIL, FAKE_PASSWORD)

        expected = UserResponse(
            id=FAKE_USER_ID, email=FAKE_EMAIL, role=FAKE_ROLE
        )

        mock_db.insert_query.assert_called_once()
        self.assertEqual(expected, result)

    def test_create_returns_None_when_emailIsDuplicate(self):
        mock_db.insert_query.side_effect = IntegrityError
        result = user_service.create(FAKE_EMAIL, FAKE_PASSWORD)

        mock_db.insert_query.assert_called_once()
        self.assertIsNone(result)

    def test_getUserEmailAssociatedWithPlayerId_returns_str_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = (FAKE_EMAIL,)
        result = user_service.get_user_email_associated_with_player_id(FAKE_PLAYER_ID)

        mock_db.read_query_one.assert_called_once()
        self.assertEqual(result, FAKE_EMAIL)

    def test_getUserEmailAssociatedWithPlayerId_returns_None_when_noData(self):
        mock_db.read_query_one.return_value = tuple()
        result = user_service.get_user_email_associated_with_player_id([FAKE_PLAYER_ID])

        mock_db.read_query_one.assert_called_once()
        mock_db.read_query.assert_not_called()
        self.assertIsNone(result)

    def test_getUserAssociatedWithPlayerId_returns_UserResponse_when_dataIsPresent(self):
        mock_db.read_query.return_value = [(FAKE_USER_ID, FAKE_EMAIL, FAKE_ROLE, FAKE_PLAYER_ID)]
        result = user_service.get_user_associated_with_player_id([FAKE_PLAYER_ID, FAKE_PLAYER_ID])
        expected = UserResponse(
            id=FAKE_USER_ID,
            email=FAKE_EMAIL,
            role=FAKE_ROLE,
            player_id=FAKE_PLAYER_ID
        )

        mock_db.read_query.assert_called_once()
        self.assertEqual(expected, result)
        self.assertEqual(expected.id, result.id)
        self.assertEqual(expected.email, result.email)
        self.assertEqual(expected.role, result.role)
        self.assertEqual(expected.player_id, result.player_id)

    def test_getUserAssociatedWithPlayerId_returns_None_when_noData(self):
        mock_db.read_query.return_value = []

        result = user_service.get_user_associated_with_player_id([FAKE_PLAYER_ID, FAKE_PLAYER_ID])
        mock_db.read_query.assert_called_once()
        self.assertIsNone(result)

    def test_update_returns_UserResponse_withChangedRole_when_newRoleAdmin(self):
        new = UserBody(role='admin')
        old = UserResponse(id=FAKE_USER_ID, email=FAKE_EMAIL, role=FAKE_ROLE)
        result = user_service.update(new, old)
        old.role = new.role
        expected = old

        mock_db.update_query.assert_called_once()
        mock_db.read_query.assert_not_called()
        self.assertEqual(expected, result)

    def test_update_returns_UserResponse_withChangedRole_when_newRoleDirector(self):
        new = UserBody(role='director')
        old = UserResponse(id=FAKE_USER_ID, email=FAKE_EMAIL, role=FAKE_ROLE)
        result = user_service.update(new, old)
        old.role = new.role
        expected = old

        mock_db.update_query.assert_called_once()
        mock_db.read_query.assert_not_called()
        self.assertEqual(expected, result)

    def test_update_returns_UserResponse_withChangedRole_when_newRoleUser(self):
        new = UserBody(role='director')
        old = UserResponse(id=FAKE_USER_ID, email=FAKE_EMAIL, role=FAKE_ROLE)
        result = user_service.update(new, old)
        old.role = new.role
        expected = old

        mock_db.update_query.assert_called_once()
        mock_db.read_query.assert_not_called()
        self.assertEqual(expected, result)

    def test_update_returns_UserResponse_withChangedPlayerId_when_newPlayerId(self):
        new = UserBody(player_id=FAKE_PLAYER_ID)
        old = UserResponse(id=FAKE_USER_ID, email=FAKE_EMAIL, role=FAKE_ROLE)
        result = user_service.update(new, old)
        old.player_id = new.player_id
        expected = old

        mock_db.update_query.assert_called_once()
        mock_db.read_query.assert_not_called()
        self.assertEqual(expected, result)
