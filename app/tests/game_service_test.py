# pylint: disable=missing-docstring C0103 C0301

import unittest
from unittest.mock import Mock

from common.responses import BadRequest
from data.models import Game, IndependentGame, SingleTournamentResponseModel, GameUpdate
from services import game_service
from tests.testdata import FAKE_FULLNAME
from tests.testdata import fake_db


mock_db = fake_db()
mock_func = Mock()
mock_tournament = Mock()


class GameService_Should(unittest.TestCase):
    def setUp(self) -> None:
        game_service.database = mock_db
        game_service.get_draw_and_win_points = mock_func
        game_service.tournament_service.get_by_id = mock_tournament

    def tearDown(self) -> None:
        mock_db.reset_mock(return_value=True, side_effect=True)
        mock_func.reset_mock(return_value=True, side_effect=True)
        mock_tournament.reset_mock(return_value=True, side_effect=True)

    def test_get_all_independent_returnsListOfIndependentGame_when_dataIsPresent(self):
        mock_db.read_query.return_value = [
            (98, None, FAKE_FULLNAME, None, None, FAKE_FULLNAME, 100, None, None),
            (99, None, FAKE_FULLNAME, 1, 0, FAKE_FULLNAME, 101, None, 'Sofia, Bulgaria')
        ]

        result = game_service.get_all_independent(0,0)

        expected = [
            IndependentGame(
                id=98, date_time=None, player1_name=FAKE_FULLNAME, score1=None, score2=None,
                player2_name=FAKE_FULLNAME, game_format_id=100, round=None, location=None),
            IndependentGame(
                id=99, date_time=None, player1_name=FAKE_FULLNAME, score1=1, score2=0,
                player2_name=FAKE_FULLNAME, game_format_id=101, round=None, location='Sofia, Bulgaria')
        ]

        self.assertEqual(2, len(result))
        self.assertEqual(expected, result)

    def test_get_all_independent_returnsNone_when_noDataIsPresent(self):
        mock_db.read_query.return_value = []
        result = game_service.get_all_independent(0,0)

        self.assertEqual(None, result)

    def test_get_by_id_returnsGame_when_dataIsPresent(self):
        mock_db.read_query_one.return_value = (
            99, None, 100, FAKE_FULLNAME, None, None, None, 101, 3, 'Sofia, Bulgaria'
            )

        result = game_service.get_by_id(0)

        expected = Game(
            id=99, datetime=None, tournament_id=100, player1_name=FAKE_FULLNAME, score1=None,
            score2=None, game_format_id=101, round=3, location='Sofia, Bulgaria'
            )

        self.assertEqual(expected, result)

    def test_get_by_id_returnsNone_when_noDataIsPresent(self):
        mock_db.read_query_one.return_value = ()

        result = game_service.get_by_id(0)

        self.assertEqual(None, result)

    def test_create_returnsIndependentGame_with_correctId(self):
        mock_db.insert_query.return_value = 99
        game = IndependentGame(
                id=98, date_time=None, player1_name=FAKE_FULLNAME, score1=None, score2=None,
                player2_name=FAKE_FULLNAME, game_format_id=100, round=None, location=None
            )

        result = game_service.create(game)

        expected = IndependentGame(
                id=99, date_time=None, player1_name=FAKE_FULLNAME, score1=None, score2=None,
                player2_name=FAKE_FULLNAME, game_format_id=100, round=None, location=None
            )

        self.assertEqual(expected, result)
        self.assertEqual(99, result.id)

    def test_update_returnsUpdatedGame_when_gameDoesNotBelongTo_aTournament(self):
        mock_db.read_query_one.return_value = (0,)
        tournament_id = None

        old = Game(
            id=99, datetime=None, tournament_id=tournament_id, player1_name=FAKE_FULLNAME,
            score1=None, score2=None, player2_name=FAKE_FULLNAME, game_format_id=101, round=None,
            location=None)

        new = GameUpdate(datetime=None, score_player_1=1, score_player_2=0, location='Sofia, Bulgaria')

        result = game_service.update(old, new)

        expected = Game(
            id=99, datetime=None, tournament_id=tournament_id, player1_name=FAKE_FULLNAME,
            score1=1, score2=0, player2_name=FAKE_FULLNAME, game_format_id=101, round=None,
            location='Sofia, Bulgaria'
        )

        self.assertEqual(expected, result)
        mock_db.update_query.assert_called_once()

    def test_update_returnsUpdatedGame_when_gameBelongsTo_aLeagueTournament(self):
        mock_db.read_query_one.return_value = (0,)
        mock_func.return_value = (10, 5)
        mock_tournament.return_value = SingleTournamentResponseModel(
            id=None, title='Tournament', format='League', game_format=100,
            number_of_rounds=2, start_date=None, end_date=None, location=None,
            prizes=None, number_of_games=2, winner=None, participants=[FAKE_FULLNAME, FAKE_FULLNAME]
        )

        old = Game(
            id=99, datetime=None, tournament_id=9999, player1_name=FAKE_FULLNAME,
            score1=None, score2=None, player2_name=FAKE_FULLNAME, game_format_id=101, round=None,
            location=None)

        new = GameUpdate(datetime=None, score_player_1=1, score_player_2=0, location='Sofia, Bulgaria')

        result = game_service.update(old, new)

        expected = Game(
            id=99, datetime=None, tournament_id=9999, player1_name=FAKE_FULLNAME,
            score1=1, score2=0, player2_name=FAKE_FULLNAME, game_format_id=101, round=None,
            location='Sofia, Bulgaria'
        )

        self.assertEqual(expected, result)
        mock_func.assert_called_once()
        mock_tournament.assert_called_once()

    def test_update_returnsUpdatedGame_when_gameBelongsTo_aKnockoutTournament(self):
        mock_db.read_query_one.return_value = (0,)
        mock_func.return_value = (10, 5)
        mock_tournament.return_value = SingleTournamentResponseModel(
            id=None, title='Tournament', format='Knockout', game_format=100,
            number_of_rounds=2, start_date=None, end_date=None, location=None,
            prizes=None, number_of_games=2, winner=None, participants=[FAKE_FULLNAME, FAKE_FULLNAME]
        )

        old = Game(
            id=99, datetime=None, tournament_id=9999, player1_name=FAKE_FULLNAME,
            score1=None, score2=None, player2_name=FAKE_FULLNAME, game_format_id=101, round=None,
            location=None)

        new = GameUpdate(datetime=None, score_player_1=1, score_player_2=0, location='Sofia, Bulgaria')

        result = game_service.update(old, new)

        expected = Game(
            id=99, datetime=None, tournament_id=9999, player1_name=FAKE_FULLNAME,
            score1=1, score2=0, player2_name=FAKE_FULLNAME, game_format_id=101, round=None,
            location='Sofia, Bulgaria'
        )

        self.assertEqual(expected, result)
        mock_tournament.assert_called_once()
        mock_func.assert_not_called()

    def test_update_returnsUpdatedGame_when_gameBelongsTo_aLeagueTournament_and_thereIsADraw(self):
        mock_db.read_query_one.return_value = (0,)
        mock_func.return_value = (10, 5)
        mock_tournament.return_value = SingleTournamentResponseModel(
            id=None, title='Tournament', format='League', game_format=100,
            number_of_rounds=2, start_date=None, end_date=None, location=None,
            prizes=None, number_of_games=2, winner=None, participants=[FAKE_FULLNAME, FAKE_FULLNAME]
        )

        old = Game(
            id=99, datetime=None, tournament_id=9999, player1_name=FAKE_FULLNAME,
            score1=None, score2=None, player2_name=FAKE_FULLNAME, game_format_id=101, round=None,
            location=None)

        new = GameUpdate(datetime=None, score_player_1=1, score_player_2=1, location='Sofia, Bulgaria')

        result = game_service.update(old, new)

        expected = Game(
            id=99, datetime=None, tournament_id=9999, player1_name=FAKE_FULLNAME,
            score1=1, score2=1, player2_name=FAKE_FULLNAME, game_format_id=101, round=None,
            location='Sofia, Bulgaria'
        )

        self.assertEqual(expected, result)
        mock_func.assert_called_once()
        mock_tournament.assert_called_once()

    def test_update_returnsBadRequest_when_gameBelongsTo_aKnockoutTournament_and_thereIsADraw(self):
        mock_db.read_query_one.return_value = (0,)
        mock_func.return_value = (10, 5)
        mock_tournament.return_value = SingleTournamentResponseModel(
            id=None, title='Tournament', format='Knockout', game_format=100,
            number_of_rounds=2, start_date=None, end_date=None, location=None,
            prizes=None, number_of_games=2, winner=None, participants=[FAKE_FULLNAME, FAKE_FULLNAME]
        )

        old = Game(
            id=99, datetime=None, tournament_id=9999, player1_name=FAKE_FULLNAME,
            score1=None, score2=None, player2_name=FAKE_FULLNAME, game_format_id=101, round=None,
            location=None)

        new = GameUpdate(datetime=None, score_player_1=1, score_player_2=1, location='Sofia, Bulgaria')

        result = game_service.update(old, new)

        expected = BadRequest

        self.assertEqual(expected, type(result))
        mock_tournament.assert_called_once()
        mock_func.assert_not_called()
