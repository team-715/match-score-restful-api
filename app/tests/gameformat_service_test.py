# pylint: disable=missing-docstring C0103

import unittest

from common.responses import BadRequest
from data.models import GameFormat, GameFormatResponseModel
from services import gameformat_service
from tests.testdata import fake_db


mock_db = fake_db()


class GameFormatService_Should(unittest.TestCase):
    def setUp(self) -> None:
        gameformat_service.database = mock_db

    def tearDown(self) -> None:
        mock_db.reset_mock(return_value=True, side_effect=True)

    def test_get_all_returnsListOfGameFormatResponseModel_when_dataIsPresent(self):
        mock_db.read_query.return_value = [
            (98, None, 10, 0, 0.5, 1),
            (99, 90, None, 0, 1, 3)
        ]

        result = gameformat_service.get_all()

        expected = [
            GameFormatResponseModel(
                id=98, time_limit=None, score_limit=10,
                loss_points=0, draw_points=0.5, win_points=1),
            GameFormatResponseModel(
                id=99, time_limit=90, score_limit=None,
                loss_points=0, draw_points=1, win_points=3)
        ]

        self.assertEqual(2, len(result))
        self.assertEqual(expected, result)

    def test_get_all_returnsNone_when_noDataIsPresent(self):
        mock_db.read_query.return_value = []
        result = gameformat_service.get_all()

        self.assertEqual(None, result)

    def test_get_by_id_returnsGameFormatResponseModel_when_dataIsPresent(self):
        mock_db.read_query.return_value = [(99, 90, None, 0, 1, 3)]

        result = gameformat_service.get_by_id(0)

        expected = GameFormatResponseModel(
            id=99, time_limit=90, score_limit=None, loss_points=0, draw_points=1, win_points=3)

        self.assertEqual(expected, result)

    def test_get_by_id_returnsNone_when_noDataIsPresent(self):
        mock_db.read_query.return_value = []

        result = gameformat_service.get_by_id(0)

        self.assertEqual(None, result)

    def test_create_returnsBadRequest_when_bothTimeLimitAndScoreLimit_areNone(self):
        game_format = GameFormat(
            id=99, time_limit=None, score_limit=None, loss_points=0, draw_points=1, win_points=3
            )

        result = gameformat_service.create(game_format)

        expected = BadRequest

        self.assertEqual(expected, type(result))
        mock_db.insert_query.assert_not_called()

    def test_create_returnsGameFormat_with_correctId_when_validInputData(self):
        mock_db.insert_query.return_value = 99

        game_format = GameFormat(
            id=1, time_limit=90, score_limit=None, loss_points=0, draw_points=1, win_points=3)

        expected = GameFormat(
            id=99, time_limit=90, score_limit=None, loss_points=0, draw_points=1, win_points=3)

        result = gameformat_service.create(game_format)

        self.assertEqual(expected, result)
        self.assertEqual(99, result.id)

    def test_get_draw_and_win_points_returnsData_when_DataIsPresent(self):
        mock_db.read_query_one.return_value = (0.5, 1)

        draw_pts, win_pts = gameformat_service.get_draw_and_win_points(0)

        self.assertEqual(0.5, draw_pts)
        self.assertEqual(1, win_pts)

    def test_get_draw_and_win_points_returnsNone_when_NoDataIsPresent(self):
        mock_db.read_query_one.return_value = ()

        result = gameformat_service.get_draw_and_win_points(0)

        self.assertEqual(None, result)
