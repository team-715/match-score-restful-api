# pylint: disable=missing-docstring C0103

import unittest
from common import utils


class CommonUtils_Should(unittest.TestCase):
    def test_constructSqlForInOperator_returns_correctStr_forListOfValues(self):
        result = utils.construct_sql_for_in_operator([1, 2, 3])
        expected = '1, 2, 3'

        self.assertEqual(result, expected)

    def test_isEven_returnsTrue_ifEven(self):
        result = utils.is_even(8)

        self.assertTrue(result)

    def test_isEven_returnsFalse_ifOdd(self):
        result = utils.is_even(9)

        self.assertFalse(result)

    def test_isAPowerOfTwo_returnsTrue_ifPowerOfTwo(self):
        result = utils.is_a_power_of_two(8)

        self.assertTrue(result)

    def test_isAPowerOfTwo_returnsFalse_ifNotPowerOfTwo(self):
        result = utils.is_even(9)

        self.assertFalse(result)

    def test_popRandom_returns_oneItemOfTypeInt(self):
        test_list = [1, 2, 3, 4]
        len_before_operation = len(test_list)
        result = utils.pop_random(test_list)

        self.assertEqual(len(test_list), len_before_operation-1)
        self.assertIsInstance(result, int)
