# pylint: disable=missing-docstring C0103 R0904 C0301

import unittest

from common.responses import BadRequest
from data.models import Tournament, TournamentResponseModel, TournamentUpdate, AllTournamentResponseModel, SingleTournamentResponseModel
from data.models import GameResponseModel, TournamentStandingsResponse
from services import tournament_service
from tests.testdata import fake_db


mock_db = fake_db()
insert_tournament_ref = tournament_service.insert_tournament
insert_tournament_players_ref = tournament_service.insert_tournament_players
insert_league_games_ref = tournament_service.insert_league_games
insert_knockout_games_ref = tournament_service.insert_knockout_games
get_by_id_ref = tournament_service.get_by_id
find_phase_from_round_ref = tournament_service.find_phase_from_round
find_round_ref = tournament_service.find_round
generate_league_rounds_2leg_ref = tournament_service.generate_league_rounds_2leg

class TournamentService_Should(unittest.TestCase):
    def setUp(self) -> None:
        tournament_service.database = mock_db

    def tearDown(self) -> None:
        mock_db.reset_mock(return_value=True, side_effect=True)

    def test_all_returnsListOfAllTournamentResponseModel_when_dataIsPresent(self):
        mock_db.read_query.return_value = [
            (98, 'World Chess Championship 1972', 0, 100, None, None, 'Reykjavik Iceland', 'World Champion title', 'Bobby Fischer'),
            (99, 'World Chess Championship 2013', 0, 101, None, None, 'Chennai, India', 'World Champion title', 'Magnus Carlsen')
        ]

        result = tournament_service.get_all('', 0, 0)

        expected = [
            AllTournamentResponseModel(id=98, title='World Chess Championship 1972', format=0, game_format=100, start_date=None,
            end_date=None, location='Reykjavik Iceland', prizes='World Champion title', winner='Bobby Fischer'),
            AllTournamentResponseModel(id=99, title='World Chess Championship 2013', format=0, game_format=101, start_date=None,
            end_date=None, location='Chennai, India', prizes='World Champion title', winner='Magnus Carlsen')
        ]

        self.assertEqual(2, len(result))
        self.assertEqual(expected, result)

    def test_all_returnsNone_when_noDataIsPresent(self):
        mock_db.read_query.return_value = []

        result = tournament_service.get_all('', 0, 0)

        self.assertEqual(None, result)

    def test_get_by_id_returnsSingleTournamentResponseModel_withCorrectNumberOfGames_when_dataIsPresent_forLeague(self):
        mock_db.read_query.return_value = [
            (98, 'Tournament', 0, 100, 2, None, None, None, None, None, 'Magnus Carlsen'),
            (98, 'Tournament', 0, 100, 2, None, None, None, None, None, 'Hikaru Nakamura')
        ]

        tournament_service.get_by_id = get_by_id_ref
        result = tournament_service.get_by_id(0)

        expected = SingleTournamentResponseModel(
            id=98, title='Tournament', format='League', game_format=100, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=2,
            winner=None, participants=['Magnus Carlsen', 'Hikaru Nakamura']
        )

        self.assertEqual(SingleTournamentResponseModel, type(result))
        self.assertEqual(expected, result)

    def test_get_by_id_returnsSingleTournamentResponseModel_withCorrectNumberOfGames_when_dataIsPresent_forKnockout(self):
        mock_db.read_query.return_value = [
            (98, 'Tournament', 1, 100, 2, None, None, None, None, None, 'Magnus Carlsen'),
            (98, 'Tournament', 1, 100, 2, None, None, None, None, None, 'Hikaru Nakamura'),
            (98, 'Tournament', 1, 100, 2, None, None, None, None, None, 'Bobby Fischer'),
            (98, 'Tournament', 1, 100, 2, None, None, None, None, None, 'Daniil Dubov')
        ]

        tournament_service.get_by_id = get_by_id_ref
        result = tournament_service.get_by_id(0)

        expected = SingleTournamentResponseModel(
            id=98, title='Tournament', format='Knockout', game_format=100, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=3,
            winner=None, participants=['Magnus Carlsen', 'Hikaru Nakamura', 'Bobby Fischer', 'Daniil Dubov']
        )

        self.assertEqual(SingleTournamentResponseModel, type(result))
        self.assertEqual(expected, result)

    def test_get_by_id_returnsNone_when_noDataIsPresent(self):
        mock_db.read_query.return_value = []

        tournament_service.get_by_id = get_by_id_ref
        result = tournament_service.get_by_id(0)

        self.assertEqual(None, result)

    def test_insert_tournament_players_executesInsertQuery(self):
        participants = ['Magnus Carlsen', 'Hikaru Nakamura']

        # tournament_service.insert_tournament_players = insert_tournament_players_ref
        # tournament_service.insert_tournament_players(participants, 0)
        insert_tournament_players_ref(participants, 0)

        mock_db.insert_query.assert_called()
        mock_db.insert_query.assert_called_once()

    def test_insert_tournament_executesInsertQuery(self):
        tournament = Tournament(
            id=0, participants=[], title='Tournament', format=0, game_format=0, start_date=None,
            end_date=None, location=None, prizes=None
            )
        tournament_service.insert_tournament = insert_tournament_ref
        tournament_service.insert_tournament(tournament, 0)

        mock_db.insert_query.assert_called()
        mock_db.insert_query.assert_called_once()

    def test_generate_league_rounds_2leg_generatesCorrectPairings(self):
        players = ['Magnus Carlsen', 'Hikaru Nakamura']

        tournament_service.generate_league_rounds_2leg = generate_league_rounds_2leg_ref
        result = list((tournament_service.generate_league_rounds_2leg(players, 2)))

        expected = [[('Magnus Carlsen', 'Hikaru Nakamura')], [('Hikaru Nakamura', 'Magnus Carlsen')]]

        self.assertEqual(expected, result)

    def test_find_round_returnsCorrectInteger_dependingOnTheInput(self):
        tournament_service.find_round = find_round_ref
        result1 = tournament_service.find_round(2)
        result2 = tournament_service.find_round(3)
        result3 = tournament_service.find_round(7)
        result4 = tournament_service.find_round(16)
        result5 = tournament_service.find_round(32)

        self.assertEqual(1, result1)
        self.assertEqual(2, result2)
        self.assertEqual(3, result3)
        self.assertEqual(4, result4)
        self.assertEqual(5, result5)

    def test_find_round_failsWithValueError_when_inputLessThan2(self):
        tournament_service.find_round = find_round_ref
        with self.assertRaises(ValueError):
            tournament_service.find_round(1)

    def test_find_phase_from_round_returnsCorrectString_dependingOnTheInput(self):
        tournament_service.find_phase_from_round = find_phase_from_round_ref
        result1 = tournament_service.find_phase_from_round(1)
        result2 = tournament_service.find_phase_from_round(2)
        result3 = tournament_service.find_phase_from_round(3)
        result4 = tournament_service.find_phase_from_round(4)
        result5 = tournament_service.find_phase_from_round(5)

        self.assertEqual('Final', result1)
        self.assertEqual('Semi Final', result2)
        self.assertEqual('Quarter Final', result3)
        self.assertEqual('1/8 Final', result4)
        self.assertEqual('1/16 Final', result5)

    def test_update_returnsUpdatedTournament_when_noNewWinner(self):
        old = SingleTournamentResponseModel(
            id=99, title='Tournament', format='League', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=2,
            winner=None, participants=['Magnus Carlsen', 'Hikaru Nakamura']
        )
        new = TournamentUpdate(
            title='Updated Tournament', start_date=None, end_date=None, location='Sofia, Bulgaria',
            prizes='1st: 10 000$, 2nd: 5 000$', winner=None
        )
        expected = SingleTournamentResponseModel(
            id=99, title='Updated Tournament', format='League', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location='Sofia, Bulgaria', prizes='1st: 10 000$, 2nd: 5 000$',
            number_of_games=2, winner=None, participants=['Magnus Carlsen', 'Hikaru Nakamura']
        )
        result = tournament_service.update(old, new)

        self.assertEqual(expected, result)
        self.assertEqual(SingleTournamentResponseModel, type(result))
        mock_db.update_query.assert_called_once()
        mock_db.read_query.assert_not_called()

    def test_update_returnsUpdatedTournament_when_newWinner_andWinnerExistsInDB(self):
        mock_db.read_query.return_value = [0]

        old = SingleTournamentResponseModel(
            id=99, title='Tournament', format='League', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=2,
            winner=None, participants=['Magnus Carlsen', 'Hikaru Nakamura']
        )
        new = TournamentUpdate(
            title='Updated Tournament', start_date=None, end_date=None, location='Sofia, Bulgaria',
            prizes='1st: 10 000$, 2nd: 5 000$', winner='Hikaru Nakamura'
        )
        expected = SingleTournamentResponseModel(
            id=99, title='Updated Tournament', format='League', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location='Sofia, Bulgaria', prizes='1st: 10 000$, 2nd: 5 000$',
            number_of_games=2, winner='Hikaru Nakamura', participants=['Magnus Carlsen', 'Hikaru Nakamura']
        )
        result = tournament_service.update(old, new)

        self.assertEqual(expected, result)
        self.assertEqual(SingleTournamentResponseModel, type(result))
        mock_db.update_query.assert_called_once()
        mock_db.read_query.assert_called_once()

    def test_update_returnsBadRequest_when_newWinner_butWinnerDoesNotExistInDB(self):
        mock_db.read_query.return_value = []

        old = SingleTournamentResponseModel(
            id=99, title='Tournament', format='League', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=2,
            winner=None, participants=['Magnus Carlsen', 'Hikaru Nakamura']
        )
        new = TournamentUpdate(
            title='Updated Tournament', start_date=None, end_date=None, location='Sofia, Bulgaria',
            prizes='1st: 10 000$, 2nd: 5 000$', winner='Hikaru Nakamura'
        )
        expected = BadRequest
        result = tournament_service.update(old, new)

        self.assertEqual(expected, type(result))
        mock_db.update_query.assert_not_called()
        mock_db.read_query.assert_called_once()

    def test_create_knockout_returnsTournamentResponseModel(self):
        tournament_service.insert_tournament = lambda participants, id: 99
        tournament_service.insert_tournament_players = lambda participants, id: None
        tournament_service.insert_knockout_games = lambda participants, id, format, location: None

        tournament = Tournament(
            id=101, participants=['Bobby Fischer', 'Magnus Carlsen'], title='Tournament',
            format=1, game_format=0, start_date=None, end_date=None,
            location=None, prizes=None
        )
        expected = TournamentResponseModel(
            id=99, participants=['Bobby Fischer', 'Magnus Carlsen'], title='Tournament',
            format="Knockout", game_format=0, rounds=1, start_date=None, end_date=None,
            location=None, prizes=None
        )
        result = tournament_service.create_knockout(tournament)

        self.assertEqual(expected, result)

    def test_create_league_returnsTournamentResponseModel(self):
        tournament_service.insert_league_games = lambda participants, id, g_format: None
        tournament_service.insert_tournament = lambda participants, id: 99
        tournament_service.insert_tournament_players = lambda participants, id: None

        tournament = Tournament(
            id=101, participants=['Bobby Fischer', 'Magnus Carlsen'], title='Tournament',
            format=0, game_format=0, start_date=None, end_date=None,
            location=None, prizes=None
        )
        expected = TournamentResponseModel(
            id=99, participants=['Bobby Fischer', 'Magnus Carlsen'], title='Tournament',
            format="League", game_format=0, rounds=2, start_date=None, end_date=None,
            location=None, prizes=None
        )
        result = tournament_service.create_league(tournament)

        self.assertEqual(expected, result)

    def test_get_games_from_round_returnsListOfGameResponseModel_withCorrectLeagueRound_when_dataIsPresent(self):
        tournament_service.get_by_id = lambda id: SingleTournamentResponseModel(
            id=99, title='Tournament', format='League', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=2,
            winner=None, participants=['Magnus Carlsen', 'Hikaru Nakamura']
        )
        mock_db.read_query.return_value = [
            (98, None, 9, 'Bobby Fischer', 1, 0, 'Garry Kasparov', 0, 1, None)
        ]
        expected = [
            GameResponseModel(id=98, datetime=None, tournament_id=9, player1_name='Bobby Fischer',
            score1=1, score2=0, player2_name='Garry Kasparov', game_format_id=0, round=1, location=None)
        ]
        result = tournament_service.get_games_from_round(0,0)

        self.assertEqual(expected, result)
        self.assertEqual(list, type(result))
        self.assertEqual(1, result[0].round)

    def test_get_games_from_round_returnsListOfGameResponseModel_withCorrectKnockoutPhase_when_dataIsPresent(self):
        tournament_service.find_phase_from_round = lambda n: 'Final'
        tournament_service.get_by_id = lambda id: SingleTournamentResponseModel(
            id=99, title='Tournament', format='Knockout', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=2,
            winner='Bobby Fischer', participants=['Bobby Fischer', 'Garry Kasparov']
        )
        mock_db.read_query.return_value = [
            (98, None, 9, 'Bobby Fischer', 1, 0, 'Garry Kasparov', 0, 1, None)
        ]
        expected = [
            GameResponseModel(id=98, datetime=None, tournament_id=9, player1_name='Bobby Fischer',
            score1=1, score2=0, player2_name='Garry Kasparov', game_format_id=0, round='Final', location=None)
        ]
        result = tournament_service.get_games_from_round(0,0)

        self.assertEqual(expected, result)
        self.assertEqual(list, type(result))
        self.assertEqual('Final', result[0].round)

    def test_get_games_from_round_returnsNone_when_noDataIsPresent(self):
        mock_db.read_query.return_value = []

        result = tournament_service.get_games_from_round(0,0)

        self.assertEqual(None, result)

    def test_get_current_standings_returnsNone_when_tournamentIsNotLeague(self):
        tournament_service.get_by_id = lambda id: SingleTournamentResponseModel(
            id=99, title='Tournament', format='Knockout', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=2,
            winner='Bobby Fischer', participants=['Bobby Fischer', 'Garry Kasparov']
        )
        result = tournament_service.get_current_standings(0)

        self.assertEqual(None, result)

    def test_get_current_standings_returnsNone_when_tournamentIsLeague_butNoDataIsPresent(self):
        mock_db.read_query.return_value = []
        tournament_service.get_by_id = lambda id: SingleTournamentResponseModel(
            id=99, title='Tournament', format='League', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=2,
            winner='Bobby Fischer', participants=['Bobby Fischer', 'Garry Kasparov']
        )
        result = tournament_service.get_current_standings(0)

        self.assertEqual(None, result)
        mock_db.read_query.assert_called_once()

    def test_get_current_standings_returnsCorrectResponse_when_tournamentIsLeague_andDataIsPresent(self):
        tournament_service.get_by_id = lambda id: SingleTournamentResponseModel(
            id=99, title='Tournament', format='League', game_format=0, number_of_rounds=2,
            start_date=None, end_date=None, location=None, prizes=None, number_of_games=2,
            winner='Bobby Fischer', participants=['Bobby Fischer', 'Garry Kasparov']
        )
        mock_db.read_query.return_value = [
            ('Bobby Fischer', 12.5), ('Garry Kasparov', 1.5)
        ]
        expected = [
            TournamentStandingsResponse(place=1, player_name='Bobby Fischer', points=12.5),
            TournamentStandingsResponse(place=2, player_name='Garry Kasparov', points=1.5)
        ]
        result = tournament_service.get_current_standings(0)

        self.assertEqual(expected, result)
        mock_db.read_query.assert_called_once()

    def test_insert_league_games_executesInsertQuery(self):
        tournament_service.generate_league_rounds_2leg = lambda participants, sets:[
            [('Magnus Carlsen', 'Hikaru Nakamura')], [('Hikaru Nakamura', 'Magnus Carlsen')]
        ]

        participants = ['Magnus Carlsen', 'Hikaru Nakamura']
        tournament_service.insert_league_games = insert_league_games_ref
        tournament_service.insert_league_games(participants, 0, 0)

        mock_db.insert_query.assert_called()
        mock_db.insert_query.assert_called_once()

    def test_insert_knockout_games_executesInsertQuery_andReadQuery(self):
        tournament_service.find_round = lambda n: 1
        tournament_service.pop_random = lambda lst: 0
        mock_db.read_query.return_value = [(0,)]

        tournament_service.insert_knockout_games = insert_knockout_games_ref
        tournament_service.insert_knockout_games([], 0, 0, None)

        mock_db.insert_query.assert_called()
        mock_db.read_query.assert_called()
        mock_db.read_query.assert_called_once()
