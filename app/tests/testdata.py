# pylint: disable=missing-docstring C0103

from datetime import datetime
from modulefinder import Module
from unittest.mock import Mock
from data.models import User, LoginData


FAKE_PLAYER_ID = 9
FAKE_USER_ID = 2
FAKE_TOKEN = "Token"  # is also invalid
FAKE_DATETIME = Mock(spec=datetime)
FAKE_EMAIL = "test@email.com"
FAKE_PASSWORD = "pass1234"
FAKE_LOGIN_DATA = Mock(spec=LoginData, email=FAKE_EMAIL,
                       password=FAKE_PASSWORD)
FAKE_ROLE = "user"
FAKE_PAGINATION = {"limit": 10, "offset": 0}
FAKE_PLAYER_ID = 7
FAKE_FULLNAME = "Naoko Takemoto"
FAKE_COUNTRY = "Japan"
FAKE_CLUB = "YAKUZA"
FAKE_SEARCH_COUNTRY_AND_CLUB = {"country": FAKE_COUNTRY, "club": FAKE_CLUB}
INVALID_EMAIL = "no_such@email.com"
INVALID_PASSWORD = "NoMatch"
INVALID_LOGIN_DATA = Mock(
    spec=LoginData, email=INVALID_EMAIL, password=INVALID_PASSWORD
)


def fake_db() -> Mock:
    mock_db = Mock()

    return mock_db


def fake_user(user_id: int = FAKE_USER_ID, email: str = FAKE_EMAIL) -> Mock:
    user = Mock(spec=User)
    user.id = user_id
    user.email = email

    return user


def fake_user_authentication(module: Module) -> Mock:
    fake_user_ = Mock()
    fake_user_.id = FAKE_USER_ID
    fake_user_.email = FAKE_EMAIL
    module.get_user_or_raise_401 = Mock()
    module.get_user_or_raise_401.return_value = fake_user_

    return fake_user_


def fake_get_user_or_raise_401(is_admin: bool = False):
    mock_user = Mock()
    if is_admin is True:
        mock_user.is_admin = lambda: True
    else:
        mock_user.is_admin = lambda: False

    mock_get_user_or_raise_401 = Mock()
    mock_get_user_or_raise_401.return_value = mock_user

    return mock_get_user_or_raise_401


def fake_params_player() -> dict:
    return {"fullname": FAKE_FULLNAME, "country": FAKE_COUNTRY, "club": FAKE_CLUB}
