# pylint: disable=missing-docstring R0904 C0103 C0103

import unittest
from unittest.mock import Mock
from fastapi.exceptions import HTTPException
from common.responses import BadRequest
from tests.testdata import (
    FAKE_LOGIN_DATA,
    FAKE_TOKEN,
    FAKE_USER_ID,
    FAKE_EMAIL,
    INVALID_LOGIN_DATA,
)
from tests.testdata import fake_user

from data.models import Token, User
from routers import users as users_router


mock_user_service = Mock(spec="services.user_service")
mock_category_service = Mock(spec="services.category_service")
mock_authorization = Mock()


class UsersRouter_Should(unittest.TestCase):
    def setUp(self) -> None:
        users_router.user_service = mock_user_service
        # users_router.category_service = mock_category_service
        users_router.get_user_or_raise_401 = mock_authorization

    def tearDown(self) -> None:
        mock_user_service.reset_mock(return_value=True, side_effect=True)
        # mock_category_service.reset_mock(return_value=True, side_effect=True)
        mock_authorization.reset_mock(return_value=True, side_effect=True)

    def test_login_returns_Token_when_validLoginData(self):
        mock_user_service.try_login = lambda email, password: fake_user()
        users_router.create_token = lambda user: Mock(spec=Token)

        result = users_router.login(FAKE_LOGIN_DATA)

        self.assertIsInstance(result, Token)

    def test_login_returns_BadRequest_when_invalidLoginData(self):
        mock_user_service.try_login = lambda email, password: None

        result = users_router.login(INVALID_LOGIN_DATA)

        self.assertIsInstance(result, BadRequest)
        self.assertEqual(400, result.status_code)
        self.assertEqual(b"Invalid login data", result.body)

    def test_register_returns_User_when_validLoginData(self):
        mock_user_service.create = lambda email, password: fake_user()

        result = users_router.register(FAKE_LOGIN_DATA)

        self.assertIsInstance(result, User)
        self.assertEqual(FAKE_EMAIL, result.email)
        self.assertEqual(FAKE_USER_ID, result.id)

    def test_register_returns_BadRequest_when_emailIsTaken(self):
        mock_user_service.create = lambda email, password: None

        result = users_router.register(FAKE_LOGIN_DATA)

        self.assertIsInstance(result, BadRequest)
        self.assertEqual(400, result.status_code)
        self.assertEqual(b"User with email: test@email.com already exists", result.body)

    def test_userInfo_returns_User_when_validToken(self):
        mock_authorization_ = Mock()
        users_router.get_user_or_raise_401 = mock_authorization_
        mock_authorization_.return_value = fake_user()

        result = users_router.user_info(FAKE_TOKEN)

        self.assertIsInstance(result, User)

    def test_userInfo_fails_when_invalidToken(self):
        mock_authorization.side_effect = HTTPException(status_code=401)
        with self.assertRaises(HTTPException):
            users_router.user_info(token="INVALID_TOKEN")
