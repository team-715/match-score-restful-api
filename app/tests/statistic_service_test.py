# pylint: disable=missing-docstring C0103 C0301

import unittest
from unittest.mock import Mock

from data.models import Player, PlayerExtended
from services import statistic_service
from tests.testdata import (
    FAKE_FULLNAME,
    FAKE_PLAYER_ID
)
from tests.testdata import fake_params_player

mock_service = Mock()


class StatisticService_Should(unittest.TestCase):
    def setUp(self) -> None:
        statistic_service.player_service = mock_service

    def tearDown(self) -> None:
        mock_service.reset_mock(return_value=True, side_effect=True)

    def test_attachStatistics_returns_PlayerExtended_withDefaultValues_whenHasNoScoredGames(self):
        player_short = Player(id=FAKE_PLAYER_ID, **fake_params_player())
        mock_service.has_scored_games.return_value = False

        result = statistic_service.attach_statistics(player_short)
        expected = PlayerExtended(**(player_short.dict()))

        mock_service.has_scored_games.assert_called_once()

        mock_service.get_tournaments_played.assert_not_called()
        mock_service.get_tournaments_won.assert_not_called()
        mock_service.get_matches_played.assert_not_called()
        mock_service.get_matches_won.assert_not_called()
        mock_service.get_most_often_played_opponent_data.assert_not_called()
        mock_service.get_most_rare_played_opponent_data.assert_not_called()
        mock_service.get_best_opponent_data.assert_not_called()
        mock_service.get_best_opponent_lost_games.assert_not_called()
        mock_service.get_worst_opponent_data.assert_not_called()
        mock_service.get_worst_opponent_lost_games.assert_not_called()

        self.assertEqual(expected, result)

    def test_attachStatistics_returns_PlayerExtended_updatedWithQueriedInfo_whenHasScoredGames_andHasLostGames(self):
        player_short = Player(id=FAKE_PLAYER_ID, **fake_params_player())
        mock_service.has_scored_games.return_value = True

        mock_service.get_tournaments_played.return_value = 2
        mock_service.get_tournaments_won.return_value = 0
        mock_service.get_matches_played.return_value = 22
        mock_service.get_matches_won.return_value = 14
        mock_service.get_most_often_played_opponent_data.return_value = [
            "Aleksander Grischuk", 5]
        mock_service.get_most_rare_played_opponent_data.return_value = [
            "Sun Pin", 1]
        mock_service.get_best_opponent_data.return_value = [
            "Aleksander Grischuk", 1]
        mock_service.get_best_opponent_lost_games.return_value = 4
        mock_service.get_worst_opponent_data.return_value = [
            "Garry Kasparov", 2]
        mock_service.get_worst_opponent_lost_games.return_value = 2

        result = statistic_service.attach_statistics(player_short)

        mock_service.has_scored_games.assert_called_once()
        mock_service.get_tournaments_played.assert_called_once()
        mock_service.get_tournaments_won.assert_called_once()
        mock_service.get_matches_played.assert_called_once()
        mock_service.get_matches_won.assert_called_once()
        mock_service.get_most_often_played_opponent_data.assert_called_once()
        mock_service.get_most_rare_played_opponent_data.assert_called_once()
        mock_service.get_best_opponent_data.assert_called_once()
        mock_service.get_best_opponent_lost_games.assert_called_once()
        mock_service.get_worst_opponent_data.assert_called_once()
        mock_service.get_worst_opponent_lost_games.assert_called_once()

        self.assertEqual(result.tournaments_won, 0)
        self.assertEqual(result.tournaments_played, 2)
        self.assertEqual(result.matches_played, 22)
        self.assertEqual(result.matches_won, 14)
        self.assertEqual(result.most_often_played_opponent,
                         "Aleksander Grischuk")
        self.assertEqual(result.total_games_with_most_often_played_opponent, 5)
        self.assertEqual(result.most_rare_played_opponent, "Sun Pin")
        self.assertEqual(result.total_games_with_most_rare_played_opponent, 1)
        self.assertEqual(result.best_opponent, f"""
            Aleksander Grischuk win / loss ratio is 20.00%
            (80.00% for {FAKE_FULLNAME})""")
        self.assertEqual(result.worst_opponent,f"""
            Garry Kasparov win / loss ratio is 50.00%
            (50.00% for {FAKE_FULLNAME})""")

    def test_attachStatistics_returns_PlayerExtended_updatedWithQueriedInfo_whenHasScoredGames_andHasNoLostGames(self):
        player_short = Player(id=FAKE_PLAYER_ID, **fake_params_player())
        mock_service.has_scored_games.return_value = True

        mock_service.get_tournaments_played.return_value = 1
        mock_service.get_tournaments_won.return_value = 0
        mock_service.get_matches_played.return_value = 5
        mock_service.get_matches_won.return_value = 2
        mock_service.get_most_often_played_opponent_data.return_value = [
            "Aleksander Grischuk", 4]
        mock_service.get_most_rare_played_opponent_data.return_value = [
            "Sun Pin", 1]
        mock_service.get_best_opponent_data.return_value = []

        mock_service.get_worst_opponent_data.return_value = []

        result = statistic_service.attach_statistics(player_short)

        mock_service.has_scored_games.assert_called_once()
        mock_service.get_tournaments_played.assert_called_once()
        mock_service.get_tournaments_won.assert_called_once()
        mock_service.get_matches_played.assert_called_once()
        mock_service.get_matches_won.assert_called_once()
        mock_service.get_most_often_played_opponent_data.assert_called_once()
        mock_service.get_most_rare_played_opponent_data.assert_called_once()
        mock_service.get_best_opponent_data.assert_called_once()
        mock_service.get_worst_opponent_data.assert_called_once()

        mock_service.get_best_opponent_lost_games.assert_not_called()
        mock_service.get_worst_opponent_lost_games.assert_not_called()

        self.assertEqual(result.tournaments_won, 0)
        self.assertEqual(result.tournaments_played, 1)
        self.assertEqual(result.matches_played, 5)
        self.assertEqual(result.matches_won, 2)
        self.assertEqual(result.most_often_played_opponent,
                         "Aleksander Grischuk")
        self.assertEqual(result.total_games_with_most_often_played_opponent, 4)
        self.assertEqual(result.most_rare_played_opponent, "Sun Pin")
        self.assertEqual(result.total_games_with_most_rare_played_opponent, 1)
        self.assertEqual(result.best_opponent, "No games lost")
        self.assertEqual(result.worst_opponent, "No games lost")
        self.assertEqual(result.best_opponent_name, "Aleksander Grischuk")
        self.assertEqual(result.best_opponent_wins, None)
        self.assertEqual(result.best_opponent_losses, None)
        self.assertEqual(result.worst_opponent_name, "Sun Pin")
        self.assertEqual(result.worst_opponent_wins, None)
        self.assertEqual(result.worst_opponent_losses, None)
