# pylint: disable=missing-docstring

from fastapi import FastAPI
from routers.users import users_router
from routers.players import players_router
from routers.statistics import statistics_router
from routers.requests import requests_router
from routers.tournaments import tournaments_router
from routers.games import games_router
from routers.gameformats import gameformats_router

app = FastAPI()

app.include_router(users_router)
app.include_router(players_router)
app.include_router(statistics_router)
app.include_router(tournaments_router)
app.include_router(requests_router)
app.include_router(games_router)
app.include_router(gameformats_router)


@app.get("/", tags=["Home page"])
def root():
    return {"message": "😎 Match Score RESTful API home page 😎"}
