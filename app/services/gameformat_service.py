# pylint: disable=missing-docstring

from data import database
from data.models import GameFormat, GameFormatResponseModel
from common.responses import BadRequest


def get_all() -> list[GameFormatResponseModel] | None:
    data = database.read_query("""
        SELECT id, time_limit, score_limit, loss_points,
               draw_points, win_points 
        FROM game_formats
    """)

    if data:
        result: list[GameFormatResponseModel] = []
        for obj in data:
            result.append(GameFormatResponseModel.from_query_result(*obj))

        return result
    return None


def get_by_id(format_id: int) -> GameFormatResponseModel | None:
    data = database.read_query("""
        SELECT id, time_limit, score_limit, loss_points,
            draw_points, win_points 
        FROM game_formats WHERE id = ?
    """, (format_id,))

    if data:
        for obj in data:
            result = GameFormatResponseModel.from_query_result(*obj)

        return result
    return None


def create(game_format: GameFormat):
    if game_format.time_limit is None and game_format.score_limit is None:
        return BadRequest("Please, provide a limit")

    generated_id = database.insert_query("""
        INSERT INTO game_formats (time_limit, score_limit,
                    loss_points, draw_points, win_points) 
        VALUES (?, ?, ?, ?, ?)
    """, (
        game_format.time_limit,
        game_format.score_limit,
        game_format.loss_points,
        game_format.draw_points,
        game_format.win_points
        ))

    game_format.id = generated_id

    return game_format


def get_draw_and_win_points(format_id: int):
    data = database.read_query_one("""
        SELECT draw_points, win_points
        FROM game_formats
        WHERE id = ?
    """, (format_id,))

    return data if data else None
