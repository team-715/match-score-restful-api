# pylint: disable=missing-docstring
# pylint: disable=R0912

from data import database
from data.models import Game, GameUpdate, IndependentGame
from common.responses import BadRequest
from services import tournament_service
from services.gameformat_service import get_draw_and_win_points

UPDATE_LEAGUE_WIN = """
    UPDATE tournaments_players
    SET points = points + ?
    WHERE tournament_id = ? AND player_name = ?
"""
UPDATE_LEAGUE_DRAW = """
    UPDATE tournaments_players
    SET points = points + ?
    WHERE tournament_id = ? AND (player_name = ? OR player_name = ?)
"""
UPDATE_KNOCKOUT_WIN = """
    UPDATE games
    SET player1_name = IF(prev_game1_id = ?, ?, player1_name),
        player2_name = IF(prev_game2_id = ?, ?, player2_name)
    WHERE prev_game1_id = ? or prev_game2_id = ?
"""


def get_all_independent(limit: int, offset: int):
    data = database.read_query("""
        SELECT id, datetime, player1_name, score1, score2,
            player2_name, game_format_id, round, location
        FROM games WHERE tournament_id is null LIMIT ? OFFSET ?
    """, (limit, offset))

    if data:
        result: list[IndependentGame] = []
        for obj in data:
            result.append(IndependentGame.from_query_result(*obj))

        return result
    return None


def get_by_id(game_id: int) -> Game | None:
    data = database.read_query_one("""
        SELECT id, datetime, tournament_id, player1_name, score1, score2,
                player2_name, game_format_id, round, location
        FROM games WHERE id = ?
    """, (game_id,))

    return Game.from_query_result(*data) if data else None


def create(game: IndependentGame):
    # participants = [game.player1_name, game.player2_name]
    # create_new_players(participants)

    generated_id = database.insert_query("""
        INSERT INTO games(datetime, player1_name, score1, score2, player2_name, 
                            game_format_id, round, location) 
        VALUES (?,?,?,?,?,?,?,?)
    """, (
            game.date_time,
            game.player1_name,
            game.score1,
            game.score2,
            game.player2_name,
            game.game_format_id,
            game.round,
            game.location))

    game.id = generated_id

    return game


def update(old: Game, new: GameUpdate) -> Game:  # update score pri knockout tournament
    updated = database.read_query_one('SELECT updated from games WHERE id = ?', (old.id,))[0]

    if new.datetime is not None:
        old.datetime = new.datetime
    if new.score_player_1 is not None:
        old.score1 = new.score_player_1
    if new.score_player_2 is not None:
        old.score2 = new.score_player_2
    if new.location is not None:
        old.location = new.location

    database.update_query("""
        UPDATE games SET datetime = ?, score1 = ?, score2 = ?, location = ?
        WHERE id = ?
    """, (old.datetime, old.score1, old.score2, old.location, old.id))

    if old.tournament_id:
        tournament = tournament_service.get_by_id(old.tournament_id)

        # if tournament.format == 0:
        if tournament.format == "League" and not updated:
            draw_points, win_points = get_draw_and_win_points(tournament.game_format)
            if new.score_player_1 is not None and new.score_player_2 is not None:
                if old.score1 > old.score2:
                    database.update_query(
                        UPDATE_LEAGUE_WIN,
                        (win_points, old.tournament_id, old.player1_name),
                    )
                elif old.score2 > old.score1:
                    database.update_query(
                        UPDATE_LEAGUE_WIN,
                        (win_points, old.tournament_id, old.player2_name),
                    )
                else:
                    database.update_query(
                        UPDATE_LEAGUE_DRAW,
                        (
                            draw_points,
                            old.tournament_id,
                            old.player1_name,
                            old.player2_name,
                        ),
                    )
                database.update_query('UPDATE games SET updated = ? WHERE id = ?', (1, old.id))

        # if tournament.format == 1:
        if tournament.format == "Knockout":
            if old.score1 is not None and old.score2 is not None:
                if old.score1 > old.score2:
                    database.update_query(
                        UPDATE_KNOCKOUT_WIN,
                        (
                            old.id,
                            old.player1_name,
                            old.id,
                            old.player1_name,
                            old.id,
                            old.id,
                        ),
                    )
                elif old.score2 > old.score1:
                    database.update_query(
                        UPDATE_KNOCKOUT_WIN,
                        (
                            old.id,
                            old.player2_name,
                            old.id,
                            old.player2_name,
                            old.id,
                            old.id,
                        ),
                    )
                else:
                    return BadRequest("Knockout games cannot end in a tie")

    return old


# def create_new_players(participants: list[str]):
#     players = database.read_query('SELECT fullname FROM players')

#     players_to_create = []
#     for name in participants:
#         if (name,) not in players:
#             players_to_create.append(name)

#     if players_to_create:
#         players_values = (len(players_to_create)*'(?),')[0:-1]
#         players_sql_params = tuple(i for i in players_to_create)

#         database.insert_query(
#             f'INSERT INTO players(fullname) VALUES {players_values}', players_sql_params)
