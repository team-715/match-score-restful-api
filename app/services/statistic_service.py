# pylint: disable=missing-docstring
from data.models import Player, PlayerExtended
from services import player_service


def attach_statistics(player_short: Player):
    player = PlayerExtended(**(player_short.dict()))

    # does not search for stats info if player has no scored games
    if not player_service.has_scored_games(player.fullname):
        return player

    player.tournaments_played = player_service.get_tournaments_played(player.fullname)
    player.tournaments_won = player_service.get_tournaments_won(player.fullname)
    player.matches_played = player_service.get_matches_played(player.fullname)
    player.matches_won = player_service.get_matches_won(player.fullname)

    most_often_opponent_data = player_service.get_most_often_played_opponent_data(
        player.fullname
    )
    (
        player.most_often_played_opponent,
        player.total_games_with_most_often_played_opponent,
    ) = most_often_opponent_data

    most_rare_opponent_data = player_service.get_most_rare_played_opponent_data(
        player.fullname
    )
    (
        player.most_rare_played_opponent,
        player.total_games_with_most_rare_played_opponent,
    ) = most_rare_opponent_data

    # that player with whom the player has the least losses
    best_opponent_data = player_service.get_best_opponent_data(player.fullname)

    if best_opponent_data:
        player.best_opponent_name, player.best_opponent_wins = best_opponent_data
        player.best_opponent_losses = player_service.get_best_opponent_lost_games(
            player.fullname, player.best_opponent_name
        )

        player.best_opponent = f"""
            {player.best_opponent_name} win / loss ratio is {player.best_opponent_ratio():.2f}%
            ({(100-player.best_opponent_ratio()):.2f}% for {player.fullname})"""
    else:
        # if player has no losses
        player.best_opponent_name = player.most_often_played_opponent

    # that player with whom the player has the most losses
    worst_opponent_data = player_service.get_worst_opponent_data(player.fullname)

    if worst_opponent_data:
        player.worst_opponent_name, player.worst_opponent_wins = worst_opponent_data
        player.worst_opponent_losses = player_service.get_worst_opponent_lost_games(
            player.fullname, player.worst_opponent_name
        )

        player.worst_opponent = f"""
            {player.worst_opponent_name} win / loss ratio is {player.worst_opponent_ratio():.2f}%
            ({(100-player.worst_opponent_ratio()):.2f}% for {player.fullname})"""
    else:
        # if player has no losses
        player.worst_opponent_name = player.most_rare_played_opponent

    return player
