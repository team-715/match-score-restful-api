# pylint: disable=missing-docstring
from mariadb import IntegrityError

from data import database
from data.models import Player, PlayerShort


def exist_by_id(player_id: int) -> bool:
    data = database.read_query_one("""
        SELECT id
        FROM players
        WHERE id = ? and flag is null
    """, (player_id,))

    return bool(data)


def get_all(country: str, club: str, limit: int, offset: int) -> list[Player]:

    if country and club:
        data = database.read_query("""
            SELECT id, fullname, country, club
            FROM players
            WHERE flag is null and (country LIKE ? and club LIKE ?)
            LIMIT ? OFFSET ?
        """, (f"%{country}%", f"%{club}%", limit, offset))

    elif country:
        data = database.read_query("""
            SELECT id, fullname, country, club
            FROM players
            WHERE flag is null and country LIKE ?
            LIMIT ? OFFSET ?
        """, (f"%{country}%", limit, offset))

    elif club:
        data = database.read_query("""
            SELECT id, fullname, country, club
            FROM players
            WHERE flag is null and club LIKE ?
            LIMIT ? OFFSET ?
        """, (f"%{club}%", limit, offset))
    else:
        data = database.read_query("""
            SELECT id, fullname, country, club
            FROM players
            WHERE flag is null
            LIMIT ? OFFSET ?
        """, (limit, offset))

    return (Player.from_query_result(*row) for row in data) if data else None


def get_by_id(player_id: int) -> Player:
    data = database.read_query("""
        SELECT id, fullname, country, club
        FROM players 
        WHERE id = ? and flag is null
    """, (player_id,))

    return next((Player.from_query_result(*row) for row in data), None)


def get_by_fullname(name: str) -> Player:
    data = database.read_query("""
        SELECT id, fullname, country, club
        FROM players
        WHERE fullname = ? and flag is null
    """, (name,))

    return next((Player.from_query_result(*row) for row in data), None)


def add(payload: Player) -> Player:
    try:
        payload.id = database.insert_query("""
            INSERT INTO players (fullname, country, club) 
            VALUES (?, ?, ?)
        """, (payload.fullname, payload.country, payload.club))
        return payload
    except IntegrityError:
        return None


def update(new: PlayerShort, old: Player) -> Player:
    if new.club:
        old.club = new.club
    if new.country:
        old.country = new.country

    database.update_query("""
        UPDATE players
        SET country = ?, club = ?
        WHERE id = ?
    """, (old.country, old.club, old.id))

    return old


def find_existing_players(participants: list[str]) -> list[str]:
    players = database.read_query("SELECT fullname FROM players")

    existing_players = [name for name in participants if (name,) in players]

    if len(existing_players) != 0:
        return existing_players
    return []


def find_non_existing_players(participants: list[str]) -> list[str]:
    players = database.read_query("SELECT fullname FROM players")

    non_existing_players = [name for name in participants if (name,) not in players]

    if len(non_existing_players) != 0:
        return non_existing_players
    return []


def create_new_players(players_to_create: list[str]):
    # constructing a SQL string
    players_values = (len(players_to_create) * "(?),")[0:-1]

    database.insert_query(
        f"INSERT INTO players(fullname) VALUES {players_values}",
        tuple(players_to_create),
    )


def delete(player_id: int) -> None:
    database.insert_query("""
        UPDATE players
        SET flag = ?
        WHERE id = ?
    """, (1, player_id))


def get_tournaments_played(player_fullname: str) -> int:
    data = list(database.read_query_one("""
        SELECT count(distinct(tournament_id))
        FROM tournaments_players
        WHERE player_name = ?
    """, (player_fullname,)))

    return data.pop() if data else 0


def get_tournaments_won(player_fullname: str) -> int:
    data = list(database.read_query_one("""
        SELECT count(winner)
        FROM tournaments
        WHERE winner = ?
    """, (player_fullname,)))

    return data.pop() if data else 0


def get_matches_played(player_fullname: str) -> int:
    sql_params = (player_fullname, player_fullname)
    data = list(
        database.read_query_one("""
        SELECT count(id)
        FROM games
        WHERE player1_name = ? or player2_name = ?
    """, sql_params))

    return data.pop() if data else 0


def get_matches_won(player_fullname: str) -> int:
    sql_params = (player_fullname, player_fullname)
    data = list(database.read_query_one("""
        SELECT count(id)
        FROM games
        WHERE player1_name = ? and score1 > score2 or player2_name = ? and score2 > score1
    """, sql_params))

    return data.pop() if data else 0


def get_most_often_played_opponent_data(player_fullname):
    sql_params = (player_fullname, player_fullname)
    data = list(database.read_query_one("""
        SELECT most_often.opponent, most_often.total_games
        FROM (SELECT Count(u.player1_name) as 'total_games', u.player1_name as opponent
            FROM (SELECT player1_name FROM games WHERE player2_name = ?
                    UNION ALL
                    SELECT player2_name FROM games WHERE player1_name = ?) as u
                    GROUP BY u.player1_name) as most_often
        ORDER BY most_often.total_games DESC, most_often.opponent
        LIMIT 1
    """, sql_params))
    return data


def get_most_rare_played_opponent_data(player_fullname):
    sql_params = (player_fullname, player_fullname)
    data = list(database.read_query_one("""
        SELECT most_often.opponent, most_often.total_games
        FROM (SELECT Count(u.player1_name) as 'total_games', u.player1_name as opponent
            FROM (SELECT player1_name FROM games WHERE player2_name = ?
                    UNION ALL 
                    SELECT player2_name FROM games WHERE player1_name = ?) as u
                    GROUP BY u.player1_name) as most_often
        ORDER BY most_often.total_games ASC, most_often.opponent
        LIMIT 1
    """, sql_params))

    return data


def get_best_opponent_data(player_fullname: str):
    sql_params = (player_fullname, player_fullname)
    data = database.read_query_one("""
        SELECT op.player1_name as opponent, lost_games.lost as losses
        FROM (SELECT player1_name FROM games WHERE player2_name = ?
                UNION
                SELECT player2_name FROM games WHERE player1_name = ?) as op
                JOIN (SELECT Count(lost.player1_name) as lost, lost.player1_name 
                FROM (SELECT u.player1_name, u.score1, u.score2
                FROM (SELECT player1_name, score1, score2 FROM games WHERE player2_name = ?
                        UNION ALL 
                        SELECT player2_name, score2, score1 FROM games WHERE player1_name = ?) as u 
                WHERE u.score1 > u.score2) as lost
                GROUP BY lost.player1_name
            ORDER BY Count(lost.player1_name)) as lost_games on lost_games.player1_name = op.player1_name
        ORDER BY losses, opponent
        LIMIT 1
    """, sql_params * 2)
    return data


def get_best_opponent_lost_games(player_fullname: str, best_opponent_name: str) -> int:
    sql_params = (player_fullname, player_fullname)
    data = list(database.read_query_one("""
        SELECT COUNT(games_with_opponents.id)
        FROM (SELECT id, player1_name as opponent, score1, score2
              FROM games
              WHERE player2_name = ?
              UNION ALL
              SELECT id, player2_name, score2, score1
              FROM games
              WHERE player1_name = ?) as games_with_opponents
        WHERE games_with_opponents.opponent = ? and games_with_opponents.score1 < games_with_opponents.score2
    """, sql_params + (best_opponent_name,))).pop()

    return data


def get_worst_opponent_data(player_fullname: str):
    sql_params = (player_fullname, player_fullname)
    data = database.read_query_one("""
        SELECT op.player1_name as opponent, lost_games.lost as losses FROM
            (SELECT player1_name FROM games WHERE player2_name = ?
            UNION SELECT player2_name FROM games WHERE player1_name = ?) as op
            JOIN (SELECT Count(lost.player1_name) as lost, lost.player1_name FROM
                            (SELECT u.player1_name, u.score1, u.score2 FROM
                                (SELECT player1_name, score1, score2 FROM games WHERE player2_name = ?
                                UNION ALL
                                SELECT player2_name, score2, score1 FROM games WHERE player1_name = ?) as u
                            WHERE u.score1>u.score2) as lost
                        GROUP BY lost.player1_name
                        ORDER BY Count(lost.player1_name)) as lost_games on lost_games.player1_name = op.player1_name
        ORDER BY losses DESC, opponent
        LIMIT 1
    """, sql_params * 2)
    return data


def get_worst_opponent_lost_games(player_fullname: str, worst_opponent_name: str) -> int:
    sql_params = (player_fullname, player_fullname)
    data = list(database.read_query_one("""
        SELECT COUNT(games_with_opponents.id) FROM
            (SELECT id, player1_name as opponent, score1, score2 FROM games WHERE player2_name = ?
            UNION ALL
            SELECT id, player2_name, score2, score1 FROM games WHERE player1_name = ?) as games_with_opponents
        WHERE games_with_opponents.opponent = ? and games_with_opponents.score1 < games_with_opponents.score2
    """, sql_params + (worst_opponent_name,))).pop()

    return data


def has_scored_games(player_name: str) -> bool:
    data = database.read_query("""
        SELECT id, player1_name, player2_name, score1, score2
        FROM games
        WHERE (player1_name = ? or player2_name = ?) and (score1 is not null and score2 is not null)
    """, (player_name, player_name))

    return bool(data)


def get_existing_player_ids_from_fullname(participants: list[str]) -> list[int]:
    existing_players = find_existing_players(participants)
    if len(existing_players) == 0:
        return []

    if len(existing_players) == 1:
        data = database.read_query_one(
            """
            SELECT id FROM players
            WHERE fullname = ?
        """, (existing_players[0],))
        player_id = [data[0]]

        return player_id

    data = database.read_query(f"""
        SELECT id FROM players 
        WHERE fullname IN {tuple(existing_players)}
    """)
    player_ids = [y for (y,) in data]

    return player_ids


# used in line 82 in tournaments.py
def get_player_ids_from_fullname(participants: list[str]) -> list[int]:
    data = database.read_query(f"""
        SELECT id FROM players
        WHERE fullname IN {tuple(participants)}
    """)

    return [y for (y,) in data] if data else None
