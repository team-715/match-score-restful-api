# pylint: disable=missing-docstring
from hashlib import sha256
from mariadb import IntegrityError

from data import database
from data.models import Role, RoleInput, User, UserBody, UserResponse


def get_all(limit: int, offset: int) -> list[UserResponse]:
    data = database.read_query(
        """
        SELECT id, email, role, player_id
        FROM users
        LIMIT ? OFFSET ?""",
        (limit, offset),
    )

    return (UserResponse.from_query_result(*row) for row in data) if data else None


# without password
def get_by_id(user_id: int) -> UserResponse | None:
    data = database.read_query(
        "SELECT id, email, role, player_id FROM users WHERE id = ?", (user_id,)
    )

    return next((UserResponse.from_query_result(*row) for row in data), None)


def find_by_email(email: str) -> User | None:
    data = database.read_query(
        "SELECT id, email, password, role FROM users WHERE email = ?", (email,)
    )

    return next((User.from_query_result(*row) for row in data), None)


def find_by_id(user_id: int) -> User | None:
    data = database.read_query(
        "SELECT id, email, password, role FROM users WHERE id = ?", (user_id,)
    )

    return next((User.from_query_result(*row) for row in data), None)


def try_login(email: str, password: str) -> User | None:
    user = find_by_email(email)

    return user if (user and user.password == _hash_password(password)) else None


def create(email: str, password: str) -> UserResponse | None:
    try:
        generated_id = database.insert_query(
            "INSERT INTO users (email, password, role) VALUES (?,?,?)",
            (email, _hash_password(password), Role.USER),
        )

        return UserResponse(id=generated_id, email=email, role=Role.USER)

    except IntegrityError:
        # mariadb raises this error when a constraint is violated
        # in that case we have duplicate emails
        return None


# passwords should be secured as hash strings in DB
def _hash_password(password: str):
    return sha256(password.encode("utf-8")).hexdigest()


# def get_user_emails_associated_with_player_ids(player_ids: list[int]) -> list[str] | None:
#     if len(player_ids) == 0:
#         return None

#     elif len(player_ids) == 1:
#         data = database.read_query_one(
#             """
#             SELECT email
#             FROM users
#             WHERE player_id = ?""",
#             (player_ids[0],))

#         # email = [data[0]]

#         return [data[0]] if data else None
#     else:
#         data = database.read_query(
#             f"""
#             SELECT email
#             FROM users
#             WHERE player_id IN {tuple(player_ids)}""")

#         emails = [y for (y,) in data]

#         return emails


def get_user_email_associated_with_player_id(player_id: int) -> str | None:
    data = database.read_query_one(
        """SELECT email FROM users WHERE player_id = ?""", (player_id,)
    )

    return data[0] if data else None


def get_user_associated_with_player_id(player_id: int) -> UserResponse:
    data = database.read_query(
        """
            SELECT id, email, role, player_id
            FROM users
            WHERE player_id = ?""",
        (player_id,),
    )

    return next((UserResponse.from_query_result(*row) for row in data), None)


def update(new: UserBody, old: UserResponse) -> UserResponse:
    old.role = new.role or old.role
    old.player_id = new.player_id or old.player_id

    if old.role == RoleInput.ADMIN:
        database.update_query(
            """
            UPDATE users SET role = ?, player_id = ? WHERE id = ?""",
            (Role.ADMIN, old.player_id, old.id),
        )
    elif old.role == RoleInput.DIRECTOR:
        database.update_query(
            """
            UPDATE users SET role = ?, player_id = ? WHERE id = ?""",
            (Role.DIRECTOR, old.player_id, old.id),
        )
    # elif old.role == RoleInput.user:
    else:
        database.update_query(
            """
            UPDATE users SET role = ?, player_id = ? WHERE id = ?""",
            (Role.USER, old.player_id, old.id),
        )

    return old
