# pylint: disable=missing-docstring

from data import database
from data.models import (
    RequestLink,
    PromoteRequest,
    Request,
    RequestSingle,
    RequestStatus,
)


def get_all(limit: int, offset: int) -> list[Request]:
    data = database.read_query("""
        SELECT id, type, status FROM requests LIMIT ? OFFSET ?""",
        (limit, offset))

    return ([Request.from_query_result(*row) for row in data]) if data else None


def get_by_id(request_id: int) -> RequestSingle:
    data = database.read_query_one("""
        SELECT id, user_id, type, player_id, status FROM requests WHERE id = ?""",
        (request_id,))

    return RequestSingle.from_query_result(*data) if data else None


def add_promote_request(user_id: int, request_type: str = "promote") -> PromoteRequest:
    request_id = database.insert_query("""
        INSERT INTO requests (user_id, type) VALUES (?, ?)""",
        (user_id, request_type))

    return PromoteRequest(id=request_id, from_user=user_id)


def add_link_request(user_id: int, player_id: int | None, request_type: str = "link") -> int:
    request_id = database.insert_query("""
        INSERT INTO requests (user_id, type, player_id) VALUES (?, ?, ?)""",
        (user_id, request_type, player_id))

    return RequestLink(
        id=request_id, from_user=user_id, associate_with_player_id=player_id)


def update(new_status: str, old: RequestSingle) -> RequestSingle:
    old.status = new_status
    if new_status == RequestStatus.APPROVED:
        new_status = "approved"
        if old.type == "link":
            database.update_query("""
                UPDATE users SET player_id = ? WHERE id = ?""",
                (old.player_id, old.user_id))

        elif old.type == "promote":
            database.update_query("""
                UPDATE users SET role = ? WHERE id = ?""",
                ("director", old.user_id))

    else:
        new_status = "declined"

    database.update_query("""
        UPDATE requests SET status = ? WHERE id = ?""",
        (new_status, old.id))

    return old


def delete(request_id: int) -> None:
    database.update_query("DELETE FROM requests WHERE id = ?", (request_id,))
