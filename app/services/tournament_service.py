# pylint: disable=missing-docstring, R0914

from math import log2

from data import database
from data.models import (
    Tournament,
    TournamentStatus,
    TournamentResponseModel,
    AllTournamentResponseModel,
)
from data.models import (
    SingleTournamentResponseModel,
    GameResponseModel,
    TournamentUpdate,
    TournamentStandingsResponse,
)
from common.responses import BadRequest
from common.utils import pop_random

SELECT_ALL_FROM_T = """
    SELECT id, title, format, game_format_id, start_date, end_date, location, prizes, winner
    FROM tournaments"""


def get_all(status: str, offset: int, limit: int) -> list[AllTournamentResponseModel]:
    if not status:
        data = database.read_query(
            f"""{SELECT_ALL_FROM_T}
            LIMIT ? OFFSET ?""",
            (limit, offset),
        )

    elif status == TournamentStatus.PAST:
        data = database.read_query(
            f"""{SELECT_ALL_FROM_T}
            WHERE end_date <= curDATE()
            LIMIT ? OFFSET ?""",
            (limit, offset),
        )

    elif status == TournamentStatus.FUTURE:
        data = database.read_query(
            f"""{SELECT_ALL_FROM_T}
            WHERE start_date > curDATE()
            LIMIT ? OFFSET ?""",
            (limit, offset),
        )

    else:  # present
        data = database.read_query(
            f"""{SELECT_ALL_FROM_T}
            WHERE (start_date <= curDATE() and end_date > curDATE())
               or (start_date <= curDATE() and end_date is null)
               or (start_date is null and end_date > curDATE())
               or (start_date is null and end_date is null)
            LIMIT ? OFFSET ?""",
            (limit, offset),
        )

    return (
        ([AllTournamentResponseModel.from_query_result(*row) for row in data])
        if data
        else None
    )


def get_by_id(t_id: int):
    data = database.read_query("""
        SELECT t.id, t.title, t.format, t.game_format_id,
            t.number_of_rounds, t.start_date, t.end_date, 
            t.location, t.prizes, t.winner, tp.player_name
        FROM tournaments as t
        LEFT JOIN tournaments_players as tp on tp.tournament_id = t.id
        WHERE t.id = ?""",
        (t_id,),
    )

    if data:
        flattened = {}
        for (
            tournament_id,
            title,
            format_,
            game_format_id,
            number_of_rounds,
            start_date,
            end_date,
            location,
            prizes,
            winner,
            player_name,
        ) in data:
            if format_ == 0:
                format_ = "League"
            if format_ == 1:
                format_ = "Knockout"

            if tournament_id not in flattened:
                flattened[tournament_id] = (
                    tournament_id,
                    title,
                    format_,
                    game_format_id,
                    number_of_rounds,
                    start_date,
                    end_date,
                    location,
                    prizes,
                    winner,
                    [],
                )

            if player_name is not None:
                flattened[tournament_id][-1].append(player_name)

        for obj in flattened.values():
            result = SingleTournamentResponseModel.from_query_result(*obj)

        if result.participants:
            if result.format == "League":
                result.number_of_games = len(result.participants) * (
                    len(result.participants) - 1
                )
            else:
                result.number_of_games = len(result.participants) - 1

            return result

        return result

    return None


def create_league(tournament: Tournament):
    # number_of_rounds = create_new_players_and_return_number_of_rounds(
    #     tournament.participants)

    number_of_rounds = len(tournament.participants) * 2 - 2

    tournament.id = insert_tournament(tournament, number_of_rounds)

    insert_league_games(tournament.participants, tournament.id, tournament.game_format)

    insert_tournament_players(tournament.participants, tournament.id)

    return TournamentResponseModel(
        id=tournament.id,
        participants=tournament.participants,
        title=tournament.title,
        game_format=tournament.game_format,
        rounds=number_of_rounds,
        start_date=tournament.start_date,
        end_date=tournament.end_date,
        location=tournament.location,
        prizes=tournament.prizes)


def get_games_from_round(t_id: int, round_: int):
    data = database.read_query("""
        SELECT id, datetime, tournament_id, player1_name, score1, score2,
            player2_name, game_format_id, round, location
        FROM games WHERE tournament_id = ? AND round = ?
    """,(t_id, round_))

    if data:
        result: list[GameResponseModel] = []
        for obj in data:
            result.append(GameResponseModel.from_query_result(*obj))

        tournament = get_by_id(t_id)
        if tournament.format == "Knockout":
            phase = find_phase_from_round(round_)
            for i in result:
                i.round = phase

        return result
    return None


def get_all_games_in_tournament(t_id: int):
    data = database.read_query("""
        SELECT id, datetime, tournament_id, player1_name, score1, score2,
            player2_name, game_format_id, round, location
        FROM games WHERE tournament_id = ?
    """, (t_id,))

    if data:
        result: list[GameResponseModel] = []
        for obj in data:
            result.append(GameResponseModel.from_query_result(*obj))

        tournament = get_by_id(t_id)
        if tournament.format == "Knockout":
            for i in result:
                phase = find_phase_from_round(i.round)
                i.round = phase

        return result
    return None


def get_current_standings(t_id: int):
    tournament = get_by_id(t_id)
    if tournament:
        if tournament.format == "League":
            data = database.read_query(
                """SELECT player_name, points
                    FROM tournaments_players
                    WHERE tournament_id = ? ORDER BY points DESC, player_name""",
                (t_id,),
            )

            if data:
                result: list[TournamentStandingsResponse] = []
                place = 1
                for name, points in data:
                    result.append(
                        TournamentStandingsResponse(
                            place=place, player_name=name, points=points
                        )
                    )
                    place += 1
                return result
            return None
        return None
    return None


def insert_tournament_players(participants: list[str], tournament_id: int):
    params = []
    for i in participants:
        params.append(tournament_id)
        params.append(i)

    values = (len(participants) * "(?, ?),")[0:-1]
    sql_params = tuple(params)

    database.insert_query(
        f"""INSERT INTO tournaments_players(tournament_id, player_name)
                 VALUES {values}""",
        sql_params,
    )


def insert_league_games(participants: list[str], tournament_id: int, game_format: int):
    shuffled_participants = list(set(participants))
    all_games = list(
        generate_league_rounds_2leg(
            shuffled_participants, sets=len(participants) * 2 - 2
        )
    )

    round_count = 1
    unpacked = []
    # for i in range(len(all_games)):
    #     for j in all_games[i]:
    #         unpacked.append((j + (round_count,) + (tournament_id,) + (game_format,)))
    #     round_count += 1

    for enum in enumerate(all_games):
        for game in enum[1]:
            unpacked.append((game + (round_count,) + (tournament_id,) + (game_format,)))
        round_count += 1

    params = []
    for name1, name2, round_, t_id, g_format in unpacked:
        params.extend([name1, name2, round_, t_id, g_format])

    number_of_games = len(participants) * (len(participants) - 1)
    values = (number_of_games * "(?, ?, ?, ?, ?),")[0:-1]
    sql_params = tuple(params)

    database.insert_query(
        f"""INSERT INTO games(player1_name, player2_name, round, tournament_id, game_format_id)
                 VALUES {values}""",
        sql_params,
    )


def generate_league_rounds_2leg(players, sets=None):
    """Generates a schedule of "fair" pairings from a list of units"""
    count = len(players)
    sets = sets or (count - 1)
    half = count // 2
    for turn in range(sets):
        left = players[:half]
        right = players[count - half - 1 + 1 :][::-1]
        pairings = list(zip(left, right))
        if turn % 2 == 1:
            pairings = [(y, x) for (x, y) in pairings]
        players.insert(1, players.pop())
        yield pairings


def create_knockout(tournament: Tournament):
    participants_copy = tournament.participants.copy()

    # find number of rounds/phases
    number_of_rounds = int(log2(len(tournament.participants)))

    # create knockout tournament
    tournament.id = insert_tournament(tournament, number_of_rounds)

    insert_tournament_players(tournament.participants, tournament.id)
    insert_knockout_games(
        tournament.participants,
        tournament.id,
        tournament.game_format,
        tournament.location,
    )

    return TournamentResponseModel(
        id=tournament.id,
        participants=participants_copy,
        title=tournament.title,
        format="Knockout",
        game_format=tournament.game_format,
        rounds=number_of_rounds,
        start_date=tournament.start_date,
        end_date=tournament.end_date,
        location=tournament.location,
        prizes=tournament.prizes,
    )


def insert_tournament(tournament: Tournament, number_of_rounds: int) -> int:
    generated_id = database.insert_query("""
        INSERT INTO tournaments(title, format, game_format_id, number_of_rounds,
            start_date, end_date, location, prizes)
        VALUES (?,?,?,?,?,?,?,?)""",
        (
            tournament.title,
            tournament.format,
            tournament.game_format,
            number_of_rounds,
            tournament.start_date,
            tournament.end_date,
            tournament.location,
            tournament.prizes,
        ),
    )

    return generated_id


def find_round(length: int) -> int:
    if length < 2:
        raise ValueError("length must be at least 2")
    if length == 2:
        return 1  # Final
    if length <= 4:
        return 2  # f'Semi Final'
    if length <= 8:
        return 3  # f'Quarter Final'
    return int(log2(length))  # f'1/{length//2} Final'


def find_phase_from_round(round_: int) -> str:
    if round_ == 1:
        return "Final"
    if round_ == 2:
        return "Semi Final"
    if round_ == 3:
        return "Quarter Final"
    return f"1/{2**(round_-1)} Final"


def insert_knockout_games(
    participants: list[str],
    t_id: int,
    t_game_format: int,
    t_location: str | None = None,
):
    length_participants = len(participants)
    starting_round = find_round(length_participants)

    games = []
    for _ in range(len(participants) // 2):
        player1 = pop_random(participants)
        player2 = pop_random(participants)
        games.extend([player1, player2, starting_round, t_id, t_game_format, t_location])

    # number_of_games = (len(participants)-1) # ne se igrae za 3 i 4, 5 i 6 i t.n.
    values = ((length_participants // 2) * "(?, ?, ?, ?, ?, ?),")[0:-1]
    sql_params = games

    database.insert_query(f"""
        INSERT INTO games (player1_name, player2_name,
            round, tournament_id, game_format_id, location)
        VALUES {values}
    """, sql_params)

    data = database.read_query("SELECT id FROM games WHERE tournament_id = ?", (t_id,))
    generated_ids = []
    # for i in range(len(data)):
    #     for j in data[i]:
    #         generated_ids.append(j)

    for enum in enumerate(data):
        for j in enum[1]:
            generated_ids.append(j)

    while len(generated_ids) > 1:
        round_ = find_round(len(generated_ids))

        game1_id = generated_ids.pop(0)
        game2_id = generated_ids.pop(0)

        generated_id = database.insert_query("""
            INSERT INTO games(tournament_id, game_format_id, round, location,
                prev_game1_id, prev_game2_id)
            VALUES (?,?,?,?,?,?)
        """, (t_id, t_game_format, round_, t_location, game1_id, game2_id))

        generated_ids.append(generated_id)


def update(
    old: SingleTournamentResponseModel, new: TournamentUpdate
) -> SingleTournamentResponseModel:
    old.title = new.title if new.title is not None else old.title
    old.start_date = new.start_date if new.start_date is not None else old.start_date
    old.end_date = new.end_date if new.end_date is not None else old.end_date
    old.location = new.location if new.location is not None else old.location
    old.prizes = new.prizes if new.prizes is not None else old.prizes
    old.winner = new.winner  # if new.winner is not None else old.winner

    if new.winner:
        data = database.read_query(
            """SELECT tournament_id FROM tournaments_players
                WHERE tournament_id = ? AND player_name = ?""",
            (old.id, new.winner),
        )
        if not data:
            return BadRequest(
                "Player with this name does not exist or is not part of this tournament"
            )

    database.update_query("""
        UPDATE tournaments 
        SET title = ?, start_date = ?, end_date = ?, location = ?, prizes = ?, winner = ?
        WHERE id = ?
    """,(old.title,
        old.start_date,
        old.end_date,
        old.location,
        old.prizes,
        old.winner,
        old.id))

    return old

# def find_number_of_phases(participants: list[str]):
#     # phase n = log2(len(participants)//2) is float
#     # round/phase 3 - quarter-final
#     # round/phase 2 - semi-final
#     # round/phase 1 - final
#     number_of_phases = int(log2(len(participants)//2))

#     return number_of_phases


# def find_non_existing_players(participants: list[str]) -> list[str] | None:
#     players = database.read_query(
#         'SELECT fullname FROM players')

#     non_existing_players = [
#         name for name in participants if (name,) not in players]

#     if len(non_existing_players) != 0:
#         return non_existing_players
#     return None


# def create_non_existing_players(players_to_create: list[str]) -> None:
#     # constructing a SQL string
#     players_values = (len(players_to_create)*'(?),')[0:-1]

#     database.insert_query(
#         f'INSERT INTO players(fullname) VALUES {players_values}', tuple(players_to_create))


# def is_a_power_of_two(n:int) -> bool:
#     if log2(n) % 1 == 0:
#         return True

# def pop_random(lst: list):
#     """Select and delete a random element from a list using random.randrange"""
#     idx = randrange(0, len(lst))

#     return lst.pop(idx)

# def create_new_players_and_return_number_of_rounds(participants: list[str]):
#     # players = database.read_query('SELECT fullname FROM players')

#     # players_to_create = []
#     # for name in participants:
#     #     if (name,) not in players:
#     #         players_to_create.append(name)

#     # if players_to_create:
#     #     players_values = (len(players_to_create)*'(?),')[0:-1]
#     #     players_sql_params = tuple(i for i in players_to_create)

#     #     database.insert_query(
#     #         f'INSERT INTO players(fullname) VALUES {players_values}', players_sql_params)

#     number_of_rounds = (len(participants) * 2 - 2)

#     return number_of_rounds
