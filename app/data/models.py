# pylint: disable=missing-docstring
# pylint: disable=W0102, W0621, W0622, W1401, E0611, R0902, R0903, R0913, C0103


from __future__ import annotations

from datetime import date, datetime
from enum import Enum
from pydantic import BaseModel, constr


class Tags(str, Enum):
    HOME = "Home Page"
    USER = "Users"
    PLAYER = "Players"
    TOURNAMENT = "Tournaments"
    REQUEST = "Requests"
    GAME = "Games"
    FORMAT = "Game Formats"
    STATISTIC = "Statistics"


class QueryRequest(str, Enum):
    PROMOTE = "promote"
    LINK = "link"


class RequestStatus(str, Enum):
    PENDING = "pending"
    APPROVED = "approved"
    DECLINED = "declined"


class TournamentStatus(str, Enum):
    PAST = "past"
    PRESENT = "present"
    FUTURE = "future"


class RoleInput(str, Enum):
    USER = "user"
    ADMIN = "admin"
    DIRECTOR = "director"


class Token(BaseModel):
    token: str


class Role:
    USER = "user"
    ADMIN = "admin"
    DIRECTOR = "director"


TEmail = constr(
    regex=r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b")
# Minimum eight characters, at least one letter and one number
TPassword = constr(
    regex="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$")
TFullname = constr(
    regex="([A-Z]{1}[a-z]+\-?)([A-Z]{1}[a-z]+)?( )(([A-Z]{1}[a-z]+\-?)([A-Z]{1}[a-z]+)?)")


class User(BaseModel):
    id: int | None
    email: TEmail
    password: str
    role: str

    def is_admin(self):
        return self.role == Role.ADMIN

    def is_director(self):
        return self.role == Role.DIRECTOR

    @classmethod
    def from_query_result(cls, id, email, password, role):
        return cls(id=id, email=email, password=password, role=role)


class UserResponse(BaseModel):
    id: int | None
    email: TEmail
    role: str
    player_id: int | None = None

    @classmethod
    def from_query_result(cls, id, email, role, player_id=None):
        return cls(id=id, email=email, role=role, player_id=player_id)


class LoginData(BaseModel):
    email: TEmail
    password: TPassword


class PasswordChange(BaseModel):
    email: TEmail
    old_password: TPassword
    new_password: TPassword


class UserBody(BaseModel):
    role: RoleInput | None = None
    player_id: int | None = None


class Tournament(BaseModel):
    id: int | None = None
    participants: list[TFullname]
    title: str
    format: int  # str: 'league' | 'cup'/'knockout'; int: 0 | 1
    game_format: int  # str - 'classical' | 'rapid' | 'blitz'
    start_date: date | None = None
    end_date: date | None = None
    location: str | None = None
    prizes: str | None = None


class AllTournamentResponseModel(BaseModel):
    id: int
    title: str
    format: int
    game_format: int | None = None
    start_date: date | None = None
    end_date: date | None = None
    location: str | None = None
    prizes: str | None = None
    winner: str | None = None

    @classmethod
    def from_query_result(
        cls,
        id,
        title,
        format,
        game_format,
        start_date,
        end_date,
        location,
        prizes,
        winner,
    ):
        return cls(
            id=id,
            title=title,
            format=format,
            game_format=game_format,
            start_date=start_date,
            end_date=end_date,
            location=location,
            prizes=prizes,
            winner=winner,
        )


class TournamentResponseModel(BaseModel):
    id: int
    participants: list[str]
    title: str
    format: str = "League"
    game_format: int | None = None
    rounds: int | None = None
    start_date: date | None = None
    end_date: date | None = None
    location: str | None = None
    prizes: str | None = None

    # @classmethod
    # def from_query_result(
    #           cls, id, participants, title, format,
    #           game_format, rounds, start_date, end_date, location, prizes):
    #     return cls(id=id, participants=participants,
    #         title=title,
    #         format=format,
    #         game_format=game_format,
    #         rounds=rounds,
    #         start_date=start_date,
    #         end_date=end_date,
    #         location=location,
    #         prizes=prizes
    #     )


class SingleTournamentResponseModel(BaseModel):
    id: int | None = None
    title: str
    format: str
    game_format: int
    number_of_rounds: int | None = None
    start_date: date | None = None
    end_date: date | None = None
    location: str | None = None
    prizes: str | None = None
    number_of_games: int | None
    winner: str | None
    participants: list[str] = []

    @classmethod
    def from_query_result(
        cls,
        id,
        title,
        format,
        game_format,
        number_of_rounds,
        start_date,
        end_date,
        location,
        prizes,
        winner,
        participants=[],
    ):
        return cls(
            id=id,
            title=title,
            format=format,
            game_format=game_format,
            number_of_rounds=number_of_rounds,
            start_date=start_date,
            end_date=end_date,
            location=location,
            prizes=prizes,
            number_of_games=None,
            winner=winner,
            participants=participants,
        )


class TournamentUpdate(BaseModel):
    title: str | None = None
    start_date: date | None = None
    end_date: date | None = None
    location: str | None = None
    prizes: str | None = None
    winner: str | None = None


class PlayerShort(BaseModel):
    country: str | None = None
    club: str | None = None


class Player(BaseModel):
    id: int | None = None
    fullname: TFullname
    country: str | None = None
    club: str | None = None

    @classmethod
    def from_query_result(cls, id, fullname, country, club):
        return cls(id=id, fullname=fullname, country=country, club=club)


class PlayerExtended(Player):
    tournaments_won: int = 0
    tournaments_played: int = 0
    matches_won: int = 0
    matches_played: int = 0
    most_often_played_opponent: str | None = None
    total_games_with_most_often_played_opponent: int = 0
    most_rare_played_opponent: str | None = None
    total_games_with_most_rare_played_opponent: int = 0

    best_opponent_name: str | None = None
    best_opponent_wins: int | None = None
    best_opponent_losses: int | None = None
    best_opponent: str | None = "No games lost"

    worst_opponent_name: str | None = None
    worst_opponent_wins: int | None = None
    worst_opponent_losses: int | None = None
    worst_opponent: str | None = "No games lost"

    def best_opponent_ratio(self) -> str:
        total = self.best_opponent_wins + self.best_opponent_losses
        ratio = (self.best_opponent_wins / total) * 100
        return ratio

    def worst_opponent_ratio(self) -> str:
        total = self.worst_opponent_wins + self.worst_opponent_losses
        ratio = round(((self.worst_opponent_wins / total) * 100), 2)
        return ratio


class PromoteRequest(BaseModel):
    id: int | None = None
    from_user: int | None = None
    type_of_request: QueryRequest = QueryRequest.PROMOTE
    status: str | None = "pending"


class RequestLink(BaseModel):
    id: int | None = None
    from_user: int | None = None
    type_of_request: QueryRequest = QueryRequest.LINK
    associate_with_player_id: int
    status: str | None = "pending"


class Request(BaseModel):
    id: int | None
    type: str | None
    status: RequestStatus

    @classmethod
    def from_query_result(cls, id, type, status):
        return cls(id=id, type=type, status=status)


class RequestSingle(BaseModel):
    id: int
    user_id: int
    type: str
    player_id: int | None
    status: str

    @classmethod
    def from_query_result(cls, id, user_id, type, player_id, status):
        return cls(
            id=id, user_id=user_id, type=type, player_id=player_id, status=status
        )


class Game(BaseModel):
    id: int
    datetime: str | None
    # date_time: datetime | None
    tournament_id: int | None
    player1_name: str | None
    score1: int | None
    score2: int | None
    player2_name: str | None
    game_format_id: int
    round: int | None
    location: str | None

    @classmethod
    def from_query_result(
        cls,
        id,
        datetime,
        tournament_id,
        player1_name,
        score1,
        score2,
        player2_name,
        game_format_id,
        round,
        location,
    ):
        if datetime:
            return cls(
                id=id,
                datetime=datetime.strftime("%Y-%d-%m %H:%M:%S"),
                tournament_id=tournament_id,
                player1_name=player1_name,
                score1=score1,
                score2=score2,
                player2_name=player2_name,
                game_format_id=game_format_id,
                round=round,
                location=location,
            )
        return cls(
            id=id,
            datetime=datetime,
            tournament_id=tournament_id,
            player1_name=player1_name,
            score1=score1,
            score2=score2,
            player2_name=player2_name,
            game_format_id=game_format_id,
            round=round,
            location=location,
        )


class GameResponseModel(BaseModel):
    id: int
    datetime: str | None
    # date_time: datetime | None
    tournament_id: int | None
    player1_name: str | None
    score1: int | None
    score2: int | None
    player2_name: str | None
    game_format_id: int
    round: int | str | None
    location: str | None

    @classmethod
    def from_query_result(
        cls,
        id,
        datetime,
        tournament_id,
        player1_name,
        score1,
        score2,
        player2_name,
        game_format_id,
        round,
        location,
    ):
        if datetime:
            return cls(
                id=id,
                datetime=datetime.strftime("%Y-%d-%m %H:%M:%S"),
                tournament_id=tournament_id,
                player1_name=player1_name,
                score1=score1,
                score2=score2,
                player2_name=player2_name,
                game_format_id=game_format_id,
                round=round,
                location=location,
            )
        return cls(
            id=id,
            datetime=datetime,
            tournament_id=tournament_id,
            player1_name=player1_name,
            score1=score1,
            score2=score2,
            player2_name=player2_name,
            game_format_id=game_format_id,
            round=round,
            location=location,
        )


# class GameWithRoundAndPhase(BaseModel):
#     id: int
#     date_time: datetime | None
#     tournament_id: int | None
#     player1_name: str | None
#     score1: int | None
#     score2: int | None
#     player2_name: str | None
#     game_format_id : int
#     round: int | None
#     phase: str | None
#     location: str | None

#     @classmethod
#     def from_query_result(
#               cls, id, datetime, tournament_id, player1_name,
#               score1, score2, player2_name, game_format_id, round, phase, location):
#         return cls(
#             id=id, datetime=datetime, tournament_id=tournament_id, player1_name=player1_name,
#             score1=score1, score2=score2, player2_name=player2_name,
#             game_format_id=game_format_id,
#             round=round, phase=phase, location=location)


class IndependentGame(BaseModel):
    id: int | None = None
    date_time: datetime | None = None
    player1_name: str
    score1: int | None = None
    score2: int | None = None
    player2_name: str
    game_format_id: int
    round: int | None = None  # Do we need round for an independent game ??
    location: str | None = None

    @classmethod
    def from_query_result(
        cls,
        id,
        datetime,
        player1_name,
        score1,
        score2,
        player2_name,
        game_format_id,
        round,
        location,
    ):
        return cls(
            id=id,
            datetime=datetime,
            player1_name=player1_name,
            score1=score1,
            score2=score2,
            player2_name=player2_name,
            game_format_id=game_format_id,
            round=round,
            location=location,
        )


class GameUpdate(BaseModel):
    # datetime: datetime | None = None
    datetime: str | None = None
    score_player_1: int | None = None
    score_player_2: int | None = None
    location: str | None = None


class TournamentStandingsResponse(BaseModel):
    place: int
    player_name: str
    points: float

    @classmethod
    def from_query_result(cls, place, player_name, points):
        return cls(place=place, player_name=player_name, points=points)


class GameFormat(BaseModel):
    id: int | None = None
    time_limit: int | None = None
    score_limit: int | None = None
    loss_points: float = 0.0
    draw_points: float
    win_points: float


class GameFormatResponseModel(BaseModel):
    id: int | None = None
    time_limit: int | None = None
    score_limit: int | None = None
    loss_points: float
    draw_points: float
    win_points: float

    @classmethod
    def from_query_result(
        cls, id, time_limit, score_limit, loss_points, draw_points, win_points
    ):
        return cls(
            id=id,
            time_limit=time_limit,
            score_limit=score_limit,
            loss_points=loss_points,
            draw_points=draw_points,
            win_points=win_points,
        )