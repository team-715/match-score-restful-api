# pylint: disable=missing-docstring


from mariadb import connect
from mariadb.connections import Connection
from common.config import settings


def _get_connection() -> Connection:
    connection = connect(
        user=settings.database_user,
        password=settings.database_password,
        host=settings.database_hostname,
        port=settings.database_port,
        database=settings.database_name,
    )
    return connection


def read_query(sql: str, sql_params: tuple = ()):
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return list(cursor)


def read_query_one(sql: str, sql_params: tuple = ()):
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)

        return cursor.fetchone()


def insert_query(sql: str, sql_params: tuple = ()) -> int:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.lastrowid


def update_query(sql: str, sql_params: tuple = ()) -> bool:
    with _get_connection() as conn:
        cursor = conn.cursor()
        cursor.execute(sql, sql_params)
        conn.commit()

        return cursor.rowcount
        # return True in Database Access demo
