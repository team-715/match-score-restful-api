# pylint: disable=missing-docstring
from fastapi import APIRouter, Header

from common.auth import get_user_or_raise_401
from common.responses import Forbidden, NoContent, NotFound
from data.models import Player, PlayerShort, Tags
from services import player_service, user_service


players_router = APIRouter(prefix="/players")


@players_router.get("/", response_model=list[Player], tags=[Tags.PLAYER])
def get_players(country: str = "", club: str = "", offset: int = 0, limit: int = 10):
    query_params = country, club, limit, offset
    result = player_service.get_all(*query_params)

    return result or NotFound(content="No database players")


@players_router.get("/{player_id}", response_model=Player, tags=[Tags.PLAYER])
def get_player_by_id(player_id: int):
    if not player_service.exist_by_id(player_id):
        return NotFound(
            content=f"No player with ID {player_id} or it has been deleted from the database"
        )

    return player_service.get_by_id(player_id)


# @players_router.get("/{id}/stats", response_model=PlayerExtended, tags=[Tags.p])
# def get_player_statistics(id: int):
#     if not player_service.exist_by_id(id):
#         return NotFound(content=f"No database player with id {id}")

#     return player_service.attach_statistics(player_service.get_by_id(id))


@players_router.post("/", response_model=Player, tags=[Tags.PLAYER])
def create_player(body: Player, token: str = Header()):
    user = get_user_or_raise_401(token)
    if not (user.is_director() or user.is_admin()):
        return Forbidden(content="Creation allowed for admin or director")

    result = player_service.add(body)

    return result


@players_router.put("/{player_id}", response_model=Player, tags=[Tags.PLAYER])
def update_player(player_id: int, body: PlayerShort, token: str = Header()):
    user = get_user_or_raise_401(token)

    player = player_service.get_by_id(player_id)
    if not player:
        return NotFound(content=f"No database player with id {player_id}")

    user_associated = user_service.get_user_associated_with_player_id(player.id)

    if user_associated:
        if user_associated.id == user.id or user.is_admin():
            result = player_service.update(body, player)
        else:
            result = Forbidden(
                content="Player is not associated with currently logged-in user."
            )
    if not user_associated:
        if user.is_admin() or user.is_director():
            result = player_service.update(body, player)
        else:
            result = Forbidden(
                content="Your account does not have required permissions."
            )

    return result


@players_router.delete("/{player_id}", tags=[Tags.PLAYER])
def delete_player(player_id: int, token: str = Header()):
    user = get_user_or_raise_401(token)
    if not user.is_admin():
        return Forbidden(content="Only admin can delete a player")

    player = player_service.get_by_id(player_id)
    if not player:
        return NotFound(content=f"No database request with id {player_id}")

    player_service.delete(player_id)
    # is it 204 the right status code
    return NoContent()
