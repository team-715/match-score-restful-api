# pylint: disable=missing-docstring

from fastapi import APIRouter, Header
from common.emails import send_email, create_msg_request_update
from common.auth import get_user_or_raise_401
from common.responses import BadRequest, Forbidden, NoContent, NotFound
from data.models import RequestLink, PromoteRequest, Request, RequestSingle, Tags
from services import request_service, player_service, user_service


requests_router = APIRouter(prefix="/requests")


@requests_router.get("/", response_model=list[Request], tags=[Tags.REQUEST])
def get_requests(token: str = Header(), limit: int = 10, offset: int = 0):
    user = get_user_or_raise_401(token)
    if user.is_admin():
        result = request_service.get_all(limit, offset)
        return result or NotFound(content="No database requests")

    return Forbidden(content="Only admin can view requests")


@requests_router.get("/{request_id}", response_model=RequestSingle, tags=[Tags.REQUEST])
def get_request_by_id(request_id: int, token: str = Header()):
    user = get_user_or_raise_401(token)
    if user.is_admin():
        result = request_service.get_by_id(request_id)
        return result or NotFound(content=f"No database request with id {request_id}")

    return Forbidden(content="Only admin can view a request")


@requests_router.post("/promote", response_model=PromoteRequest, tags=[Tags.REQUEST])
def create_promote_request(token: str = Header()):
    user = get_user_or_raise_401(token)
    result = request_service.add_promote_request(user.id)

    return result


@requests_router.post("/link", response_model=RequestLink, tags=[Tags.REQUEST])
def create_link_request(body: RequestLink, token: str = Header()):
    user = get_user_or_raise_401(token)

    if (
        not body.associate_with_player_id
    ):
        return BadRequest(content="Please, insert player_id for account link")
    if not player_service.exist_by_id(body.associate_with_player_id):
        return NotFound(
            content=f"Player with id {body.associate_with_player_id} doesn't exist"
        )

    result = request_service.add_link_request(user.id, body.associate_with_player_id)

    return result


@requests_router.patch("/{request_id}", response_model=RequestSingle, tags=[Tags.REQUEST])
def update_request(request_id: int, body: Request, token: str = Header()):
    user = get_user_or_raise_401(token)
    if user.is_admin():
        request = request_service.get_by_id(request_id)
        if not request:
            return NotFound(content=f"No database request with id {request_id}")
        result = request_service.update(body.status, request)

        # send a notification
        user = user_service.find_by_id(result.user_id)
        send_email(
            user.email,
            create_msg_request_update,
            request_type=result.type,
            status=result.status,
        )

        return result

    return Forbidden(content="Only admin can approve or decline a request")


@requests_router.delete("/{request_id}", tags=[Tags.REQUEST])
def delete_request(request_id: int, token: str = Header()):
    user = get_user_or_raise_401(token)
    if not user.is_admin():
        return Forbidden(content="Only admin can delete a request")

    request = request_service.get_by_id(request_id)
    if not request:
        return NotFound(content=f"No database request with id {request_id}")

    request_service.delete(request_id)
    return NoContent()
