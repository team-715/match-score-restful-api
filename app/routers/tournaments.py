# pylint: disable=missing-docstring
# pylint: disable=E1133
from fastapi import APIRouter, Header
from data import database
from data.models import (
    AllTournamentResponseModel,
    Tags,
    Tournament,
    TournamentStatus,
    TournamentUpdate,
)
from common.auth import get_user_or_raise_401
from common.emails import send_email, create_msg_added_in_tournament
from common.responses import BadRequest, Forbidden, NotFound
from common.utils import is_a_power_of_two, is_even
from services import tournament_service, player_service, user_service


tournaments_router = APIRouter(prefix="/tournaments")


@tournaments_router.get(
    "/", response_model=list[AllTournamentResponseModel], tags=[Tags.TOURNAMENT]
)
def view_tournaments(
    status: TournamentStatus | None = None, limit: int = 5, offset: int = 0
):
    result = tournament_service.get_all(status, offset, limit)

    return result or NotFound(content="No database tournaments")


# @tournaments_router.get('/', response_model=list[Tournament], tags=[Tags.t])
@tournaments_router.get("/{tournament_id}", tags=[Tags.TOURNAMENT])
def view_tournament(tournament_id: int):
    result = tournament_service.get_by_id(tournament_id)

    return result or NotFound(content=f"No database tournament with id {tournament_id}")


@tournaments_router.post("/", tags=[Tags.TOURNAMENT])
def create_tournament(tournament: Tournament, token: str = Header()):
    user = get_user_or_raise_401(token)
    players_count = len(tournament.participants)
    participants_copy = tournament.participants.copy()

    if user.is_admin() or user.is_director():
        # create new players
        players_to_create = player_service.find_non_existing_players(
            tournament.participants
        )
        if players_to_create:
            player_service.create_new_players(players_to_create)

        if tournament.format == 0:  # league
            if (
                is_even(players_count)
                and 2 <= players_count <= 20
                and database.read_query_one(
                    "SELECT id from game_formats where id = ?",
                    (tournament.game_format,),
                )
            ):
                # ^ proverka dali game_format sushtestvuva tui kato e foreign key
                # create league tournament
                result = tournament_service.create_league(tournament)
            else:
                return BadRequest("Invalid game format or players count")

        elif tournament.format == 1:  # knockout
            if (
                is_a_power_of_two(players_count)
                and 2 <= players_count <= 128
                and database.read_query_one(
                    "SELECT id from game_formats where id = ?",
                    (tournament.game_format,),
                )
            ):
                # create knockout tournament
                result = tournament_service.create_knockout(tournament)
            else:
                return BadRequest(
                    # "A knockout tournament cannot be created.
                    # Please, insert number of participants which is a power of two."
                    "Invalid game format or players count"
                )
        else:
            return BadRequest("Invalid tournament format")

        # email notification that user associated with player profile is added to a tournament
        player_ids = player_service.get_player_ids_from_fullname(participants_copy)
        if player_ids:
            for player_id in player_ids:
                user_email = user_service.get_user_email_associated_with_player_id(player_id)
                if user_email:
                    send_email(
                        email=user_email,
                        func=create_msg_added_in_tournament,
                        player_name=player_service.get_by_id(player_id).fullname,
                    )

        return result  # Tournament object

    return Forbidden("You sir are not authorized to do that")


@tournaments_router.get("/{tournament_id}/games", tags=[Tags.TOURNAMENT])
def view_all_games_in_tournament(tournament_id: int, round_: int | None = None):
    if round_:
        result = tournament_service.get_games_from_round(tournament_id, round_)
    else:
        result = tournament_service.get_all_games_in_tournament(tournament_id)

    return result or NotFound("")


@tournaments_router.get("/{tournament_id}/standings", tags=[Tags.TOURNAMENT])
def view_current_standings_in_tournament(tournament_id: int):
    result = tournament_service.get_current_standings(tournament_id)

    return result or NotFound("No standings for this tournament or no such tournament")


@tournaments_router.put("/{tournament_id}", tags=[Tags.TOURNAMENT])
def update_tournament(tournament_id: int, body: TournamentUpdate, token: str = Header()):
    user = get_user_or_raise_401(token)

    if user.is_admin() or user.is_director():
        old = tournament_service.get_by_id(tournament_id)
        if old:
            return tournament_service.update(old, body)
        return NotFound(f"Tournament with id #{tournament_id} does not exist")
    return Forbidden("You are not authorized to update events")
