# pylint: disable=missing-docstring

from fastapi import APIRouter, Header
from data.models import (
    PasswordChange,
    Tags,
    LoginData,
    Token,
    User,
    UserBody,
    UserResponse,
)
from common.auth import get_user_or_raise_401, create_token
from common.responses import Accepted, BadRequest, Forbidden, NotFound
from services import user_service, player_service


users_router = APIRouter(prefix="/users")


@users_router.get("/", response_model=list[UserResponse], tags=[Tags.USER])
def get_users(token: str = Header(), limit: int = 10, offset: int = 0):
    user = get_user_or_raise_401(token)
    if user.is_admin():
        result = user_service.get_all(limit, offset)
        return result or NotFound(content="No database users")

    return Forbidden(content="Only admin can view users")


@users_router.get("/{user_id}", response_model=UserResponse, tags=[Tags.USER])
def get_by_id(user_id: int, token: str = Header()):
    user = get_user_or_raise_401(token)
    if user.is_admin():
        result = user_service.get_by_id(user_id)
        return result or NotFound(content=f"No database user with id: {user_id}")

    return Forbidden(content="Only admin can view users")


@users_router.put("/{user_id}", response_model=UserResponse, tags=[Tags.USER])
def update_user(user_id: int, body: UserBody, token: str = Header()):
    user = get_user_or_raise_401(token)
    if user.is_admin():
        user = user_service.get_by_id(user_id)
        if not user:
            return NotFound(content=f"No database user with id {user_id}")
        if body.player_id:
            player = player_service.get_by_id(body.player_id)
            if not player:
                return NotFound(content=f"No database player with id {body.player_id}")

        result = user_service.update(body, user)
        return result

    return Forbidden(content="Only admin can edit an user")


@users_router.patch("/{user_id}", response_model=UserResponse, tags=[Tags.USER])
def update_password(user_id: int, body: PasswordChange, token: str = Header()):
    user = get_user_or_raise_401(token)
    user_to_update = user_service.get_by_id(user_id)
    if not user_to_update:
        return NotFound(content=f"No database user with id {user_id}")

    if user.id != user_to_update.id:
        return Forbidden(content="Not have required permissions to change the resource")

    if user.id == user_to_update.id and user.email == body.email:
        if user_service.try_login(email=body.email, password=body.old_password):
            return Accepted(content="I will change the password but logic is missing")
        return BadRequest("Invalid password")
    return BadRequest("Invalid email")


@users_router.get("/info", response_model=User, tags=[Tags.USER])
def user_info(token: str = Header()):
    return get_user_or_raise_401(token)


@users_router.post("/login", response_model=Token, tags=[Tags.USER])
def login(data: LoginData):
    user = user_service.try_login(data.email, data.password)
    if user:
        return create_token(user)
    return BadRequest(content="Invalid login data")


@users_router.post("/register", response_model=UserResponse, tags=[Tags.USER])
def register(data: LoginData):
    user = user_service.create(data.email, data.password)

    return user or BadRequest(f"User with email: {data.email} already exists")
