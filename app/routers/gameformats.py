# pylint: disable=missing-docstring

from fastapi import APIRouter, Header
from common.auth import get_user_or_raise_401
from common.responses import Forbidden, NotFound
from data.models import GameFormat, GameFormatResponseModel, Tags
from services import gameformat_service


gameformats_router = APIRouter(prefix="/gameformats")


@gameformats_router.get("/", response_model=list[GameFormatResponseModel], tags=[Tags.FORMAT])
def view_all_game_formats(token: str = Header()):
    get_user_or_raise_401(token)

    # if user.is_admin() or user.is_director():
    result = gameformat_service.get_all()

    return result or NotFound("There are currently no game formats in the system")


@gameformats_router.get("/{format_id}", response_model=GameFormatResponseModel, tags=[Tags.FORMAT])
def view_game_format(format_id: int, token: str = Header()):
    get_user_or_raise_401(token)
    # if user.is_admin() or user.is_director():

    result = gameformat_service.get_by_id(format_id)

    return result or NotFound(f"No game format with id# {format_id}")


@gameformats_router.post("/", response_model=GameFormat, tags=[Tags.FORMAT])
def create_game_format(game_format: GameFormat, token: str = Header()):
    user = get_user_or_raise_401(token)

    if user.is_admin() or user.is_director():
        result = gameformat_service.create(game_format)

        return result
    return Forbidden("You are not authorized")
