# pylint: disable=missing-docstring

from fastapi import APIRouter
from common.responses import NotFound
from data.models import PlayerExtended, Tags
from services import player_service, statistic_service


statistics_router = APIRouter(prefix="/stats")


@statistics_router.get("/{player_id}", response_model=PlayerExtended, tags=[Tags.STATISTIC])
def get_player_statistics(player_id: int):
    if not player_service.exist_by_id(player_id):
        return NotFound(content=f"No database player with id {player_id}")

    player = player_service.get_by_id(player_id)
    result = statistic_service.attach_statistics(player)

    return result
