########
# GET all that are not part of a tournament
# PUT /id

# pylint: disable=missing-docstring

from fastapi import APIRouter, Header
from data.models import Game, GameUpdate, Tags, IndependentGame

from common.auth import get_user_or_raise_401
from common.emails import (
    send_email,
    create_msg_added_in_game,
    create_msg_game_update,
)
from common.responses import Forbidden, NotFound
from services import game_service, player_service, user_service

games_router = APIRouter(prefix="/games")


@games_router.get("/independent", response_model=list[IndependentGame], tags=[Tags.GAME])
def view_independent(limit: int = 10, offset: int = 0):
    result = game_service.get_all_independent(limit, offset)

    return result or NotFound("There are no independent games")


@games_router.get("/{game_id}", response_model=Game, tags=[Tags.GAME])
def view_independent_by_id(game_id: int):
    result = game_service.get_by_id(game_id)

    return result or NotFound(f"No database game with id {game_id}")


@games_router.post("/", response_model=IndependentGame, tags=[Tags.GAME])
def create_game(game: IndependentGame, token: str = Header()):
    user = get_user_or_raise_401(token)

    if user.is_admin() or user.is_director():
        participants = [game.player1_name, game.player2_name]
        # create new players
        players_to_create = player_service.find_non_existing_players(participants)
        if players_to_create:
            player_service.create_new_players(players_to_create)
        # create game
        result = game_service.create(game)

        # email notification that user associated with player profile is added to a game
        player_ids = player_service.get_existing_player_ids_from_fullname(participants)
        for player_id in player_ids:
            user_email = user_service.get_user_email_associated_with_player_id(player_id)
            if user_email:
                send_email(func=create_msg_added_in_game, email=user_email)

        return result  # Tournament object

    return Forbidden("You are not authorized to create events")


@games_router.put("/{game_id}", response_model=Game, tags=[Tags.GAME])
def update_game(game_id: int, body: GameUpdate, token: str = Header()):
    user = get_user_or_raise_401(token)

    if user.is_admin() or user.is_director():
        old = game_service.get_by_id(game_id)
        if not old:
            return NotFound(f"Game with id #{game_id} does not exist")
        if old:
            result = game_service.update(old, body)
            # email notification that a game has been updated
            if old.player1_name and old.player2_name:
                # participants = [old.player1_name, old.player2_name]
                # player_ids = player_service.get_existing_player_ids_from_fullname(
                #     participants
                # )
                # user_emails = user_service.get_user_emails_associated_with_player_ids(
                #     player_ids
                # )
                # send_in_bulk(func=create_msg_game_update, emails=user_emails, game=old)

                # to display also player name in email
                for player_name in [old.player1_name, old.player2_name]:
                    played_id = (player_service.get_by_fullname(player_name)).id
                    user_email = user_service.get_user_email_associated_with_player_id(
                        played_id
                    )
                    if user_email:
                        send_email(
                            func=create_msg_game_update,
                            email=user_email,
                            player_name=player_name,
                            game=old,
                        )

            if old.player1_name and old.player2_name is None:
                player_id = player_service.get_by_fullname(old.player1_name).id
                user_email = user_service.get_user_email_associated_with_player_id(
                    player_id
                )
                if user_email:
                    send_email(
                        func=create_msg_game_update,
                        email=user_email,
                        player_name=old.player1_name,
                        game=old,
                    )

            if old.player2_name and old.player1_name is None:
                player_id = player_service.get_by_fullname(old.player2_name).id
                user_email = user_service.get_user_email_associated_with_player_id(
                    player_id
                )
                if user_email:
                    send_email(
                        func=create_msg_game_update,
                        email=user_email,
                        player_name=old.player2_name,
                        game=old,
                    )

            return result

    return Forbidden("You are not authorized to update events")


# delete games/id -> only independent games
