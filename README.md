
***

# Match Score RESTful API - Final Teamwork

## Name
MatchScore

## Description

Tournament directors have long needed a solution that will streamline the organization and management of sport events. MatchScore aims to solve that problem by providing all the tools required

## Features

The API supports the following operations:

1. Organizing events
- Game – a one-off event
- Tournament – knockout or league format

2. Managing events
- Mark the score.
- Change the date
- Update the location

3. Player profiles
- Name, Country, Sports Club

4. Score Tracking
- Tournament and player statistics

5. Registration
- Allows event organization and management (if approved)
- Associate with player profile

6. Email notifications - informs the user when:
- His promote-to-director request has been approved/declined
- He was added to a tournament
- He was added to a game
- There has been an update to one of his games + full information about the changes

## Use cases

1. Example #1

Create a knockout tournament with Bobby Fischer, Magnus Carlsen, Hikaru Nakamura and Daniil Dubov

- The system generates a tournament scheme starting from 'Semi Finals' and randomly assigns participants to games

- The system tries to link any of the four participants to an existing profile by their full name. If a profile is not found, one will be automatically created with no country and no sports club.

- The system automatically updates one of the two finalists when a new score is added to a Semi Final

2. Example #2

Create a league tournament with Bobby Fischer, Magnus Carlsen, Hikaru Nakamura and Daniil Dubov

- The system calculates that at least 6 rounds (2leg league) are required and randomly creates games for each round
- After scoring is added for a game, the tournament standings are automatically updated

## Support
You can contact us for help:
- ivaylo.penchev.a40@learn.telerikacademy.com
- nikol.bratkova.a40@learn.telerikacademy.com

## Authors and acknowledgment
Appreciation to those who have contributed to the project:
- Ivaylo Penchev
- Nikol Bratkova

## Project status
Active
