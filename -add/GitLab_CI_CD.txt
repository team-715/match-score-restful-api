GitLab CI/CD - Automation engine

Enables teams to perform DEvOps practices:
	- CI - Continues Integration
	- CD - Continuous Delivery/Deployment

Automatically build, test, ad deploy using a GitLab pipeline.

GitLab Pipelines is a version control .yml file called .gitlab-ci.yml that lives at the root directory of a gitlab project. Automated set of sequentia steps to build, test, and deliver/deploy your code. GitLab pipelines have two main components:
	1) jobs - describe teh tasks that need to be done (compile, test or deploy code);

	2) stages - define the order in which jobs will be completed

Pipeline is a set of instructions for a program to execute.A program that executes jobs in a gitlab pipeline is called a GitLab Runner.

GitLab Runner is a separate program that can be run on your local host, VM or even docker container.


1. Creating pipeline for a gitlab project
from CI/CD section of the left side menu -> Pipelines.




for tests:
python -m unittest discover -s tests  -p "*_test.py" -v
python -m unittest discover tests  "*_test.py" -v







---------------
.gitlab-ci.yml


image: python:3.10

before_script:
  - apt-get update
  - apt-get install libmariadb-dev
  - apt-get install libmariadb3
  - python --version  # For debugging
  - pip install --upgrade pip
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate
  - cd app
  # - pip install -r requirements.txt

stages:          
  - test
  - deploy

unit-test-job:
  stage: test
  script:
    - echo "Running unit tests... This will take about 60 seconds."
    # - python -m unittest discover tests  "*_test.py" -v
    - echo "Code coverage is 87%"

lint-test-job:   # This job also runs in the test stage.
  stage: test    # It can run at the same time as unit-test-job (in parallel).
  script:
    - echo "Linting code... This will take about 10 seconds."
    # - sleep 10
    - echo "No lint issues found."

deploy-job:      # This job runs in the deploy stage.
  stage: deploy  # It only runs when *both* jobs in the test stage complete successfully.
  environment: production
  script:
    - echo "Deploying application..."
    - echo "Application successfully deployed."






