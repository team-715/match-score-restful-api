A virtual environment is a folder that has all of the code that you're going to need to run the application that you are creating. So everything that we need is just going to be installed into that folder and then we are able to use it.

To create that folder we have a little utility called virtualenv:

# 1. Install a virtual environment (globally)
pip install virtualenv

# 2. Create in Windows systems
python -m venv <folder_name>
py -3 -m venv venv

-m stands for go grab a particular module

# 3. Change the global interpreter with virtual
in VSCode from Menu Bar -> View -> Command Palette (Ctrl+Shift+P) -> Python: Select Interpreter -> + Enter interpreter path...
	.\venv\Scripts\python.exe

# 4. Change the terminal environment. Using virtual environments in Windows systems
	cmd.exe
<folder_name>\Scripts\Activate.bat

	Powershell
<folder_name>\Scripts\Activate.ps1
.\venv\Scripts\activate.ps1 (from demo in VSCode

	bash shell
. ./<folder_name>/Scripts/activate


Installing packages in a virtual environment. After activation.

# Install an individual package
pip install colorama

# Install from a list of packages
pip install -r requirements.txt

# requirements.txt
colorama

Generate the file requirements.txt
	pip freeze > requirements.txt


Install all dependencies when running the app from another machine
	pip install -r requirements.txt



