CREATE DATABASE  IF NOT EXISTS `match_score_fp` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `match_score_fp`;
-- MariaDB dump 10.19  Distrib 10.9.2-MariaDB, for Win64 (AMD64)
--
-- Host: nlikyov.asuscomm.com    Database: match_score_fp
-- ------------------------------------------------------
-- Server version	10.6.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `game_formats`
--

DROP TABLE IF EXISTS `game_formats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_formats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_limit` int(11) DEFAULT NULL,
  `score_limit` int(11) DEFAULT NULL,
  `loss_points` float DEFAULT NULL,
  `draw_points` float DEFAULT NULL,
  `win_points` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_formats`
--

LOCK TABLES `game_formats` WRITE;
/*!40000 ALTER TABLE `game_formats` DISABLE KEYS */;
INSERT INTO `game_formats` VALUES
(1,60,NULL,0,0.5,1),
(2,NULL,3,NULL,NULL,NULL),
(3,30,NULL,NULL,NULL,NULL),
(4,NULL,5,0,1,3);
/*!40000 ALTER TABLE `game_formats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `tournament_id` int(11) DEFAULT NULL,
  `player1_name` varchar(90) DEFAULT NULL,
  `player2_name` varchar(90) DEFAULT NULL,
  `score1` int(11) DEFAULT NULL,
  `score2` int(11) DEFAULT NULL,
  `game_format_id` int(11) DEFAULT NULL,
  `round` int(11) DEFAULT NULL,
  `phase` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `prev_game1_id` int(11) DEFAULT NULL,
  `prev_game2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_games_tournaments1_idx` (`tournament_id`),
  KEY `fk_games_players1_idx` (`player1_name`),
  KEY `fk_games_players2_idx` (`player2_name`),
  KEY `fk_games_game_formats1_idx` (`game_format_id`),
  KEY `fk_games_games1_idx` (`prev_game1_id`),
  KEY `fk_games_games2_idx` (`prev_game2_id`),
  CONSTRAINT `fk_games_game_formats1` FOREIGN KEY (`game_format_id`) REFERENCES `game_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_games1` FOREIGN KEY (`prev_game1_id`) REFERENCES `games` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_games2` FOREIGN KEY (`prev_game2_id`) REFERENCES `games` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_players1` FOREIGN KEY (`player1_name`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_players2` FOREIGN KEY (`player2_name`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_tournaments1` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=754 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES
(556,'2023-01-09 13:34:00',NULL,'Ivo Penchev','Misho Asenov',1,0,1,1,NULL,'Los Knyazhelis',NULL,NULL),
(557,'2023-01-09 13:34:00',NULL,'Veselin Topalov','Levon Aronian',1,0,1,NULL,NULL,'Sofia, Bulgaria',NULL,NULL),
(653,NULL,46,'Hikaru Nakamura','Fabiano Caruana',3,1,1,1,NULL,NULL,NULL,NULL),
(654,NULL,46,'Magnus Carlsen','Aleksander Grischuk',3,3,1,1,NULL,NULL,NULL,NULL),
(655,NULL,46,'Garry Kasparov','Daniil Dubov',2,1,1,1,NULL,NULL,NULL,NULL),
(656,NULL,46,'Aleksander Grischuk','Hikaru Nakamura',0,2,1,2,NULL,NULL,NULL,NULL),
(657,NULL,46,'Daniil Dubov','Fabiano Caruana',3,0,1,2,NULL,NULL,NULL,NULL),
(658,NULL,46,'Garry Kasparov','Magnus Carlsen',3,0,1,2,NULL,NULL,NULL,NULL),
(659,NULL,46,'Hikaru Nakamura','Daniil Dubov',1,1,1,3,NULL,NULL,NULL,NULL),
(660,NULL,46,'Aleksander Grischuk','Garry Kasparov',0,2,1,3,NULL,NULL,NULL,NULL),
(661,NULL,46,'Fabiano Caruana','Magnus Carlsen',0,1,1,3,NULL,NULL,NULL,NULL),
(662,NULL,46,'Garry Kasparov','Hikaru Nakamura',2,2,1,4,NULL,NULL,NULL,NULL),
(663,NULL,46,'Magnus Carlsen','Daniil Dubov',0,3,1,4,NULL,NULL,NULL,NULL),
(664,NULL,46,'Fabiano Caruana','Aleksander Grischuk',2,2,1,4,NULL,NULL,NULL,NULL),
(665,NULL,46,'Hikaru Nakamura','Magnus Carlsen',2,0,1,5,NULL,NULL,NULL,NULL),
(666,NULL,46,'Garry Kasparov','Fabiano Caruana',2,0,1,5,NULL,NULL,NULL,NULL),
(667,NULL,46,'Daniil Dubov','Aleksander Grischuk',1,0,1,5,NULL,NULL,NULL,NULL),
(668,NULL,46,'Fabiano Caruana','Hikaru Nakamura',0,1,1,6,NULL,NULL,NULL,NULL),
(669,NULL,46,'Aleksander Grischuk','Magnus Carlsen',1,0,1,6,NULL,NULL,NULL,NULL),
(670,NULL,46,'Daniil Dubov','Garry Kasparov',1,1,1,6,NULL,NULL,NULL,NULL),
(671,NULL,46,'Hikaru Nakamura','Aleksander Grischuk',3,1,1,7,NULL,NULL,NULL,NULL),
(672,NULL,46,'Fabiano Caruana','Daniil Dubov',0,4,1,7,NULL,NULL,NULL,NULL),
(673,NULL,46,'Magnus Carlsen','Garry Kasparov',0,5,1,7,NULL,NULL,NULL,NULL),
(674,NULL,46,'Daniil Dubov','Hikaru Nakamura',2,1,1,8,NULL,NULL,NULL,NULL),
(675,NULL,46,'Garry Kasparov','Aleksander Grischuk',3,1,1,8,NULL,NULL,NULL,NULL),
(676,NULL,46,'Magnus Carlsen','Fabiano Caruana',2,3,1,8,NULL,NULL,NULL,NULL),
(677,NULL,46,'Hikaru Nakamura','Garry Kasparov',1,1,1,9,NULL,NULL,NULL,NULL),
(678,NULL,46,'Daniil Dubov','Magnus Carlsen',2,0,1,9,NULL,NULL,NULL,NULL),
(679,NULL,46,'Aleksander Grischuk','Fabiano Caruana',1,2,1,9,NULL,NULL,NULL,NULL),
(680,NULL,46,'Magnus Carlsen','Hikaru Nakamura',0,3,1,10,NULL,NULL,NULL,NULL),
(681,NULL,46,'Fabiano Caruana','Garry Kasparov',1,4,1,10,NULL,NULL,NULL,NULL),
(682,NULL,46,'Aleksander Grischuk','Daniil Dubov',1,3,1,10,NULL,NULL,NULL,NULL),
(731,'2023-02-05 10:00:00',52,'Magnus Carlsen','Hikaru Nakamura',0,2,2,NULL,'1/8 Final','Petrich, California',NULL,NULL),
(732,NULL,52,'Dancho Lechev','Fabiano Caruana',NULL,NULL,2,NULL,'1/8 Final','Hong Kong SAR',NULL,NULL),
(733,NULL,52,'Chi Cheng','Ivo Penchev',NULL,NULL,2,NULL,'1/8 Final','Hong Kong SAR',NULL,NULL),
(734,NULL,52,'Veselin Topalov','Sisi Peneva',NULL,NULL,2,NULL,'1/8 Final','Hong Kong SAR',NULL,NULL),
(735,NULL,52,'Sun Pin','Jack Daniel',NULL,NULL,2,NULL,'1/8 Final','Hong Kong SAR',NULL,NULL),
(736,NULL,52,'Levon Aronian','Garry Kasparov',NULL,NULL,2,NULL,'1/8 Final','Hong Kong SAR',NULL,NULL),
(737,'2023-01-09 13:34:00',52,'Daniil Dubov','Misho Asenov',2,3,2,NULL,'1/8 Final','Sofia, Bulgaria',NULL,NULL),
(738,'2023-01-09 13:34:00',52,'Lili Denevska','Radko Radev',1,3,2,NULL,'1/8 Final','Sofia, Bulgaria',NULL,NULL),
(739,NULL,52,'Hikaru Nakamura',NULL,NULL,NULL,2,NULL,'Quarter Final','Hong Kong SAR',731,732),
(740,NULL,52,NULL,NULL,NULL,NULL,2,NULL,'Quarter Final','Hong Kong SAR',733,734),
(741,NULL,52,NULL,NULL,NULL,NULL,2,NULL,'Quarter Final','Hong Kong SAR',735,736),
(742,NULL,52,'Misho Asenov','Radko Radev',NULL,NULL,2,NULL,'Quarter Final','Hong Kong SAR',737,738),
(743,NULL,52,NULL,NULL,NULL,NULL,2,NULL,'Semi Final','Hong Kong SAR',739,740),
(744,NULL,52,NULL,NULL,NULL,NULL,2,NULL,'Semi Final','Hong Kong SAR',741,742),
(745,NULL,52,NULL,NULL,NULL,NULL,2,NULL,'Final','Hong Kong SAR',743,744),
(746,NULL,NULL,'Radko Radev','Daniil Dubov',1,2,1,NULL,NULL,'Moskow, RU',NULL,NULL),
(747,NULL,NULL,'Radko Radev','Daniil Dubov',1,0,1,NULL,NULL,'Sofia, BG',NULL,NULL),
(748,NULL,NULL,'Daniil Dubov','Misho Asenov',0,2,1,NULL,NULL,'Sofia, BG',NULL,NULL),
(749,NULL,NULL,'Daniil Dubov','Misho Asenov',2,1,1,NULL,NULL,'Moskow, RU',NULL,NULL),
(750,NULL,NULL,'Aleksander Grischuk','Daniil Dubov',0,3,1,NULL,NULL,NULL,NULL,NULL),
(751,NULL,NULL,'Daniil Dubov','Aleksander Grischuk',1,2,NULL,NULL,NULL,NULL,NULL,NULL),
(752,NULL,NULL,'Fabiano Caruana','Daniil Dubov',NULL,NULL,1,NULL,NULL,NULL,NULL,NULL),
(753,NULL,NULL,'Levon Aronian','Veselin Topalov',1,2,1,NULL,NULL,'Ankara, TR',NULL,NULL);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(90) NOT NULL,
  `country` varchar(45) DEFAULT NULL,
  `club` varchar(45) DEFAULT NULL,
  `tournaments_won` int(11) DEFAULT NULL,
  `tournaments_played` int(11) DEFAULT NULL,
  `matches_won` int(11) DEFAULT NULL,
  `matches_played` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fullname_UNIQUE` (`fullname`)
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES
(99,'Tai Lin','Taiwan','SK Taipe City',NULL,NULL,NULL,NULL),
(100,'Chen Chen','Taiwan','SK Taipe United',NULL,NULL,NULL,NULL),
(111,'Dancho Lechev','Bulgaria','SK Real Sofia',NULL,NULL,NULL,NULL),
(112,'Sisi Peneva','Bulgaria','SK Real Sofia',NULL,NULL,NULL,NULL),
(113,'Radko Radev','Bulgaria','SK Real Sofia',NULL,NULL,NULL,NULL),
(114,'Lili Denevska','Bulgaria','SK Real Sofia',NULL,NULL,NULL,NULL),
(281,'Magnus Carlsen',NULL,NULL,NULL,NULL,NULL,NULL),
(282,'Hikaru Nakamura',NULL,NULL,NULL,NULL,NULL,NULL),
(283,'Fabiano Caruana',NULL,NULL,NULL,NULL,NULL,NULL),
(289,'Jack Daniel','USA','SK Master Tigers',NULL,NULL,NULL,NULL),
(290,'Anne Marie Nicholson','USA','SK Master Tigers',NULL,NULL,NULL,NULL),
(291,'Anne-Marie Nicholson-Jackson','USA','SK Master Tigers',NULL,NULL,NULL,NULL),
(292,'Garry Kasparov','Russia','SK Moskow City',NULL,NULL,NULL,NULL),
(293,'Aleksander Grischuk',NULL,NULL,NULL,NULL,NULL,NULL),
(294,'Daniil Dubov','Russia','SK CSKA',NULL,NULL,NULL,NULL),
(295,'Sun Pin',NULL,NULL,NULL,NULL,NULL,NULL),
(296,'Chi Cheng',NULL,NULL,NULL,NULL,NULL,NULL),
(299,'Ivo Penchev','Bulgaria',NULL,NULL,NULL,NULL,NULL),
(300,'Misho Asenov','Turkey','Fenerbahce S.K.',0,1000,0,9000),
(301,'Veselin Topalov','Bulgaria','SK Tigri',NULL,NULL,NULL,NULL),
(302,'Levon Aronian',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `player_id` int(11) DEFAULT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`),
  KEY `fk_requests_users1_idx` (`user_id`),
  KEY `fk_requests_players1_idx` (`player_id`),
  CONSTRAINT `fk_requests_players1` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_requests_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` VALUES
(22,11,'promote',NULL,'pending'),
(23,11,'link',111,'declined'),
(24,11,'link',111,'approved'),
(25,11,'promote',NULL,'declined');
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournaments`
--

DROP TABLE IF EXISTS `tournaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `format` tinyint(4) NOT NULL,
  `game_format_id` int(11) DEFAULT NULL,
  `number_of_rounds` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `prizes` varchar(1000) DEFAULT NULL,
  `winner` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tournaments_game_formats1_idx` (`game_format_id`),
  KEY `fk_tournaments_players1_idx` (`winner`),
  CONSTRAINT `fk_tournaments_game_formats1` FOREIGN KEY (`game_format_id`) REFERENCES `game_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournaments_players1` FOREIGN KEY (`winner`) REFERENCES `players` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournaments`
--

LOCK TABLES `tournaments` WRITE;
/*!40000 ALTER TABLE `tournaments` DISABLE KEYS */;
INSERT INTO `tournaments` VALUES
(3,'Chess League',0,1,2,NULL,NULL,NULL,NULL,NULL),
(5,'New Chess League',0,1,10,NULL,NULL,'Sofia, Bulgaria',NULL,NULL),
(6,'Past Event 1',0,1,NULL,'2019-05-10','2019-06-10','NY, USA',NULL,100),
(7,'Past Event 2',0,1,NULL,'2020-04-05','2020-04-28','Petrich, Bulgaria',NULL,100),
(8,'Future Event 1',1,NULL,NULL,'2025-10-01','2025-11-15','Budapest, Hungary',NULL,NULL),
(9,'Present Event 1',1,NULL,NULL,'2022-10-15','2022-12-01','Sofia, Bulgaria',NULL,NULL),
(10,'Present Event 2',1,NULL,NULL,'1998-02-02',NULL,NULL,NULL,NULL),
(11,'Future Event 2',1,NULL,NULL,'2024-02-02',NULL,NULL,NULL,NULL),
(12,'Past Event 3',1,NULL,NULL,NULL,'1998-02-02',NULL,NULL,292),
(13,'Present Event 5',1,NULL,NULL,NULL,'2024-02-02',NULL,NULL,NULL),
(46,'New Chess League',0,1,10,'2023-01-09',NULL,'Sofia, Bulgaria',NULL,NULL),
(52,'Asian Championship 2023',1,2,4,NULL,NULL,'Hong Kong SAR','1st place: $100 000, 2nd place: $30 000, 3th place: $5 000',NULL);
/*!40000 ALTER TABLE `tournaments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournaments_players`
--

DROP TABLE IF EXISTS `tournaments_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments_players` (
  `tournament_id` int(11) NOT NULL,
  `player_name` varchar(90) NOT NULL,
  `points` float DEFAULT 0,
  KEY `fk_table1_tournaments1_idx` (`tournament_id`),
  KEY `fk_table1_players1_idx` (`player_name`),
  CONSTRAINT `fk_tournaments_players_players1` FOREIGN KEY (`player_name`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournaments_players_tournaments1` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournaments_players`
--

LOCK TABLES `tournaments_players` WRITE;
/*!40000 ALTER TABLE `tournaments_players` DISABLE KEYS */;
INSERT INTO `tournaments_players` VALUES
(46,'Magnus Carlsen',0),
(46,'Hikaru Nakamura',2),
(46,'Fabiano Caruana',0),
(46,'Garry Kasparov',1.5),
(46,'Aleksander Grischuk',1),
(46,'Daniil Dubov',2.5),
(52,'Ivo Penchev',0),
(52,'Misho Asenov',0),
(52,'Hikaru Nakamura',0),
(52,'Magnus Carlsen',0),
(52,'Garry Kasparov',0),
(52,'Veselin Topalov',0),
(52,'Levon Aronian',0),
(52,'Fabiano Caruana',0),
(52,'Dancho Lechev',0),
(52,'Sisi Peneva',0),
(52,'Radko Radev',0),
(52,'Lili Denevska',0),
(52,'Jack Daniel',0),
(52,'Sun Pin',0),
(52,'Chi Cheng',0),
(52,'Daniil Dubov',0);
/*!40000 ALTER TABLE `tournaments_players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(65) NOT NULL,
  `role` varchar(45) NOT NULL DEFAULT 'user',
  `player_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_player_profiles_idx` (`player_id`),
  CONSTRAINT `fk_users_player_profiles` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES
(1,'match.score.fp@gmail.com','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','admin',NULL),
(10,'yovo.tonchev7@abv.bg','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','director',NULL),
(11,'dancholechev@yahoo.com','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','user',NULL),
(13,'system_user@mail.bg','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','user',113);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-03 13:51:48
