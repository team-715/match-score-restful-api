CREATE DATABASE  IF NOT EXISTS `match_score_fp` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `match_score_fp`;
-- MariaDB dump 10.19  Distrib 10.9.2-MariaDB, for Win64 (AMD64)
--
-- Host: nlikyov.asuscomm.com    Database: match_score_fp
-- ------------------------------------------------------
-- Server version	10.6.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `game_formats`
--

DROP TABLE IF EXISTS `game_formats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_formats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_limit` int(11) DEFAULT NULL,
  `score_limit` int(11) DEFAULT NULL,
  `loss_points` float DEFAULT 0,
  `draw_points` float NOT NULL,
  `win_points` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_formats`
--

LOCK TABLES `game_formats` WRITE;
/*!40000 ALTER TABLE `game_formats` DISABLE KEYS */;
INSERT INTO `game_formats` VALUES
(1,60,NULL,0,0.5,1),
(2,NULL,3,0,0.5,1),
(3,30,NULL,0,0.5,1),
(4,NULL,5,0,1,3),
(6,87,9,0,2,5),
(7,60,5,0,1,3);
/*!40000 ALTER TABLE `game_formats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `tournament_id` int(11) DEFAULT NULL,
  `player1_name` varchar(90) DEFAULT NULL,
  `player2_name` varchar(90) DEFAULT NULL,
  `score1` int(11) DEFAULT NULL,
  `score2` int(11) DEFAULT NULL,
  `game_format_id` int(11) DEFAULT NULL,
  `round` int(11) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `prev_game1_id` int(11) DEFAULT NULL,
  `prev_game2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_games_tournaments1_idx` (`tournament_id`),
  KEY `fk_games_players1_idx` (`player1_name`),
  KEY `fk_games_players2_idx` (`player2_name`),
  KEY `fk_games_game_formats1_idx` (`game_format_id`),
  KEY `fk_games_games1_idx` (`prev_game1_id`),
  KEY `fk_games_games2_idx` (`prev_game2_id`),
  CONSTRAINT `fk_games_game_formats1` FOREIGN KEY (`game_format_id`) REFERENCES `game_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_games1` FOREIGN KEY (`prev_game1_id`) REFERENCES `games` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_games2` FOREIGN KEY (`prev_game2_id`) REFERENCES `games` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_players1` FOREIGN KEY (`player1_name`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_players2` FOREIGN KEY (`player2_name`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_tournaments1` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=966 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES
(556,'2023-01-09 13:34:00',NULL,'Ivo Penchev','Misho Asenov',2,0,1,1,'Los Knyazhelis',NULL,NULL),
(557,'2023-01-09 13:34:00',NULL,'Veselin Topalov','Levon Aronian',1,0,1,NULL,'Sofia, Bulgaria',NULL,NULL),
(653,NULL,46,'Hikaru Nakamura','Fabiano Caruana',3,1,1,1,NULL,NULL,NULL),
(654,NULL,46,'Magnus Carlsen','Aleksander Grischuk',3,3,1,1,NULL,NULL,NULL),
(655,NULL,46,'Garry Kasparov','Daniil Dubov',1,2,1,1,NULL,NULL,NULL),
(656,NULL,46,'Aleksander Grischuk','Hikaru Nakamura',0,2,1,2,NULL,NULL,NULL),
(657,NULL,46,'Daniil Dubov','Fabiano Caruana',3,0,1,2,NULL,NULL,NULL),
(658,NULL,46,'Garry Kasparov','Magnus Carlsen',3,0,1,2,NULL,NULL,NULL),
(659,NULL,46,'Hikaru Nakamura','Daniil Dubov',1,1,1,3,NULL,NULL,NULL),
(660,NULL,46,'Aleksander Grischuk','Garry Kasparov',0,2,1,3,NULL,NULL,NULL),
(661,NULL,46,'Fabiano Caruana','Magnus Carlsen',0,1,1,3,NULL,NULL,NULL),
(662,NULL,46,'Garry Kasparov','Hikaru Nakamura',2,2,1,4,NULL,NULL,NULL),
(663,NULL,46,'Magnus Carlsen','Daniil Dubov',0,3,1,4,NULL,NULL,NULL),
(664,NULL,46,'Fabiano Caruana','Aleksander Grischuk',2,2,1,4,NULL,NULL,NULL),
(665,NULL,46,'Hikaru Nakamura','Magnus Carlsen',2,0,1,5,NULL,NULL,NULL),
(666,NULL,46,'Garry Kasparov','Fabiano Caruana',2,0,1,5,NULL,NULL,NULL),
(667,NULL,46,'Daniil Dubov','Aleksander Grischuk',1,0,1,5,NULL,NULL,NULL),
(668,NULL,46,'Fabiano Caruana','Hikaru Nakamura',0,1,1,6,NULL,NULL,NULL),
(669,NULL,46,'Aleksander Grischuk','Magnus Carlsen',1,0,1,6,NULL,NULL,NULL),
(670,NULL,46,'Daniil Dubov','Garry Kasparov',2,1,1,6,NULL,NULL,NULL),
(671,NULL,46,'Hikaru Nakamura','Aleksander Grischuk',3,1,1,7,NULL,NULL,NULL),
(672,NULL,46,'Fabiano Caruana','Daniil Dubov',0,4,1,7,NULL,NULL,NULL),
(673,NULL,46,'Magnus Carlsen','Garry Kasparov',0,5,1,7,NULL,NULL,NULL),
(674,NULL,46,'Daniil Dubov','Hikaru Nakamura',2,1,1,8,NULL,NULL,NULL),
(675,NULL,46,'Garry Kasparov','Aleksander Grischuk',3,1,1,8,NULL,NULL,NULL),
(676,NULL,46,'Magnus Carlsen','Fabiano Caruana',2,3,1,8,NULL,NULL,NULL),
(677,NULL,46,'Hikaru Nakamura','Garry Kasparov',2,1,1,9,NULL,NULL,NULL),
(678,NULL,46,'Daniil Dubov','Magnus Carlsen',2,0,1,9,NULL,NULL,NULL),
(679,NULL,46,'Aleksander Grischuk','Fabiano Caruana',1,2,1,9,NULL,NULL,NULL),
(680,NULL,46,'Magnus Carlsen','Hikaru Nakamura',1,0,1,10,'Sofia, Bulgaria',NULL,NULL),
(681,NULL,46,'Fabiano Caruana','Garry Kasparov',1,4,1,10,NULL,NULL,NULL),
(682,NULL,46,'Aleksander Grischuk','Daniil Dubov',1,3,1,10,NULL,NULL,NULL),
(746,NULL,NULL,'Radko Radev','Daniil Dubov',1,2,1,NULL,'Moskow, RU',NULL,NULL),
(747,NULL,NULL,'Radko Radev','Daniil Dubov',1,0,1,NULL,'Sofia, BG',NULL,NULL),
(748,NULL,NULL,'Daniil Dubov','Misho Asenov',2,0,1,NULL,'Sofia, BG',NULL,NULL),
(749,NULL,NULL,'Daniil Dubov','Misho Asenov',0,1,1,NULL,'Moskow, RU',NULL,NULL),
(750,NULL,NULL,'Aleksander Grischuk','Daniil Dubov',0,3,1,NULL,NULL,NULL,NULL),
(751,NULL,NULL,'Daniil Dubov','Aleksander Grischuk',1,2,NULL,NULL,NULL,NULL,NULL),
(752,NULL,NULL,'Fabiano Caruana','Daniil Dubov',NULL,NULL,1,NULL,NULL,NULL,NULL),
(753,NULL,NULL,'Levon Aronian','Veselin Topalov',1,2,1,NULL,'Ankara, TR',NULL,NULL),
(754,'2023-01-09 13:34:00',NULL,'Veselin Topalov','Levon Aronian',NULL,NULL,1,NULL,'Sofia,Bulgaria',NULL,NULL),
(755,'2023-01-09 13:34:00',NULL,'Veselin Topalov','Levon Aronian',NULL,NULL,1,NULL,'Sofia,Bulgaria',NULL,NULL),
(786,'2023-11-15 19:00:00',55,'Chi Cheng','Dancho Lechev',1,2,2,4,'Petrich, California',NULL,NULL),
(787,NULL,55,'Daniil Dubov','Sun Pin',NULL,NULL,2,4,'Hong Kong SAR',NULL,NULL),
(788,NULL,55,'Ivo Penchev','Radko Radev',NULL,NULL,2,4,'Hong Kong SAR',NULL,NULL),
(789,NULL,55,'Lili Denevska','Levon Aronian',NULL,NULL,2,4,'Hong Kong SAR',NULL,NULL),
(790,NULL,55,'Garry Kasparov','Magnus Carlsen',NULL,NULL,2,4,'Hong Kong SAR',NULL,NULL),
(791,NULL,55,'Fabiano Caruana','Sisi Peneva',NULL,NULL,2,4,'Hong Kong SAR',NULL,NULL),
(792,NULL,55,'Veselin Topalov','Hikaru Nakamura',NULL,NULL,2,4,'Hong Kong SAR',NULL,NULL),
(793,NULL,55,'Jack Daniel','Misho Asenov',NULL,NULL,2,4,'Hong Kong SAR',NULL,NULL),
(794,'2023-11-15 19:00:00',55,'Dancho Lechev','Radko Radev',NULL,NULL,2,3,'Hong Kong SAR',786,787),
(795,'2023-11-15 19:00:00',55,NULL,'Dancho Lechev',NULL,NULL,2,3,'Hong Kong SAR',788,789),
(796,NULL,55,NULL,NULL,NULL,NULL,2,3,'Hong Kong SAR',790,791),
(797,NULL,55,NULL,NULL,NULL,NULL,2,3,'Hong Kong SAR',792,793),
(798,NULL,55,NULL,NULL,NULL,NULL,2,2,'Hong Kong SAR',794,795),
(799,NULL,55,NULL,NULL,NULL,NULL,2,2,'Hong Kong SAR',796,797),
(800,NULL,55,NULL,NULL,NULL,NULL,2,1,'Hong Kong SAR',798,799),
(801,'2023-02-06 10:00:00',56,'Radko Radev','Dancho Lechev',1,2,2,2,'Petrich, California',NULL,NULL),
(802,'2023-11-15 19:00:00',56,'Anne Marie Nicholson','Ivo Penchev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(803,NULL,56,'Dancho Lechev',NULL,NULL,NULL,2,1,'Mexico City, MX',801,802),
(804,NULL,57,'Ivo Penchev','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(805,NULL,57,'Anne Marie Nicholson','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(806,NULL,57,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',804,805),
(807,NULL,58,'Ivo Penchev','Anne Marie Nicholson',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(808,'2023-11-09 09:00:00',58,'Radko Radev','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(809,NULL,58,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',807,808),
(810,NULL,59,'Anne Marie Nicholson','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(811,NULL,59,'Ivo Penchev','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(812,NULL,59,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',810,811),
(813,NULL,60,'Ivo Penchev','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(814,NULL,60,'Dancho Lechev','Anne Marie Nicholson',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(815,NULL,60,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',813,814),
(816,NULL,61,'Anne Marie Nicholson','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(817,NULL,61,'Ivo Penchev','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(818,NULL,61,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',816,817),
(819,NULL,62,'Anne Marie Nicholson','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(820,NULL,62,'Dancho Lechev','Ivo Penchev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(821,NULL,62,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',819,820),
(822,NULL,63,'Ivo Penchev','Anne Marie Nicholson',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(823,NULL,63,'Dancho Lechev','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(824,NULL,63,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',822,823),
(825,NULL,64,'Ivo Penchev','Anne Marie Nicholson',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(826,NULL,64,'Dancho Lechev','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(827,NULL,64,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',825,826),
(828,NULL,65,'Ivo Penchev','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(829,NULL,65,'Radko Radev','Anne Marie Nicholson',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(830,NULL,65,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',828,829),
(831,NULL,66,'Dancho Lechev','Ivo Penchev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(832,NULL,66,'Radko Radev','Anne Marie Nicholson',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(833,NULL,66,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',831,832),
(834,NULL,67,'Anne Marie Nicholson','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(835,NULL,67,'Ivo Penchev','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(836,NULL,67,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',834,835),
(837,'2023-01-09 10:00:00',68,'Radko Radev','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(838,NULL,68,'Ivo Penchev','Anne Marie Nicholson',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(839,NULL,68,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',837,838),
(840,NULL,69,'Radko Radev','Anne Marie Nicholson',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(841,NULL,69,'Dancho Lechev','Ivo Penchev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(842,NULL,69,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',840,841),
(843,NULL,70,'Ivo Penchev','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(844,NULL,70,'Anne Marie Nicholson','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(845,NULL,70,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',843,844),
(846,NULL,71,'Dancho Lechev','Sun Pin',NULL,NULL,1,1,NULL,NULL,NULL),
(847,NULL,71,'Sun Pin','Dancho Lechev',NULL,NULL,1,2,NULL,NULL,NULL),
(848,NULL,72,'Niksy Fo','Dancho Lechev',NULL,NULL,1,1,NULL,NULL,NULL),
(849,NULL,72,'Dancho Lechev','Niksy Fo',NULL,NULL,1,2,NULL,NULL,NULL),
(850,NULL,73,'Niksy Dju','Dancho Lechev',NULL,NULL,1,1,NULL,NULL,NULL),
(851,NULL,73,'Dancho Lechev','Niksy Dju',NULL,NULL,1,2,NULL,NULL,NULL),
(852,NULL,74,'Dancho Lechev','Niksy Dju',NULL,NULL,1,1,NULL,NULL,NULL),
(853,NULL,74,'Niksy Dju','Dancho Lechev',NULL,NULL,1,2,NULL,NULL,NULL),
(854,NULL,75,'Dancho Lechev','Niksy Dju',NULL,NULL,1,1,NULL,NULL,NULL),
(855,NULL,75,'Niksy Dju','Dancho Lechev',NULL,NULL,1,2,NULL,NULL,NULL),
(856,'2023-01-09 13:00:00',NULL,NULL,'Dancho Lechev',0,0,1,NULL,'Sofia, Bulgaria',NULL,NULL),
(857,NULL,76,'Dancho Lechev','Anne Marie Nicholson',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(858,NULL,76,'Ivo Penchev','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(859,NULL,76,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',857,858),
(860,NULL,77,'Niksy Dju','Dancho Lechev',NULL,NULL,1,1,NULL,NULL,NULL),
(861,NULL,77,'Dancho Lechev','Niksy Dju',NULL,NULL,1,2,NULL,NULL,NULL),
(922,NULL,NULL,'Daniil Dubov','Misho Asenov',1,0,NULL,NULL,NULL,NULL,NULL),
(923,NULL,NULL,'Daniil Dubov','Garry Kasparov',0,3,1,NULL,NULL,NULL,NULL),
(924,NULL,NULL,'Garry Kasparov','Daniil Dubov',3,0,1,NULL,NULL,NULL,NULL),
(925,NULL,NULL,'Aleksander Grischuk','Daniil Dubov',1,2,1,NULL,NULL,NULL,NULL),
(938,NULL,84,'Jan-Krzysztof Duda','Shakhriyar Mamedyarov',NULL,NULL,1,1,NULL,NULL,NULL),
(939,NULL,84,'Ding Liren','Wesley So',NULL,NULL,1,1,NULL,NULL,NULL),
(940,NULL,84,'Wesley So','Jan-Krzysztof Duda',NULL,NULL,1,2,NULL,NULL,NULL),
(941,NULL,84,'Ding Liren','Shakhriyar Mamedyarov',NULL,NULL,1,2,NULL,NULL,NULL),
(942,NULL,84,'Jan-Krzysztof Duda','Ding Liren',NULL,NULL,1,3,NULL,NULL,NULL),
(943,NULL,84,'Wesley So','Shakhriyar Mamedyarov',NULL,NULL,1,3,NULL,NULL,NULL),
(944,NULL,84,'Shakhriyar Mamedyarov','Jan-Krzysztof Duda',NULL,NULL,1,4,NULL,NULL,NULL),
(945,NULL,84,'Wesley So','Ding Liren',NULL,NULL,1,4,NULL,NULL,NULL),
(946,NULL,84,'Jan-Krzysztof Duda','Wesley So',NULL,NULL,1,5,NULL,NULL,NULL),
(947,NULL,84,'Shakhriyar Mamedyarov','Ding Liren',NULL,NULL,1,5,NULL,NULL,NULL),
(948,NULL,84,'Ding Liren','Jan-Krzysztof Duda',NULL,NULL,1,6,NULL,NULL,NULL),
(949,NULL,84,'Shakhriyar Mamedyarov','Wesley So',NULL,NULL,1,6,NULL,NULL,NULL),
(951,NULL,85,'Niksy Dju','Dancho Lechev',NULL,NULL,1,1,NULL,NULL,NULL),
(952,NULL,85,'Dancho Lechev','Niksy Dju',NULL,NULL,1,2,NULL,NULL,NULL),
(953,NULL,86,'Dancho Lechev','Toshko Peshev',NULL,NULL,1,1,NULL,NULL,NULL),
(954,NULL,86,'Toshko Peshev','Dancho Lechev',NULL,NULL,1,2,NULL,NULL,NULL),
(955,NULL,87,'Koko Didov','Dido Kokov',NULL,NULL,1,1,NULL,NULL,NULL),
(956,NULL,87,'Dido Kokov','Koko Didov',NULL,NULL,1,2,NULL,NULL,NULL),
(957,NULL,88,'Anne Marie Nicholson','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(958,NULL,88,'Radko Radev','Ivo Penchev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(959,NULL,88,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',957,958),
(960,NULL,89,'Anne Marie Nicholson','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(961,NULL,89,'Ivo Penchev','Dancho Lechev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(962,NULL,89,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',960,961),
(963,NULL,90,'Anne Marie Nicholson','Radko Radev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(964,NULL,90,'Dancho Lechev','Ivo Penchev',NULL,NULL,2,2,'Mexico City, MX',NULL,NULL),
(965,NULL,90,NULL,NULL,NULL,NULL,2,1,'Mexico City, MX',963,964);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(90) NOT NULL,
  `country` varchar(45) DEFAULT NULL,
  `club` varchar(45) DEFAULT NULL,
  `flag` tinyint(1) DEFAULT NULL COMMENT ' null not deleted record (existing)\\n1 is for deleted record',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fullname_UNIQUE` (`fullname`)
) ENGINE=InnoDB AUTO_INCREMENT=321 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES
(99,'Tai Lin','Taiwan','SK Taipe City',NULL),
(100,'Chen Chen','Taiwan','SK Taipe United',NULL),
(111,'Dancho Lechev','Bulgaria','SK Real Sofia',NULL),
(112,'Sisi Peneva','Bulgaria','SK Real Sofia',NULL),
(113,'Radko Radev','Bulgaria','SK Real Sofia',NULL),
(114,'Lili Denevska','Bulgaria','SK Real Sofia',NULL),
(281,'Magnus Carlsen','Norway','',NULL),
(282,'Hikaru Nakamura',NULL,NULL,NULL),
(283,'Fabiano Caruana',NULL,NULL,NULL),
(289,'Jack Daniel','USA','SK Texas Guns',NULL),
(290,'Anne Marie Nicholson','USA','SK Master Tigers',NULL),
(291,'Anne-Marie Nicholson-Jackson','USA','SK NY',NULL),
(292,'Garry Kasparov','Russia','SK Moskow City',NULL),
(293,'Aleksander Grischuk',NULL,NULL,NULL),
(294,'Daniil Dubov','Russia','SK CSKA',NULL),
(295,'Sun Pin','Taiwan','SK Taipe City',NULL),
(296,'Chi Cheng','Taiwan','SK Taipe City',NULL),
(299,'Ivo Penchev','Bulgaria',NULL,NULL),
(300,'Misho Asenov','Turkey','Fenerbahce S.K.',NULL),
(301,'Veselin Topalov','Bulgaria','SK Tigers',NULL),
(302,'Levon Aronian',NULL,NULL,NULL),
(303,'Niksy Fo','USA','SK Master Tigers',NULL),
(304,'Niksy Dju','Russia','SK CSKA',NULL),
(305,'Bibi Mentor','Bulgaria','SK Master Tigers',NULL),
(306,'Ekaterina Atalik','Russia','Moskow City',NULL),
(307,'Anatoly Karpov','Russia','Saint Petersburg',NULL),
(308,'Boris Maliutin','Russia','SK CSKA',NULL),
(309,'Artyom Timofeev','Russia','Saint Petersburg ',NULL),
(310,'Alexander Shabalov','USA','SK Master Tigers',NULL),
(311,'Samuel Shankland','USA','SK Texas Guns',NULL),
(312,'Bob Black','USA','SK Texas Guns',NULL),
(314,'Shakhriyar Mamedyarov',NULL,NULL,NULL),
(315,'Wesley So',NULL,NULL,NULL),
(316,'Ding Liren',NULL,NULL,NULL),
(317,'Jan-Krzysztof Duda',NULL,NULL,NULL),
(318,'Toshko Peshev',NULL,NULL,NULL),
(319,'Koko Didov',NULL,NULL,NULL),
(320,'Dido Kokov',NULL,NULL,NULL);
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `player_id` int(11) DEFAULT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`),
  KEY `fk_requests_users1_idx` (`user_id`),
  KEY `fk_requests_players1_idx` (`player_id`),
  CONSTRAINT `fk_requests_players1` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_requests_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` VALUES
(22,11,'promote',NULL,'approved'),
(23,11,'link',111,'approved'),
(24,11,'link',111,'declined'),
(25,11,'promote',NULL,'declined');
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournaments`
--

DROP TABLE IF EXISTS `tournaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `format` tinyint(4) NOT NULL,
  `game_format_id` int(11) NOT NULL,
  `number_of_rounds` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `prizes` varchar(1000) DEFAULT NULL,
  `winner` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tournaments_game_formats1_idx` (`game_format_id`),
  KEY `fk_tournaments_players1_idx` (`winner`),
  CONSTRAINT `fk_tournaments_game_formats1` FOREIGN KEY (`game_format_id`) REFERENCES `game_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournaments_players1` FOREIGN KEY (`winner`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournaments`
--

LOCK TABLES `tournaments` WRITE;
/*!40000 ALTER TABLE `tournaments` DISABLE KEYS */;
INSERT INTO `tournaments` VALUES
(3,'Chess League',0,1,2,NULL,NULL,NULL,NULL,NULL),
(5,'New Chess League',0,1,10,NULL,NULL,'Sofia, Bulgaria',NULL,NULL),
(6,'Past Event 1',0,1,NULL,'2019-05-10','2019-06-10','NY, USA',NULL,'Chen Chen'),
(7,'Past Event 2',0,1,NULL,'2020-04-05','2020-04-28','Petrich, Bulgaria',NULL,'Chen Chen'),
(8,'Future Event 1',1,1,NULL,'2025-10-01','2025-11-15','Budapest, Hungary',NULL,NULL),
(9,'Present Event 1',1,1,NULL,'2022-10-15','2022-12-01','Sofia, Bulgaria',NULL,NULL),
(10,'Present Event 2',1,1,NULL,'1998-02-02',NULL,NULL,NULL,NULL),
(11,'Future Event 2',1,1,NULL,'2024-02-02',NULL,NULL,NULL,NULL),
(12,'Past Event 3',1,1,NULL,NULL,'1998-02-02',NULL,NULL,'Garry Kasparov'),
(13,'Present Event 5',1,1,NULL,NULL,'2024-02-02',NULL,NULL,NULL),
(46,'New Chess League',0,1,10,'2023-01-09','2023-02-09','Mallorca, Spain',NULL,NULL),
(55,'Asian Championship 2023',1,2,4,NULL,NULL,'Hong Kong SAR','1st place: $100 000, 2nd place: $30 000, 3th place: $5 000',NULL),
(56,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(57,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(58,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(59,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(60,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(61,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(62,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(63,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(64,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(65,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(66,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(67,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(68,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(69,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(70,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(71,'Summer League',0,1,2,NULL,NULL,'Sofia, BG','Lamborghini Aventador LP 780-4',NULL),
(72,'Summer League',0,1,2,NULL,NULL,'Sofia, BG','Lamborghini Aventador LP 780-4',NULL),
(73,'Summer League',0,1,2,NULL,NULL,'Sofia, BG','Lamborghini Aventador LP 780-4',NULL),
(74,'Summer League',0,1,2,NULL,NULL,'Sofia, BG','Lamborghini Aventador LP 780-4',NULL),
(75,'Summer League',0,1,2,NULL,NULL,'Sofia, BG','Lamborghini Aventador LP 780-4',NULL),
(76,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(77,'Summer League',0,1,2,NULL,NULL,'Sofia, BG','Lamborghini Aventador LP 780-4',NULL),
(84,'New Chess League 2.0',0,1,6,'2023-01-09',NULL,'Sofia, Bulgaria','Winner takes it all: 100 000$',NULL),
(85,'Summer League',0,1,2,NULL,NULL,'Sofia, BG','Lamborghini Aventador LP 780-4',NULL),
(86,'Summer League',0,1,2,NULL,NULL,'Sofia, BG','Lamborghini Aventador LP 780-4',NULL),
(87,'Summer League',0,1,2,NULL,NULL,'Sofia, BG','Lamborghini Aventador LP 780-4',NULL),
(88,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(89,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL),
(90,'World Championship 2023',1,2,2,'2023-03-15','2023-03-16','Mexico City, MX','1st place: $500, 2nd place: $100',NULL);
/*!40000 ALTER TABLE `tournaments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tournaments_players`
--

DROP TABLE IF EXISTS `tournaments_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments_players` (
  `tournament_id` int(11) NOT NULL,
  `player_name` varchar(90) NOT NULL,
  `points` float DEFAULT 0,
  KEY `fk_table1_tournaments1_idx` (`tournament_id`),
  KEY `fk_table1_players1_idx` (`player_name`),
  CONSTRAINT `fk_tournaments_players_players1` FOREIGN KEY (`player_name`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournaments_players_tournaments1` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tournaments_players`
--

LOCK TABLES `tournaments_players` WRITE;
/*!40000 ALTER TABLE `tournaments_players` DISABLE KEYS */;
INSERT INTO `tournaments_players` VALUES
(46,'Magnus Carlsen',1.5),
(46,'Hikaru Nakamura',2.5),
(46,'Fabiano Caruana',0),
(46,'Garry Kasparov',1.5),
(46,'Aleksander Grischuk',1),
(46,'Daniil Dubov',2.5),
(55,'Ivo Penchev',0),
(55,'Misho Asenov',0),
(55,'Hikaru Nakamura',0),
(55,'Magnus Carlsen',0),
(55,'Garry Kasparov',0),
(55,'Veselin Topalov',0),
(55,'Levon Aronian',0),
(55,'Fabiano Caruana',0),
(55,'Dancho Lechev',0),
(55,'Sisi Peneva',0),
(55,'Radko Radev',0),
(55,'Lili Denevska',0),
(55,'Jack Daniel',0),
(55,'Sun Pin',0),
(55,'Chi Cheng',0),
(55,'Daniil Dubov',0),
(56,'Dancho Lechev',0),
(56,'Radko Radev',0),
(56,'Anne Marie Nicholson',0),
(56,'Ivo Penchev',0),
(57,'Dancho Lechev',0),
(57,'Radko Radev',0),
(57,'Anne Marie Nicholson',0),
(57,'Ivo Penchev',0),
(58,'Dancho Lechev',0),
(58,'Radko Radev',0),
(58,'Anne Marie Nicholson',0),
(58,'Ivo Penchev',0),
(59,'Dancho Lechev',0),
(59,'Radko Radev',0),
(59,'Anne Marie Nicholson',0),
(59,'Ivo Penchev',0),
(60,'Dancho Lechev',0),
(60,'Radko Radev',0),
(60,'Anne Marie Nicholson',0),
(60,'Ivo Penchev',0),
(61,'Dancho Lechev',0),
(61,'Radko Radev',0),
(61,'Anne Marie Nicholson',0),
(61,'Ivo Penchev',0),
(62,'Dancho Lechev',0),
(62,'Radko Radev',0),
(62,'Anne Marie Nicholson',0),
(62,'Ivo Penchev',0),
(63,'Dancho Lechev',0),
(63,'Radko Radev',0),
(63,'Anne Marie Nicholson',0),
(63,'Ivo Penchev',0),
(64,'Dancho Lechev',0),
(64,'Radko Radev',0),
(64,'Anne Marie Nicholson',0),
(64,'Ivo Penchev',0),
(65,'Dancho Lechev',0),
(65,'Radko Radev',0),
(65,'Anne Marie Nicholson',0),
(65,'Ivo Penchev',0),
(66,'Dancho Lechev',0),
(66,'Radko Radev',0),
(66,'Anne Marie Nicholson',0),
(66,'Ivo Penchev',0),
(67,'Dancho Lechev',0),
(67,'Radko Radev',0),
(67,'Anne Marie Nicholson',0),
(67,'Ivo Penchev',0),
(68,'Dancho Lechev',0),
(68,'Radko Radev',0),
(68,'Anne Marie Nicholson',0),
(68,'Ivo Penchev',0),
(69,'Dancho Lechev',0),
(69,'Radko Radev',0),
(69,'Anne Marie Nicholson',0),
(69,'Ivo Penchev',0),
(70,'Dancho Lechev',0),
(70,'Radko Radev',0),
(70,'Anne Marie Nicholson',0),
(70,'Ivo Penchev',0),
(71,'Dancho Lechev',0),
(71,'Sun Pin',0),
(72,'Dancho Lechev',0),
(72,'Niksy Fo',0),
(73,'Dancho Lechev',0),
(73,'Niksy Dju',0),
(74,'Dancho Lechev',0),
(74,'Niksy Dju',0),
(75,'Dancho Lechev',0),
(75,'Niksy Dju',0),
(76,'Dancho Lechev',0),
(76,'Radko Radev',0),
(76,'Anne Marie Nicholson',0),
(76,'Ivo Penchev',0),
(77,'Dancho Lechev',0),
(77,'Niksy Dju',0),
(84,'Wesley So',0),
(84,'Ding Liren',0),
(84,'Jan-Krzysztof Duda',0),
(84,'Shakhriyar Mamedyarov',0),
(85,'Dancho Lechev',0),
(85,'Niksy Dju',0),
(86,'Dancho Lechev',0),
(86,'Toshko Peshev',0),
(87,'Koko Didov',0),
(87,'Dido Kokov',0),
(88,'Dancho Lechev',0),
(88,'Radko Radev',0),
(88,'Anne Marie Nicholson',0),
(88,'Ivo Penchev',0),
(89,'Dancho Lechev',0),
(89,'Radko Radev',0),
(89,'Anne Marie Nicholson',0),
(89,'Ivo Penchev',0),
(90,'Dancho Lechev',0),
(90,'Radko Radev',0),
(90,'Anne Marie Nicholson',0),
(90,'Ivo Penchev',0);
/*!40000 ALTER TABLE `tournaments_players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(65) NOT NULL,
  `role` varchar(45) NOT NULL DEFAULT 'user',
  `player_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_player_profiles_idx` (`player_id`),
  CONSTRAINT `fk_users_player_profiles` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES
(1,'match.score.fp@gmail.com','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','admin',NULL),
(10,'yovo.tonchev7@abv.bg','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','director',NULL),
(11,'dancholechev@yahoo.com','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','director',111),
(13,'system_user@mail.bg','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','user',295),
(16,'champion@abv.bg','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','user',296),
(17,'firstplace@mail.com','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','user',NULL),
(19,'topalov@mail.com','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','user',301),
(20,'naka@mura.jp','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','user',282),
(21,'fabianocaruana@chess.it','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','user',283),
(22,'jacky.d@chess.us','41377906ce26d8b0d18b1ba5be75edf7b92aa0036fa7362ead70c8d6d348230d','user',289);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-11 14:39:45
