SELECT * FROM match_score_fp.games;

SELECT id, player1_name, player2_name, score1, score2 from games where player1_name = 'Daniil Dubov' or player2_name = 'Daniil Dubov';

SELECT count(id) as 'matches_played' from games where player1_name = 'Daniil Dubov' or player2_name = 'Daniil Dubov';

SELECT Count(u.player1_name) as 'total_games', u.player1_name from (SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov') as u 
GROUP BY u.player1_name;

-- matches won
SELECT count(id) as 'matches_won' from games where player1_name = 'Daniil Dubov' and score1>score2 or player2_name = 'Daniil Dubov' and score2>score1;
SELECT u.player1_name, u.score1, u.score2 FROM (SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov')as u WHERE u.score1<u.score2;

-- wins and opponents
SELECT Count(wins.player1_name), wins.player1_name FROM (SELECT u.player1_name, u.score1, u.score2 FROM (SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov')as u WHERE u.score1<u.score2) as wins
GROUP BY wins.player1_name
ORDER BY Count(wins.player1_name) DESC;

-- matches loss
SELECT count(id) as 'matches_loss' from games where player1_name = 'Daniil Dubov' and score1<score2 or player2_name = 'Daniil Dubov' and score2<score1;
SELECT u.player1_name, u.score1, u.score2 FROM (SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov')as u WHERE u.score1>u.score2;

-- lost matches and opponents
SELECT Count(lost.player1_name), lost.player1_name FROM (SELECT u.player1_name, u.score1, u.score2 FROM (SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov')as u WHERE u.score1>u.score2) as lost
GROUP BY lost.player1_name
ORDER BY Count(lost.player1_name) DESC;

-- matches equal
SELECT u.player1_name, u.score1, u.score2 FROM (SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov')as u WHERE u.score1=u.score2;


-- most often played opponent name
SELECT most_often.total_games, most_often.opponent FROM 
            (SELECT Count(u.player1_name) as 'total_games', u.player1_name as opponent FROM 
                (SELECT player1_name FROM games WHERE player2_name = 'Daniil Dubov' 
                UNION ALL 
                SELECT player2_name FROM games WHERE player1_name = 'Daniil Dubov') as u
            GROUP BY u.player1_name) as most_often
        ORDER BY most_often.total_games DESC, most_often.opponent  LIMIT 1;


-- opponents scores
SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov';

-- opponents names
SELECT distinct(player1_name) FROM (SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov') as opponents;

-- best opponent - with the most losses NOOOOOO
-- best opponent - with the less wins или с когото имаш най-малко загуби
-- 5 games  3 losses 2 wins--> 2/5 --> 40%
-- 2 games  2 losses 0 wins --> 0/2 --> 0%

-- SELECT IFNULL(col_with_null_value, default_value);
SELECT op.player1_name as opponent, lost_games.lost  as losses FROM (SELECT player1_name FROM games WHERE player2_name = 'Daniil Dubov'
UNION SELECT player2_name FROM games WHERE player1_name = 'Daniil Dubov') as op
JOIN (SELECT Count(lost.player1_name) as lost, lost.player1_name FROM (SELECT u.player1_name, u.score1, u.score2 FROM (SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov') as u WHERE u.score1>u.score2) as lost
GROUP BY lost.player1_name
ORDER BY Count(lost.player1_name)) as lost_games on lost_games.player1_name = op.player1_name
ORDER BY losses, opponent
LIMIT 1;

'Fabiano Caruana'
-- best opponent losses
SELECT COUNT(games_with_opponents.id) FROM (SELECT id, player1_name as opponent, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT id, player2_name, score2, score1 from games where player1_name = 'Daniil Dubov') as games_with_opponents
WHERE games_with_opponents.opponent = 'Fabiano Caruana' and games_with_opponents.score1 < games_with_opponents.score2;

-- worst opponent which has the less losses срещу когото имаш най-много загуби
SELECT * FROM match_score_fp.games;
'Misho Asenov'
SELECT op.player1_name as opponent, lost_games.lost  as losses FROM (SELECT player1_name FROM games WHERE player2_name = 'Daniil Dubov'
UNION SELECT player2_name FROM games WHERE player1_name = 'Daniil Dubov') as op
JOIN (SELECT Count(lost.player1_name) as lost, lost.player1_name FROM (SELECT u.player1_name, u.score1, u.score2 FROM (SELECT player1_name, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT player2_name, score2, score1 from games where player1_name = 'Daniil Dubov') as u WHERE u.score1>u.score2) as lost
GROUP BY lost.player1_name
ORDER BY Count(lost.player1_name)) as lost_games on lost_games.player1_name = op.player1_name
ORDER BY losses DESC, opponent
LIMIT 1;

-- worst opponent losses
SELECT count(games_with_opponents.id) FROM (SELECT id, player1_name as opponent, score1, score2 from games where player2_name = 'Daniil Dubov'
UNION ALL SELECT id, player2_name, score2, score1 from games where player1_name = 'Daniil Dubov') as games_with_opponents
WHERE games_with_opponents.opponent = 'Misho Asenov' and games_with_opponents.score1 < games_with_opponents.score2;