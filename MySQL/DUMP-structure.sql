CREATE DATABASE  IF NOT EXISTS `f6jl0k22j2cpjuse` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `f6jl0k22j2cpjuse`;
-- MariaDB dump 10.19  Distrib 10.9.2-MariaDB, for Win64 (AMD64)
--
-- Host: iu51mf0q32fkhfpl.cbetxkdyhwsb.us-east-1.rds.amazonaws.com    Database: f6jl0k22j2cpjuse
-- ------------------------------------------------------
-- Server version	10.4.26-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `game_formats`
--

DROP TABLE IF EXISTS `game_formats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_formats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_limit` int(11) DEFAULT NULL,
  `score_limit` int(11) DEFAULT NULL,
  `loss_points` float DEFAULT 0,
  `draw_points` float NOT NULL,
  `win_points` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime DEFAULT NULL,
  `tournament_id` int(11) DEFAULT NULL,
  `player1_name` varchar(90) DEFAULT NULL,
  `player2_name` varchar(90) DEFAULT NULL,
  `score1` int(11) DEFAULT NULL,
  `score2` int(11) DEFAULT NULL,
  `game_format_id` int(11) DEFAULT NULL,
  `round` int(11) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `prev_game1_id` int(11) DEFAULT NULL,
  `prev_game2_id` int(11) DEFAULT NULL,
  `updated` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_games_tournaments1_idx` (`tournament_id`),
  KEY `fk_games_players1_idx` (`player1_name`),
  KEY `fk_games_players2_idx` (`player2_name`),
  KEY `fk_games_game_formats1_idx` (`game_format_id`),
  KEY `fk_games_games1_idx` (`prev_game1_id`),
  KEY `fk_games_games2_idx` (`prev_game2_id`),
  CONSTRAINT `fk_games_game_formats1` FOREIGN KEY (`game_format_id`) REFERENCES `game_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_games1` FOREIGN KEY (`prev_game1_id`) REFERENCES `games` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_games2` FOREIGN KEY (`prev_game2_id`) REFERENCES `games` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_players1` FOREIGN KEY (`player1_name`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_players2` FOREIGN KEY (`player2_name`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_games_tournaments1` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(90) NOT NULL,
  `country` varchar(45) DEFAULT NULL,
  `club` varchar(45) DEFAULT NULL,
  `flag` tinyint(1) DEFAULT NULL COMMENT ' null not deleted record (existing)\\n1 is for deleted record',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fullname_UNIQUE` (`fullname`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL,
  `player_id` int(11) DEFAULT NULL,
  `status` varchar(45) NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`),
  KEY `fk_requests_users1_idx` (`user_id`),
  KEY `fk_requests_players1_idx` (`player_id`),
  CONSTRAINT `fk_requests_players1` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_requests_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tournaments`
--

DROP TABLE IF EXISTS `tournaments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `format` tinyint(4) NOT NULL,
  `game_format_id` int(11) NOT NULL,
  `number_of_rounds` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `prizes` varchar(1000) DEFAULT NULL,
  `winner` varchar(90) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tournaments_game_formats1_idx` (`game_format_id`),
  KEY `fk_tournaments_players1_idx` (`winner`),
  CONSTRAINT `fk_tournaments_game_formats1` FOREIGN KEY (`game_format_id`) REFERENCES `game_formats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournaments_players1` FOREIGN KEY (`winner`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tournaments_players`
--

DROP TABLE IF EXISTS `tournaments_players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tournaments_players` (
  `tournament_id` int(11) NOT NULL,
  `player_name` varchar(90) NOT NULL,
  `points` float DEFAULT 0,
  KEY `fk_table1_tournaments1_idx` (`tournament_id`),
  KEY `fk_table1_players1_idx` (`player_name`),
  CONSTRAINT `fk_tournaments_players_players1` FOREIGN KEY (`player_name`) REFERENCES `players` (`fullname`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tournaments_players_tournaments1` FOREIGN KEY (`tournament_id`) REFERENCES `tournaments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(65) NOT NULL,
  `role` varchar(45) NOT NULL DEFAULT 'user',
  `player_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_player_profiles_idx` (`player_id`),
  CONSTRAINT `fk_users_player_profiles` FOREIGN KEY (`player_id`) REFERENCES `players` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-11 18:17:09
